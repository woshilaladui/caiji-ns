package com.whut.ns.caiji.hadoop.test_grouping;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Reducer;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/10/23 15:00
 * @desription
 */
public class OrderReducer extends Reducer<OrderBean, NullWritable, OrderBean, NullWritable> {


    @Override
    protected void reduce(OrderBean key, Iterable<NullWritable> values, Context context)		throws IOException, InterruptedException {


        System.out.println("1 key = " + key.toString());

        for (NullWritable value : values) {
            System.out.println("2 key = " + key.toString());
            context.write(key, NullWritable.get());
        }



    }

}
