package com.whut.my.caiji.ns.dto.response.modle.netty.im;

import java.io.Serializable;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2021/2/15 13:38
 * @desription
 */
public class MyFriendsBody implements Serializable {
    private Long friendUserId;
    private Long friendUsername;
    //private String friendFaceImage;
    //private String friendNickname;


    public Long getFriendUserId() {
        return friendUserId;
    }

    public void setFriendUserId(Long friendUserId) {
        this.friendUserId = friendUserId;
    }

    public Long getFriendUsername() {
        return friendUsername;
    }

    public void setFriendUsername(Long friendUsername) {
        this.friendUsername = friendUsername;
    }
}
