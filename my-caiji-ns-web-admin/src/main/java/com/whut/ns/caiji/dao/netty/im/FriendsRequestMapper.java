package com.whut.ns.caiji.dao.netty.im;


import com.whut.my.caiji.ns.domain.netty.im.FriendsRequest;

public interface FriendsRequestMapper {
    int deleteByPrimaryKey(Long id);

    int insert(FriendsRequest record);

    int insertSelective(FriendsRequest record);

    FriendsRequest selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(FriendsRequest record);

    int updateByPrimaryKey(FriendsRequest record);

    //根据好友请求对象进行删除操作
    void deleteByFriendRequest(FriendsRequest friendsRequest);
}