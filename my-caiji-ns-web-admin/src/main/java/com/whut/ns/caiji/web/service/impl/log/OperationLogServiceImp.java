package com.whut.ns.caiji.web.service.impl.log;

import com.whut.my.caiji.ns.commons.persistence.BaseModel;
import com.whut.my.caiji.ns.domain.log.TbLogRecord;
import com.whut.my.caiji.ns.domain.zhongkongshi.ZhongKongShi;
import com.whut.ns.caiji.abstracts.AbstractBaseServiceImpl;
import com.whut.ns.caiji.annotation.Read;
import com.whut.ns.caiji.annotation.Write;
import com.whut.ns.caiji.dao.log.OperationLogDao;
import com.whut.ns.caiji.dao.zhongkongshi.TbZhongKongShiDao;
import com.whut.ns.caiji.web.service.log.OperationLogService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/7/30 17:42
 * @desription
 */
@Service
public class OperationLogServiceImp extends AbstractBaseServiceImpl<TbLogRecord, OperationLogDao>  implements OperationLogService {

    @Override
    @Read
    public List<TbLogRecord> getAllByUsernameOrTimeBetween(String username, String startDate, String endDate) {

        return dao.getAllByUsernameOrTimeBetween(
                "%"+username+"%",
                startDate,
                endDate
        );
    }


    @Read
    public String preDeleteOperationLogs(List<Long> operationIds){
        return null;
    }

    @Override
    @Write
    public boolean deleteOperationLogs(List<Long> operationIds) {

        int flag = dao.deleteOperationLogs(operationIds);

        return flag > 0;
    }

    @Override
    public TbLogRecord getLastLoginLog(String username, String tag) {
        return dao.getLastLoginLog(username,tag);
    }

    @Override
    public Integer deleteLogNMonthsAgo(int N) {
        return dao.deleteLogNMonthsAgo(N);
    }

    @Override
    @Write
    public BaseModel save(TbLogRecord entity) {

        Integer flag  = dao.insert(entity);

        if(flag > 0){
            //System.out.println("添加日志成功");
            return BaseModel.success("添加日志成功");
        }
        //System.out.println("添加日志失败");
        return BaseModel.error("添加日志失败");

    }

    @Override
    @Write
    public BaseModel saveCollection(List<TbLogRecord> entities) {
//
//        if(entities != null && entities.size()>0){
//            dao.insertAll(entities);
//        }

        return null;
    }
}
