package com.whut.ns.caiji.security.auth;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.whut.my.caiji.ns.commons.constants.Constant;
import com.whut.my.caiji.ns.commons.persistence.BaseModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.parameters.P;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Deprecated
@Component
public class MyAuthenticationSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

    @Autowired
    private ObjectMapper objectMapper;

    @Value("${spring.security.loginType}")
    private String loginType;

    @Override
    public void onAuthenticationSuccess(
            HttpServletRequest request,
            HttpServletResponse response,
            Authentication authentication
    ) throws ServletException, IOException {

        if(loginType.equalsIgnoreCase("JSON")){
            response.setContentType("application/json:charset=UTF-8");
            response.getWriter().write(objectMapper.writeValueAsString(
                    new BaseModel(
                            Constant.SUCESS,
                            "授权成功",
                            null
                    )
            ));
        }else {
            super.onAuthenticationSuccess(request, response, authentication);
        }

    }
}
