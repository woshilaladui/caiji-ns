package com.whut.ns.caiji.dao.auth;

import com.whut.my.caiji.ns.commons.persistence.BaseDao;
import com.whut.my.caiji.ns.domain.user.SysRolePermission;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/7/6 11:43
 * @desription
 */
@Repository
public interface TbSysRolePermissionDao extends BaseDao<SysRolePermission> {

    /**
     *
     * @param roleId
     * @return
     */
    List<SysRolePermission> selectRolePermissionByRoleId(Long roleId);

    /**
     *
     * @param roleId
     * @return
     */
    List<Long> selectRolePermissionIdByRoleId(Long roleId);

    /**
     *
     * @param permissionIds
     * @param roleId
     */
    void deleteRolePermissionByPermissionIds(List<Long> permissionIds,Long roleId);

}
