package com.whut.ns.caiji.web.service.standard;

import com.whut.my.caiji.ns.commons.persistence.BaseModel;
import com.whut.my.caiji.ns.commons.persistence.BaseService;
import com.whut.my.caiji.ns.domain.standard.Standard;
import com.whut.my.caiji.ns.domain.zhongkongshi.ZhongKongShi;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/7/14 17:40
 * @desription
 */
@Service
public interface TbStandardService  extends BaseService<Standard> {

    List<Standard> getStandardsDataByTableName(String tableName);

    Standard getStandardDataByTableName(String tableName);

    BaseModel preSave(Standard standard);



}
