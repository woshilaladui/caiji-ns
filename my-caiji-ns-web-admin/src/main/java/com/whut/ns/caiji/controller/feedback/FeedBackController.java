package com.whut.ns.caiji.controller.feedback;

import com.whut.my.caiji.ns.commons.constants.Constant;
import com.whut.my.caiji.ns.domain.feedback.FeedBack;

import com.whut.my.caiji.ns.commons.persistence.BaseModel;
import com.whut.my.caiji.ns.dto.response.modle.feedback.FeedBackBody;
import com.whut.my.caiji.ns.dto.response.modle.feedback.FeedBackModel;

import com.whut.ns.caiji.annotation.OperationLog;
import com.whut.ns.caiji.web.service.feedback.FeedBackService;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.List;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/11/10 9:48
 * @desription
 */
@Controller
@CrossOrigin("*")
@RequestMapping(value = "/feedback")
public class FeedBackController {

    @Autowired
    private FeedBackService feedBackService;

    @RequestMapping("/getAllFeedBacks")
    @ResponseBody
    @OperationLog(operation = "获取全部反馈信息")
    public FeedBackModel getAllFeedBacks() {

        List<FeedBack> feedBacks = feedBackService.selectAll();

        return new FeedBackModel(
                Constant.SUCESS,
                "获取全部反馈信息成功",
                new FeedBackBody(feedBacks)
        );

    }

    @RequestMapping("/getAllFeedBackByUsernameOrTimeBetween")
    @ResponseBody
    @OperationLog(operation = "通过用户名和时间来查询反馈信息")
    public FeedBackModel getAllFeedBackByUsernameOrTimeBetween(
            @RequestParam(name = "username", required = false) String username,
            @RequestParam(name = "startDate", required = false) Date startDate,
            @RequestParam(name = "endDate", required = false) Date endDate
    ) {

        List<FeedBack> feedBacks = feedBackService.getAllFeedBackByUsernameOrTimeBetween(username, startDate, endDate);

        return new FeedBackModel(
                Constant.SUCESS,
                "获取全部反馈信息成功",
                new FeedBackBody(feedBacks)
        );

    }

    @RequestMapping("/saveFeedBack")
    @ResponseBody
    @OperationLog(operation = "保存用户反馈")
    public BaseModel saveFeedBack(
            FeedBack feedBack
    ) {

        //System.out.println("saveFeedBack = " +  feedBack.toString());
        return feedBackService.save(feedBack);

    }

    @RequestMapping("/deleteFeedBack")
    @ResponseBody
    @OperationLog(operation = "删除用户反馈")
    public BaseModel deleteFeedBack(
            @RequestParam(name = "id", required = true) Long id
    ) {

        int flag = feedBackService.delete(id);

        if (flag > 0) {
            return BaseModel.success("删除反馈信息成功");
        }

        return BaseModel.success("删除反馈信息失败");

    }


}
