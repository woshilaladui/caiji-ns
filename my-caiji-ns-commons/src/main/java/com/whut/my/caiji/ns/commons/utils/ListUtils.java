package com.whut.my.caiji.ns.commons.utils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/7/7 19:46
 * @desription
 */
public class ListUtils {

    public static List<Long> changeIntegerListToLongList(List<Integer> sources){
        final List<Long> collect = sources.stream().map(x -> Long.valueOf(x)).collect(Collectors.toList());
        return collect;
    }

}
