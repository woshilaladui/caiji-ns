package com.whut.my.caiji.ns.dto.response.modle.zhongkongshi;


import com.whut.my.caiji.ns.dto.response.modle.BaseModel;

public class ZhongKongShiModel extends BaseModel {

    public ZhongKongShiBody data;

    public ZhongKongShiModel() {
    }

    public ZhongKongShiModel(ZhongKongShiBody data) {
        this.data = data;
    }

    public ZhongKongShiModel(int code, String msg, ZhongKongShiBody data) {
        super(code, msg);
        this.data = data;
    }

    public ZhongKongShiModel(int code, String msg, String controllerName, ZhongKongShiBody data) {
        super(code, msg, controllerName);
        this.data = data;
    }

    public ZhongKongShiBody getData() {
        return data;
    }

    public void setData(ZhongKongShiBody data) {
        this.data = data;
    }
}
