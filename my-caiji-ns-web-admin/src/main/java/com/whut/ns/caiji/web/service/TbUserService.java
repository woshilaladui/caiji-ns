package com.whut.ns.caiji.web.service;

import com.whut.my.caiji.ns.commons.persistence.BaseModel;
import com.whut.my.caiji.ns.commons.persistence.BaseService;
import com.whut.my.caiji.ns.dto.response.modle.user.UserBody;
import com.whut.my.caiji.ns.domain.user.TbUser;
import com.whut.my.caiji.ns.domain.user.permission.Permission;
import org.springframework.stereotype.Service;

@Service
public interface TbUserService extends BaseService<TbUser> {


    UserBody login(String email, String password);

    Permission getUserPermission(Long id);

    BaseModel register(String username, String phone, String password);



}
