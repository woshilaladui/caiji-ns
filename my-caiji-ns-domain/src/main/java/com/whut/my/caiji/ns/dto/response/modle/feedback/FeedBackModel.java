package com.whut.my.caiji.ns.dto.response.modle.feedback;

import com.whut.my.caiji.ns.dto.response.modle.BaseModel;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/11/10 9:54
 * @desription
 */
public class FeedBackModel extends BaseModel {

    public FeedBackBody data;

    public FeedBackModel() {
    }

    public FeedBackModel(FeedBackBody data) {
        this.data = data;
    }

    public FeedBackModel(int code, String msg, FeedBackBody data) {
        super(code, msg);
        this.data = data;
    }

    public FeedBackModel(int code, String msg, Object data, FeedBackBody data1) {
        super(code, msg, data);
        this.data = data1;
    }

    @Override
    public FeedBackBody getData() {
        return data;
    }

    public void setData(FeedBackBody data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "FeedBackModel{" +
                "data=" + data +
                ", code=" + code +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                '}';
    }
}
