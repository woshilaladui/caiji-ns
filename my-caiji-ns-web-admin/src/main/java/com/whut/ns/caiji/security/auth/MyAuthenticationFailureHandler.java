package com.whut.ns.caiji.security.auth;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.whut.my.caiji.ns.commons.constants.Constant;
import com.whut.my.caiji.ns.commons.persistence.BaseModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Deprecated
@Component
public class MyAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {

    @Value("spring.security.loginType")
    private String loginType;

    @Autowired
    private ObjectMapper objectMapper;


    @Override
    public void onAuthenticationFailure(
            HttpServletRequest request,
            HttpServletResponse response,
            AuthenticationException exception
    ) throws IOException, ServletException {

        if(loginType.equalsIgnoreCase("JSON")){

            response.setContentType("application/json:charset=UTF-8");
            response.getWriter().write(objectMapper.writeValueAsString(
                    new BaseModel(
                            Constant.ERROR,
                            "授权失败 : " + exception.getMessage(),
                            null
                    )
            ));

        }else {
            super.onAuthenticationFailure(request, response, exception);
        }


    }
}
