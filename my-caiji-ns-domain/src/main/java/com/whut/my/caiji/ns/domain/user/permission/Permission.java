package com.whut.my.caiji.ns.domain.user.permission;

public class Permission {

    public String username;

    public int state;

    public int department;

    public int duty;

    public int authority;

    public String detail;

    public Permission() {
    }

    public Permission(String username,int state, int department, int duty, int authority,String detail) {
        this.username = username;
        this.state = state;
        this.department = department;
        this.duty = duty;
        this.authority = authority;
        this.detail = detail;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getDepartment() {
        return department;
    }

    public void setDepartment(int department) {
        this.department = department;
    }

    public int getDuty() {
        return duty;
    }

    public void setDuty(int duty) {
        this.duty = duty;
    }

    public int getAuthority() {
        return authority;
    }

    public void setAuthority(int authority) {
        this.authority = authority;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    @Override
    public String toString() {
        return "Permission{" +
                "state=" + state +
                ", department=" + department +
                ", duty=" + duty +
                ", permission=" + authority +
                '}';
    }
}
