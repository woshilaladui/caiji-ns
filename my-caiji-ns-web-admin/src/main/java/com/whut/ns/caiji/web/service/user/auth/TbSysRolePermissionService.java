package com.whut.ns.caiji.web.service.user.auth;

import com.whut.my.caiji.ns.commons.persistence.BaseModel;
import com.whut.my.caiji.ns.commons.persistence.BaseService;
import com.whut.my.caiji.ns.domain.user.SysRolePermission;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/7/6 15:37
 * @desription
 */
@Service
public interface TbSysRolePermissionService extends BaseService<SysRolePermission> {

    BaseModel preSaveCollection(
            Long roleId,
            List<Long> permissionIds
    );

    List<Long> selectRolePermissionIdByRoleId(Long roleId);

}
