package com.whut.ns.caiji.web.service.huayanshi;

import com.whut.my.caiji.ns.commons.persistence.BaseModel;
import com.whut.my.caiji.ns.commons.persistence.BaseService;
import com.whut.my.caiji.ns.domain.huayanshi.HuaYanShi;
import com.whut.my.caiji.ns.dto.response.modle.huayanshi.HuaYanShiModel;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/7/15 10:51
 * @desription
 */
@Service
public interface TbHuaYanShiService extends BaseService<HuaYanShi> {
    List<HuaYanShi> getHuaYanShiDataByTableNameAndDate(String tableName, Date date);

    /**
     *
     * @param sourceHuaYanShis
     * @return
     */
    BaseModel preSaveCollection(List<HuaYanShi> sourceHuaYanShis);

    HuaYanShiModel getHuaYanShiDataDifferenceValueBytableNameAndDate(String tableName, Date today, Date yesterday );

}
