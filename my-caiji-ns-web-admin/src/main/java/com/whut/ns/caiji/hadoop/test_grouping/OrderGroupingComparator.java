package com.whut.ns.caiji.hadoop.test_grouping;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/10/23 15:06
 * @desription
 */
public class OrderGroupingComparator extends WritableComparator  {

    protected OrderGroupingComparator() {
        super(OrderBean.class, true);
    }

    @Override
    public int compare(WritableComparable a, WritableComparable b) {

        OrderBean aBean = (OrderBean) a;
        OrderBean bBean = (OrderBean) b;

        int result;
        if (aBean.getOrder_id() > bBean.getOrder_id()) {
            result = 1;
        } else if (aBean.getOrder_id() < bBean.getOrder_id()) {
            result = -1;
        }else {
            result = 0;
        }

        return result;
    }

}
