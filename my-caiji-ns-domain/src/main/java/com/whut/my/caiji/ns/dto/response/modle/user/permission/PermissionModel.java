package com.whut.my.caiji.ns.dto.response.modle.user.permission;


import com.whut.my.caiji.ns.dto.response.modle.BaseModel;

public class PermissionModel extends BaseModel {

    public PermissionBody data;

    public PermissionModel() {
    }

    public PermissionModel(PermissionBody data) {
        this.data = data;
    }

    public PermissionModel(int code, String msg, PermissionBody data) {
        //super(code, msg);
        this.data = data;
    }
}
