package com.whut.ns.caiji.dao.standard;

import com.whut.my.caiji.ns.commons.persistence.BaseDao;
import com.whut.my.caiji.ns.commons.persistence.BaseModel;
import com.whut.my.caiji.ns.domain.standard.Standard;

import java.util.List;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/7/14 16:44
 * @desription
 */
public interface TbStandardDao extends BaseDao<Standard> {


    List<Standard> getStandardsDataByTableName(String tableName);

    Standard getStandardDataByTableName(String tableName);

}
