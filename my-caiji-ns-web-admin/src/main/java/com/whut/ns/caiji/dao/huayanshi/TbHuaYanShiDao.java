package com.whut.ns.caiji.dao.huayanshi;

import com.whut.my.caiji.ns.commons.persistence.BaseDao;
import com.whut.my.caiji.ns.domain.huayanshi.HuaYanShi;

import java.util.List;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/7/15 10:26
 * @desription
 */
public interface TbHuaYanShiDao extends BaseDao<HuaYanShi> {

    /**
     * 通过表名和时间来查询数据
     * @param tableName
     * @param date
     * @return
     */
    List<HuaYanShi> getHuaYanShiDataByTableNameAndDate(String tableName, String date);

    List<HuaYanShi> getHuaYanShiDataDifferenceValueBytableNameAndDate(String tableName, String today,String yesterday);

    Integer updateHuaYanShis(List<HuaYanShi> huaYanShis);

    //获取date日期下的该表所有的下标值
    List<Integer> getHuaYanShiCurrentDateIndex(String tableName,String date);



}
