package com.whut.my.caiji.ns.domain.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.whut.my.caiji.ns.commons.persistence.BaseEntity;
import com.whut.my.caiji.ns.commons.utils.RegexpUtils;
import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@Builder
public class SysUser extends BaseEntity {


    private static final long serialVersionUID = -7770733966285161765L;
    private String username;

    //@NotNull
    @Pattern(regexp = RegexpUtils.PHONE, message = "手机号格式不正确")
    private String phone;

    @JsonIgnore
    private String password;

    private int enabled;

    private Long departmentId;

    private String detail;

    public SysUser(String username, @NotNull @Pattern(regexp = RegexpUtils.PHONE, message = "手机号格式不正确") String phone, String password, int enabled, Long departmentId, String detail) {
        this.username = username;
        this.phone = phone;
        this.password = password;
        this.enabled = enabled;
        this.departmentId = departmentId;
        this.detail = detail;
    }

    public SysUser(
            Long id,
            Date createdAt,
            Date updatedAt,
            String username,
            @NotNull @Pattern(regexp = RegexpUtils.PHONE, message = "手机号格式不正确") String phone,
            String password,
            int enabled,
            Long departmentId,
            String detail
    ) {
        super(id, createdAt, updatedAt);
        this.username = username;
        this.phone = phone;
        this.password = password;
        this.enabled = enabled;
        this.departmentId = departmentId;
        this.detail = detail;
    }

    @Override
    public String toString() {
        return "SysUser{" +
                "id=" + getId() +
                "username='" + username + '\'' +
                ", phone='" + phone + '\'' +
                ", password='" + password + '\'' +
                ", enabled=" + enabled +
                ", departmentId=" + departmentId +
                ", detail='" + detail + '\'' +
                ", createdAt=" + getCreatedAt() +
                ", updatedAt=" + getUpdatedAt() +
                '}';
    }
}
