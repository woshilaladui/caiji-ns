package com.whut.my.caiji.ns.dto.response.modle.user;

import com.whut.my.caiji.ns.dto.response.modle.BaseModel;
/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/10/26 10:55
 * @desription
 */
public class UserLoginLogModel extends BaseModel {



    public UserLoginLogBody data;



    public UserLoginLogModel() {
    }


    public UserLoginLogModel(UserLoginLogBody data) {
        this.data = data;
    }


    public UserLoginLogModel(int code, String msg, UserLoginLogBody data) {
        super(code, msg,data);
        //this.data = data;
    }

}
