package com.whut.my.caiji.ns.dto.response.modle.netty.im;

import java.io.Serializable;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2021/2/15 13:06
 * @desription
 */
public class ChatMsgBody implements Serializable {
    private Long senderId;//发送者id
    private Long receiverId;//接收者id
    private String msg;//聊天内容
    private Long msgId; //用于消息的签收

    public ChatMsgBody() {
    }

    public ChatMsgBody(Long senderId, Long receiverId, String msg, Long msgId) {
        this.senderId = senderId;
        this.receiverId = receiverId;
        this.msg = msg;
        this.msgId = msgId;
    }

    public Long getSenderId() {
        return senderId;
    }

    public void setSenderId(Long senderId) {
        this.senderId = senderId;
    }

    public Long getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(Long receiverId) {
        this.receiverId = receiverId;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Long getMsgId() {
        return msgId;
    }

    public void setMsgId(Long msgId) {
        this.msgId = msgId;
    }
}
