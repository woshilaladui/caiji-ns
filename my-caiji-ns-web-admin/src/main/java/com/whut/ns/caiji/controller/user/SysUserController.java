package com.whut.ns.caiji.controller.user;

import com.whut.my.caiji.ns.commons.constants.Constant;
import com.whut.my.caiji.ns.commons.persistence.BaseModel;

import com.whut.my.caiji.ns.domain.user.SysUser;
import com.whut.ns.caiji.annotation.OperationLog;
import com.whut.ns.caiji.web.service.user.auth.TbSysUserService;

import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/7/8 2:24
 * @desription
 */
@Controller
@RequestMapping("/sys/user")
@CrossOrigin("*")
public class SysUserController {

    @Resource
    private TbSysUserService tbSysUserService;

    @RequestMapping(value = "/deleteUser", method = RequestMethod.POST)
    @ResponseBody
    @OperationLog(operation = "删除用户")
    public BaseModel deleteUser(
            @RequestParam(required = true) Long userId
    ) {
        return tbSysUserService.deleteUser(userId);
    }

    @RequestMapping(value = "/addUser", method = RequestMethod.POST)
    @ResponseBody
    @OperationLog(operation = "添加新的用户")
    public BaseModel addUser(@Validated SysUser sysUser) {

        //System.out.println("sysUser = " + sysUser.toString());

        return tbSysUserService.preSave(sysUser);
    }

    @RequestMapping("/getAllUsers")
    @ResponseBody
    @OperationLog(operation = "获取全部用户信息")
    public BaseModel getAllUsers() {

        List<SysUser> sysUsers = tbSysUserService.selectAll();

        return new BaseModel(
                Constant.SUCESS,
                "获取全部用户成功",
                sysUsers
        );

    }

    @RequestMapping(value = "/updateUser", method = RequestMethod.POST)
    @ResponseBody
    @OperationLog(operation = "更新用户信息")
    public BaseModel updateUser(@Validated SysUser sysUser) {

        tbSysUserService.update(sysUser);

        return BaseModel.success("更新用户信息成功");

    }

    @RequestMapping(value = "/updateUserPassword", method = RequestMethod.POST)
    @ResponseBody
    @OperationLog(operation = "更新用户密码")
    public BaseModel updateUserPassword(
            Long id,
            String oldPassword,
            String newPassword
    ) {

        return tbSysUserService.updateUserPassword(
                id,
                oldPassword,
                newPassword
        );


    }

    @RequestMapping(value = "/lockUser", method = RequestMethod.POST)
    @ResponseBody
    @OperationLog(operation = "锁定用户")
    public BaseModel lockUser(
            @RequestParam(required = true) String username,
            @RequestParam(required = true) Integer enabled
    ) {


        return tbSysUserService.lockUser(username, enabled);

    }

}
