package com.whut.ns.caiji.controller.user;

import com.whut.my.caiji.ns.commons.constants.Constant;
import com.whut.my.caiji.ns.commons.persistence.BaseModel;
import com.whut.my.caiji.ns.commons.utils.ListUtils;
import com.whut.my.caiji.ns.domain.user.SysRole;
import com.whut.ns.caiji.annotation.OperationLog;
import com.whut.ns.caiji.web.service.user.auth.TbSysRoleService;
import com.whut.ns.caiji.web.service.user.auth.TbSysUserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/7/8 16:45
 * @desription
 */
@Controller
@RequestMapping("/sys/userRole")
@CrossOrigin("*")
public class SysUserRoleController {

    @Autowired
    private TbSysUserRoleService tbSysUserRoleService;

    @Autowired
    private TbSysRoleService tbSysRoleService;

    @RequestMapping("/getAllRoles")
    @ResponseBody
    @OperationLog(operation = "获取全部角色信息")
    public BaseModel getAllRoles(){
        return new BaseModel(
                Constant.SUCESS,
                "获取全部角色信息成功",
                tbSysRoleService.selectAll()
        );
    }

    @RequestMapping(value = "/updateRole",method = RequestMethod.POST)
    @ResponseBody
    @OperationLog(operation = "更新角色信息")
    public BaseModel updateRole(
            SysRole sysRole
    ){

        //System.out.println("sysRole = "+sysRole.toString());

        tbSysRoleService.update(sysRole);

        return BaseModel.success("更新角色 = "+sysRole.getRoleName()+"的角色信息成功");
    }


    @RequestMapping("/getRolesByUsername")
    @ResponseBody
    @OperationLog(operation = "获取该用户的角色信息")
    public BaseModel getRolesByUsername(
            @RequestParam String username
    ){

        List<SysRole> sysRoles = tbSysRoleService.getRoleByUsername(username);


        return new BaseModel(
                Constant.SUCESS,
                "获取角色 = "+username+" 的角色信息成功",
                sysRoles
        );
    }

    @ResponseBody
    @RequestMapping(value = "/setUserRoles",method = RequestMethod.POST)
    @OperationLog(operation = "给用户设置角色信息")
    public BaseModel setUserRoles(
            @RequestBody Map<String,Object> map
    ){
        Long userId = Long.valueOf((Integer) map.get("userId"));
        List<Integer> roleIds = (ArrayList<Integer>) map.get("roleIds");

        return tbSysUserRoleService.preSaveCollection(
                userId,
                ListUtils.changeIntegerListToLongList(roleIds)
        );

    }

}
