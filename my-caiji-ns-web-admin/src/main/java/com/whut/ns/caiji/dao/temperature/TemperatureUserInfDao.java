package com.whut.ns.caiji.dao.temperature;

import com.whut.my.caiji.ns.commons.persistence.BaseDao;
import com.whut.my.caiji.ns.domain.standard.Standard;
import com.whut.my.caiji.ns.domain.temperature.TemperatureUserInf;
import org.springframework.stereotype.Repository;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/11/5 15:43
 * @desription
 */
@Repository
public interface TemperatureUserInfDao extends BaseDao<TemperatureUserInf> {

    TemperatureUserInf getUerInfByNickName(String username);
}
