package com.whut.my.caiji.ns.dto.response.modle.user;

import lombok.Builder;

import java.io.Serializable;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/10/26 10:56
 * @desription
 */
public class UserLoginLogBody implements Serializable
{

    public String LastLoginDate;

    public Object todoList;



    public UserLoginLogBody() {
    }

    public UserLoginLogBody(String lastLoginDate) {
        LastLoginDate = lastLoginDate;
    }

    public UserLoginLogBody(String lastLoginDate, Object todoList) {
        LastLoginDate = lastLoginDate;
        this.todoList = todoList;
    }

    @Override
    public String toString() {
        return "UserLoginLogBody{" +
                "LastLoginDate='" + LastLoginDate + '\'' +
                ", todoList=" + todoList +
                '}';
    }
}
