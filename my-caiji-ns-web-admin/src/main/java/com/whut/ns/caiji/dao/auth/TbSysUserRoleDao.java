package com.whut.ns.caiji.dao.auth;

import com.whut.my.caiji.ns.commons.persistence.BaseDao;
import com.whut.my.caiji.ns.domain.user.SysUserRole;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/7/6 11:44
 * @desription
 */
@Repository
public interface TbSysUserRoleDao extends BaseDao<SysUserRole> {

    List<Long> selectUserRoleIdByUserId(Long userId);

    void deleteUserRoleByRoleIds(List<Long> roleIds,Long userId);

    Integer deleteUserRoleUserId(Long userId);

}
