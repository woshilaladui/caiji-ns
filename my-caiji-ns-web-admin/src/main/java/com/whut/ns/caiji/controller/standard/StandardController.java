package com.whut.ns.caiji.controller.standard;

import com.whut.my.caiji.ns.commons.constants.Constant;
import com.whut.my.caiji.ns.commons.persistence.BaseModel;
import com.whut.my.caiji.ns.commons.utils.IpUtils;
import com.whut.my.caiji.ns.domain.standard.Standard;
import com.whut.my.caiji.ns.dto.response.modle.standard.StandardBody;
import com.whut.my.caiji.ns.dto.response.modle.standard.StandardModel;
import com.whut.ns.caiji.annotation.OperationLog;
import com.whut.ns.caiji.web.service.standard.TbStandardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/7/14 16:25
 * @desription
 */
@Controller
@RequestMapping("/standard")
@CrossOrigin("*")
public class StandardController {

    @Resource
    private TbStandardService tbStandardService;

    @Autowired
    private HttpServletRequest httpServletRequest;

    /**
     * 获取全部标准信息
     * @return
     */
    @RequestMapping(value = "/getAllStandard")
    @ResponseBody
    @OperationLog(operation = "获取全部标准信息")
    public StandardModel getAllStandard(){

        return new StandardModel(
                Constant.SUCESS,
                "获取全部标准信息",
                new StandardBody(tbStandardService.selectAll())
        );
    }//end getAllStandard


    /**
     * 设置标准
     * @param data
     * @return
     */
    @RequestMapping(value = "/setStandard",method = RequestMethod.POST)
    @ResponseBody
    @OperationLog(operation = "设置合格标准")
    public BaseModel setStandard(@RequestBody List<Standard> data){

        return tbStandardService.preSave(data.get(0));

    }

    /**
     * 通过表名得到该表所有得标准
     * @param tableName
     * @return
     */

    @RequestMapping(value = "/getStandardsDataByTableName")
    @ResponseBody
    @OperationLog(operation = "通过表名来获取该表的标准信息")
    public BaseModel getStandardsDataByTableName( String tableName){

        return new BaseModel(
                Constant.SUCESS,
                "获取 "+tableName+" 表名的所有得标准",
                tbStandardService.getStandardsDataByTableName(tableName)
        );

    }


    /**
     * 通过表名来获取当前最新的标准
     * @param tableName
     * @return
     */
    @RequestMapping(value = "/getStandardDataByTableName")
    @ResponseBody
    @OperationLog(operation = "通过表名来获取该表当前最新的标准信息")
    public BaseModel getStandardDataByTableName(String tableName){
        return new BaseModel(
                Constant.SUCESS,
                "获取 "+tableName+" 表名当前最新的标准",
                tbStandardService.getStandardDataByTableName(tableName)
        );
    }


}
