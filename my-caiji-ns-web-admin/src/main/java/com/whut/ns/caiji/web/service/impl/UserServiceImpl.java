package com.whut.ns.caiji.web.service.impl;


import com.whut.my.caiji.ns.commons.persistence.BaseModel;
import com.whut.my.caiji.ns.dto.response.modle.user.UserBody;
import com.whut.my.caiji.ns.domain.user.TbUser;
import com.whut.my.caiji.ns.domain.user.permission.Permission;
import com.whut.ns.caiji.abstracts.AbstractBaseServiceImpl;
import com.whut.ns.caiji.dao.TbUserDao;
import com.whut.ns.caiji.web.service.TbUserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
@Transactional(readOnly = true)//除了查询不用开启事务其他都需要开启事务
public class UserServiceImpl extends AbstractBaseServiceImpl<TbUser,TbUserDao> implements TbUserService {
    


    @Override
    public UserBody login(String phone, String password) {




        TbUser user = dao.selectUserByPhone(phone);



        List<TbUser> userList = dao.selectAll();



        if (user != null && user.getPassword().equals(DigestUtils.md5DigestAsHex(password.getBytes()))) {//校验密码

            //生成token
            String token = jwtUtil.createJWT(user.getId().toString(), user.getUsername(), user.getAuthority());

            Date date = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy‐MM‐dd HH:mm:ss");


            return new UserBody(
                            user,
                            token
            );

        } else {   //end if

            return new UserBody(
                    null,
                    "用户或者密码错误"
            );

        }//end else


    }

    @Override
    public Permission getUserPermission(Long id) {
        return null;
    }

    @Override
    public BaseModel register(String username, String phone, String password) {
        return null;
    }

    @Override
    public BaseModel save(TbUser entity) {
        return null;
    }

    @Override
    public BaseModel saveCollection(List<TbUser> entities) {
        return null;
    }
}
