package com.whut.my.caiji.ns.domain.temperature;

import com.whut.my.caiji.ns.commons.persistence.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/11/5 15:41
 * @desription
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class TemperatureUserInf extends BaseEntity {

    private String nickName;
    private String email;

    public TemperatureUserInf(String nickName, String email) {
        this.nickName = nickName;
        this.email = email;
    }

    public TemperatureUserInf(Long id, Date createdAt, Date updatedAt, String nickName, String email) {
        super(id, createdAt, updatedAt);
        this.nickName = nickName;
        this.email = email;
    }

    public TemperatureUserInf(Date createdAt, Date updatedAt, String nickName, String email) {
        super(createdAt, updatedAt);
        this.nickName = nickName;
        this.email = email;
    }

    public TemperatureUserInf() {
    }


    @Override
    public String toString() {
        return "TemperatureUserInf{" +
                "nickName='" + nickName + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
