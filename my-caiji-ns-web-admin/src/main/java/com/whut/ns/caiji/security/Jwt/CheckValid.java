package com.whut.ns.caiji.security.Jwt;


import com.whut.my.caiji.ns.commons.constants.Constant;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

//JWT安全校验
@Component
public class CheckValid {

    @Autowired
    private HttpServletRequest request;

    //检验请求是否携带token
    public boolean isValid() {
        Claims claims = (Claims) request.getAttribute(Constant.CLAIMS);

        if (claims != null) {

            return true;//存在token
        }

        return false;
    }

    //返回当前用户的权限
    public int getRole() {
        Claims claims = (Claims) request.getAttribute(Constant.CLAIMS);
        return Integer.parseInt(claims.get("roles").toString());
    }


    public String getUser(){
        Claims claims = (Claims) request.getAttribute(Constant.CLAIMS);
        return claims.getSubject();
    }

    public int getUserId(){
        Claims claims = (Claims) request.getAttribute(Constant.CLAIMS);
        return Integer.parseInt(claims.get("userId").toString());
    }

}
