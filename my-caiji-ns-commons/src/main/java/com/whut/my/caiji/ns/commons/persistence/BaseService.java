package com.whut.my.caiji.ns.commons.persistence;






import java.util.List;

/**
 * 所有业务逻辑层的基类
 * <p>Title: BaseService</p>
 * <p>Description: </p>
 *
 * @author Lusifer
 * @version 1.0.0
 * @date 2020/5/30 11:55
 */
public interface BaseService<T extends BaseEntity> {
    /**
     * 查询全部
     * @return
     */
    public List<T> selectAll();

    /**
     * 保存信息
     * @param entity
     * @return
     */
    BaseModel save(T entity);


    /**
     * 保存集合信息
     * @param entities
     * @return
     */
    BaseModel saveCollection(List<T> entities);

    /**
     * 删除用户信息
     * @param id
     */
    Integer delete(Long id);

    /**
     * 根据 ID 获取信息
     * @param id
     * @return
     */
    T getById(Long id);

    /**
     * 更新信息
     * @param entity
     */
    Integer update(T entity);



    /**
     * 查询总笔数
     * @return
     */
    int count(T entity);
}
