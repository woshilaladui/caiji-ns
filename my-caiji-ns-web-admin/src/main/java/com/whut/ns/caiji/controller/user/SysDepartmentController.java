package com.whut.ns.caiji.controller.user;

import com.whut.my.caiji.ns.commons.constants.Constant;
import com.whut.my.caiji.ns.commons.persistence.BaseModel;
import com.whut.my.caiji.ns.domain.user.SysDepartment;
import com.whut.ns.caiji.annotation.OperationLog;
import com.whut.ns.caiji.web.service.user.auth.TbSysDepartmentService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/7/10 10:07
 * @desription
 */
@Controller
@RequestMapping("/sys/department")
@CrossOrigin("*")
public class SysDepartmentController {

    @Resource
    private TbSysDepartmentService tbSysDepartmentService;

    @RequestMapping("/getAllDepartments")
    @ResponseBody
    @OperationLog(operation = "获取全部部门信息")
    public BaseModel getAllDepartments(){

        List<SysDepartment> sysDepartments = tbSysDepartmentService.selectAll();

        return new BaseModel(
                Constant.SUCESS,
                "获取全部部门成功",
                sysDepartments
        );



    }

}
