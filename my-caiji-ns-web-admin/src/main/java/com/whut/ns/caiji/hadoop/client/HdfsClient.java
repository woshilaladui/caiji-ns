package com.whut.ns.caiji.hadoop.client;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.*;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/8/16 16:58
 * @desription
 */
public class HdfsClient {

    //private TextInputFormat


    private static FileSystem fs = null;
    private static Configuration conf = null;

    public static void main(String[] args) {

        System.out.println("");

        try {
            //testMkdir();
            putFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void initFileSystem() throws IOException {
        conf = new Configuration();

        conf.set("fs.defaultFS", "hdfs://10.120.20.226:8020");

        fs = FileSystem.get(conf);

    }

    /**
     *  API之创建目录
     * @throws IOException
     */
    public static void testMkdir() throws IOException {

        initFileSystem();

        //1. 测试创建目录，描述一个目录
        Path hdfsfile = new Path("/dir1");

        //2. 调用创建目录的方法

        fs.mkdirs(hdfsfile);

        fs.close();

        System.out.println("创建成功");

    }

    /**
     * API之删除目录
     * @throws IOException
     */
    public static void testDelete() throws IOException {


        System.out.println("");

        initFileSystem();

        //1. 测试删除目录，描述一个目录

        Path hdfsfile = new Path("/dir1");

        //2. 调用创建目录的方法

        fs.delete(hdfsfile, true);

        fs.close();

        System.out.println("删除成功");
    }

    /**
     * API之文件重命名
     * @throws IOException
     */
    public static void testRename() throws IOException {

        initFileSystem();

        //1. 测试重命名，将file1改为file01
        Path oldName = new Path("/file1");

        Path newName = new Path("/file01");

        //2.调用重命名方法
        fs.rename(oldName, newName);

        fs.close();

        System.out.println("命名成功");


    }

    /**
     * IOUtils上传文件
     * @throws IOException
     */
    public static void putFile() throws IOException {

        initFileSystem();

        // 创建输入流，读取输入文件
        FileInputStream input = new FileInputStream(new File("d://test.txt"));

        // 创建输出流
        FSDataOutputStream out = fs.create(new Path("/dir1/gg.txt"));

        //IO的流拷贝
        IOUtils.copyBytes(input, out, conf);

        //关闭资源
        IOUtils.closeStream(input);

        IOUtils.closeStream(out);

        System.out.println("上传完毕");

    }

    /**
     * IOUtils下载文件
     * @throws IOException
     */
    public static void getFile() throws IOException {

        initFileSystem();

        // 获取输入流 从HDFS上读取
        FSDataInputStream input = fs.open(new Path("/gg.txt"));

        // 获取输出流
        FileOutputStream out = new FileOutputStream(new File("c://gg.txt"));

        //流拷贝
        IOUtils.copyBytes(input, out, conf);

        //关闭流
        IOUtils.closeStream(input);

        IOUtils.closeStream(out);

        System.out.println("下载完成");


    }

    /**
     * API之文件状态
     * @throws IOException
     */
    public static void testFileStatus() throws IOException {

        initFileSystem();

        //1. 描述你要读取的文件 /file02
        Path path = new Path("/file02");

        //获取文件的状态信息
        RemoteIterator<LocatedFileStatus> it = fs.listLocatedStatus(path);

        while (it.hasNext()) {

            // 取出对象
            LocatedFileStatus status = it.next();

            System.out.println("name:" + status.getPath());

            //获取位置

            BlockLocation[] locate = status.getBlockLocations();
            for (BlockLocation bl : locate) {

                System.out.println("当前块的所有副本位置：" + Arrays.toString(bl.getHosts()));
                System.out.println("当前块大小：" + bl.getLength());
                System.out.println("当前块的副本的ip地址信息：" + Arrays.toString(bl.getNames()));

            }

            System.out.println("系统的块大小：" + status.getBlockSize());
            System.out.println("文件总长度：" + status.getLen());

        }

    }


}
