package com.whut.ns.caiji.web.service.impl.feedback;

import com.whut.my.caiji.ns.commons.email.SendEmail;
import com.whut.my.caiji.ns.commons.persistence.BaseModel;
import com.whut.my.caiji.ns.commons.utils.DateUtils;
import com.whut.my.caiji.ns.domain.feedback.FeedBack;

import com.whut.ns.caiji.abstracts.AbstractBaseServiceImpl;
import com.whut.ns.caiji.dao.feedback.FeedBackDao;

import com.whut.ns.caiji.web.service.feedback.FeedBackService;
import org.apache.commons.mail.EmailException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/11/10 9:39
 * @desription
 */
@Component
public class FeedBackServiceImp extends AbstractBaseServiceImpl<FeedBack, FeedBackDao> implements FeedBackService {

    @Autowired
    private SendEmail sendEmail;

    private final String TO = "782799981@qq.com";

    @Override
    public List<FeedBack> getAllFeedBackByUsernameOrTimeBetween(String username, Date startDate, Date endDate) {

        String disposeStartDate = "";
        String disposeEndDate = "";

        //返回
        if (startDate == null) {
            disposeStartDate = "1990-10-10";
        }

        if (endDate == null) {
            disposeEndDate = DateUtils.formatDate(DateUtils.getCurrentDate(), "YYYY-MM-dd");
        }

        return dao.getAllFeedBackByUsernameOrTimeBetween(
                "%" + username + "%",
                disposeStartDate,
                disposeEndDate
        );
    }

    @Override
    public BaseModel save(FeedBack entity) {

        entity.setCreatedAt(DateUtils.getCurrentDate());
        entity.setUpdatedAt(DateUtils.getCurrentDate());

        if (dao.insert(entity) > 0) {

            //插入成功后需要将反馈信息邮件给后端人员

            StringBuilder msg = new StringBuilder();

            msg
                    .append("问题分类：")
                    .append(entity.getClassification())
                    .append("\n")
                    .append("问题标题：")
                    .append(entity.getTitle())
                    .append("\n")
                    .append("问题描述：\n")
                    .append(entity.getContent());

            try {
                sendEmail.send(
                        "问题报告 " + entity.getTitle(),
                        msg.toString(),
                        TO
                );
            } catch (EmailException e) {
                e.printStackTrace();
                return BaseModel.error("插入失败");
            }

            return BaseModel.success("插入成功");
        }

        return BaseModel.error("插入失败");
    }

    @Override
    public BaseModel saveCollection(List<FeedBack> entities) {
        return null;
    }


}
