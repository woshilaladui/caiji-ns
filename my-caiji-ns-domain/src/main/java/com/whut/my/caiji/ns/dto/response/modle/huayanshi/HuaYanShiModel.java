package com.whut.my.caiji.ns.dto.response.modle.huayanshi;


import com.whut.my.caiji.ns.dto.response.modle.BaseModel;

public class HuaYanShiModel extends BaseModel {

    public HuaYanShiBody data;

    public HuaYanShiModel() {
    }

    public HuaYanShiModel(HuaYanShiBody data) {
        this.data = data;
    }

    public HuaYanShiModel(int code, String msg, HuaYanShiBody data) {
        super(code, msg);
        this.data = data;
    }

    public HuaYanShiModel(int code, String msg, String controllerName, HuaYanShiBody data) {
        super(code, msg, controllerName);
        this.data = data;
    }

    public HuaYanShiBody getData() {
        return data;
    }

    public void setData(HuaYanShiBody data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "HuaYanShiRequestModel{" +
                "data=" + data +
                '}';
    }
}
