package com.whut.my.caiji.ns.commons.utils;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/7/10 11:17
 * @desription
 */
public class EncrtptionUtils {

    private static BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    public static String encondeByBCrypt(String password){


        return passwordEncoder.encode(password);
    }

    public static boolean isMatach(String rawPassword, String encodedPassword){
        return passwordEncoder.matches(rawPassword,encodedPassword);
    }



}
