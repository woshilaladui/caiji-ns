package com.whut.my.caiji.ns.dto.response.modle.logRecord;

import java.util.Date;

public class LogRecordBaseModel {

    private Integer id;

    private String username;

    private Date time;

    private String operation;

    private String controller;

    public LogRecordBaseModel() {
    }

    public LogRecordBaseModel(Integer id, String username, Date time, String operation, String controller) {
        this.id = id;
        this.username = username;
        this.time = time;
        this.operation = operation;
        this.controller = controller;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getController() {
        return controller;
    }

    public void setController(String controller) {
        this.controller = controller;
    }

    @Override
    public String toString() {
        return "LogRecordBaseModel{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", time=" + time +
                ", operation='" + operation + '\'' +
                ", controller='" + controller + '\'' +
                '}';
    }
}
