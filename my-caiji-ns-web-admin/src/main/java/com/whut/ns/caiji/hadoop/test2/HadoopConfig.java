package com.whut.ns.caiji.hadoop.test2;

import org.apache.hadoop.fs.FileSystem;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.URI;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/10/5 10:40
 * @desription
 */
//@Configuration
public class HadoopConfig {

    @Bean("fileSystem")
    public FileSystem createFs(){
        //读取配置文件
        org.apache.hadoop.conf.Configuration conf = new org.apache.hadoop.conf.Configuration();
        conf.set("dfs.replication", "1");
        conf.set("dfs.client.use.datanode.hostname", "true");
        System.setProperty("HADOOP_USER_NAME","root");
        FileSystem fs = null;
        try {
            URI uri = new URI("hdfs://10.120.20.226:8020/");
            fs = FileSystem.get(uri,conf);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  fs;
    }

}
