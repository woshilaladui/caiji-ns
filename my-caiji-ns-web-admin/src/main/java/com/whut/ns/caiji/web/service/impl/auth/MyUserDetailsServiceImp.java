package com.whut.ns.caiji.web.service.impl.auth;

import com.whut.ns.caiji.dao.auth.TbSysPermissionDao;
import com.whut.ns.caiji.dao.auth.TbSysRoleDao;
import com.whut.ns.caiji.dao.auth.TbSysUserDao;
import com.whut.ns.caiji.security.auth.MyUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class MyUserDetailsServiceImp implements UserDetailsService {

    @Value("${server.isServer}")
    private Boolean isServer;
    //private Boolean isServer = true;

    @Value("${server.warName}")
    private String warName;
    //private String warName = "/caiji_v4.3";



    @Autowired
    private TbSysUserDao tbSysUserDao;

    @Autowired
    private TbSysRoleDao tbSysRoleDao;

    @Autowired
    private TbSysPermissionDao tbSysPermissionDao;



    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {


        //1.加载基础用户信息
        MyUserDetails myUserDetails = tbSysUserDao.selectByUserName(username);


        //2.加载用户角色列表
        List<String> roleCodes = tbSysRoleDao.selectRoleCodeByUserName(username);

        //System.out.println(roleCodes.toString());

        //角色是一个特殊权限 ROLE_admin(Spring security中识别ROLE_)

        //3.通过用户角色列表加载用户资源权限列表
        //角色是一个特殊的权限，ROLE_前缀

        List<String> authorities = tbSysPermissionDao.selectAuthorityByRoleCode(roleCodes);

        //System.out.println("authorities1 = "+ authorities.toString());


        roleCodes = roleCodes.stream()
                .map(rc -> "ROLE_" +rc)
                .collect(Collectors.toList());

        authorities.addAll(roleCodes);

        if(isServer){
            authorities = authorities.stream()
                    .map(authority -> warName+ authority)
                    .collect(Collectors.toList());
        }

        //System.out.println("authorities2 = "+ authorities.toString());

        myUserDetails.setAuthorities(
                AuthorityUtils.commaSeparatedStringToAuthorityList(
                        String.join(",",authorities)
                )
        );


        return myUserDetails;
    }
}
