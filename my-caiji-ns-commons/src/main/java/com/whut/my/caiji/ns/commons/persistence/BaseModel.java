package com.whut.my.caiji.ns.commons.persistence;

import com.whut.my.caiji.ns.commons.constants.Constant;
import lombok.*;

/**
 * @author zm
 * @className BaseModel
 * @Date 2019/8/22 9:18
 * @Version 1.0
 * @Description
 **/
@Data
@Builder
@ToString(callSuper = true)
@NoArgsConstructor
//@AllArgsConstructor
public class BaseModel {

    //状态代码 方便前端匹配错误码表
    public int code;

    //可读性错误信息
    public String msg;

    public Object data;

    public BaseModel(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public BaseModel(int code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public static BaseModel success(String msg){

        BaseModel baseModel = new BaseModel();
        baseModel.code = Constant.SUCESS;
        baseModel.msg = msg;
        baseModel.data = null;

        return baseModel;

    }

    public static BaseModel error(String msg){

        BaseModel baseModel = new BaseModel();
        baseModel.code = Constant.ERROR;
        baseModel.msg = msg;
        baseModel.data = null;

        return baseModel;

    }


}
