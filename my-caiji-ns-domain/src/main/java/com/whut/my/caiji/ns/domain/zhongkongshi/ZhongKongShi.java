package com.whut.my.caiji.ns.domain.zhongkongshi;


import com.whut.my.caiji.ns.commons.persistence.BaseEntity;
import com.whut.my.caiji.ns.domain.user.TbUser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;


/**
 * 中控室的实体类与数据的映射关系
 *
 *
 * @author nianshao zm
 * @date 2020/5/27
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ZhongKongShi extends BaseEntity {


    private static final long serialVersionUID = 1329792221836907609L;
    private Date date;

    private Integer index;//下标

    private Long departmentId;//该行数据属于哪个部门

    private String tableName;

    private String data;

    private Long userId;

    public ZhongKongShi(Long id,  Date date, Integer index, Long departmentId, String tableName, String data, Long userId,Date createdAt, Date updatedAt) {
        super(id, createdAt, updatedAt);
        this.date = date;
        this.index = index;
        this.departmentId = departmentId;
        this.tableName = tableName;
        this.data = data;
        this.userId = userId;
    }

    public Integer getIndex() {
        return index;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public void setDepartmentId(Long departmentId) {
        this.departmentId = departmentId;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public void setData(String data) {
        this.data = data;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "ZhongKongShi{" +
                "id=" + getId() +
                "date=" + date +
                ", index=" + index +
                ", departmentId=" + departmentId +
                ", tableName='" + tableName + '\'' +
                ", data='" + data + '\'' +
                ", createdAt='" + getCreatedAt() + '\'' +
                ", updatedAt='" + getUpdatedAt() + '\'' +
                ", userId=" + userId +
                '}';
    }
}
