package com.whut.ns.caiji.web.service.impl.auth;

import com.whut.my.caiji.ns.commons.constants.Constant;
import com.whut.my.caiji.ns.commons.persistence.BaseModel;
import com.whut.my.caiji.ns.commons.utils.DateUtils;
import com.whut.my.caiji.ns.domain.user.SysUserRole;
import com.whut.ns.caiji.abstracts.AbstractBaseServiceImpl;
import com.whut.ns.caiji.annotation.Write;
import com.whut.ns.caiji.dao.auth.TbSysRoleDao;
import com.whut.ns.caiji.dao.auth.TbSysUserDao;
import com.whut.ns.caiji.dao.auth.TbSysUserRoleDao;
import com.whut.ns.caiji.security.auth.jwt.JwtTokenUtil;
import com.whut.ns.caiji.web.service.user.auth.TbSysUserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/7/8 11:47
 * @desription
 */
@Service
//@Transactional(readOnly = true)
public class TbSysUserRoleServiceImp extends AbstractBaseServiceImpl<SysUserRole, TbSysUserRoleDao> implements TbSysUserRoleService {


    @Autowired
    private TbSysRoleDao tbSysRoleDao;


    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private TbSysUserDao tbSysUserDao;


    @Write
    @Override
    public BaseModel save(SysUserRole entity) {
        return null;
    }

    //@Transactional(readOnly = false)
    @Write
    @Override
    public BaseModel preSaveCollection(
            Long userId,
            List<Long> userRoleIds
    ) {


        Long departmentId = tbSysUserDao.selectDepartmentIdByUserId(userId);


        if (isInvalidate(departmentId, userRoleIds)) {
            /**
             * 可能是移除 也可能是新添加
             * 1.查询该userId 的所有roleIds
             * 2.对比这两个集合
             */

            //从数据库查询到的
            List<Long> dateBaseUserRoleIds = dao.selectUserRoleIdByUserId(userId);

            //需要插入的id
            List<Long> addUserRoles = getAddIds(
                    dateBaseUserRoleIds,
                    userRoleIds
            );


            //需要删除的id
            List<Long> deleteUserRole = getDeleteIds(
                    dateBaseUserRoleIds,
                    userRoleIds
            );

            //删除
            if (deleteUserRole != null && deleteUserRole.size() > 0) {
                dao.deleteUserRoleByRoleIds(deleteUserRole, userId);
            }


            //新增
            List<SysUserRole> sysUserRoles = new ArrayList<>();
            for (int i = 0; i < addUserRoles.size(); i++) {
                sysUserRoles.add(
                        new SysUserRole(
                                userId,
                                addUserRoles.get(i),
                                DateUtils.getCurrentDate(),
                                DateUtils.getCurrentDate()
                        )
                );
            }


            return saveCollection(sysUserRoles);
        } else {
            return BaseModel.error("非法设置，请设置合法角色");
        }


    }

    private boolean isInvalidate(Long departmentId, List<Long> userRoleIds) {


        //化验室分配化验室底下的权限
        if (departmentId.equals(Constant.DEPARTMENT_HUAYS)) {


            for (Long userRoleId : userRoleIds) {

                if ((userRoleId >= Constant.YG_START && userRoleId <= Constant.HYS_END) || userRoleId == Constant.DUTY_HYS_MANAGER)
                    continue;
                return false;
            }

            return true;

        } else if (departmentId.equals(Constant.DEPARTMENT_ZHKONGKS)) {

            for (Long userRoleId : userRoleIds) {

                if ((userRoleId >= Constant.ZK_START && userRoleId <= Constant.ZK_END) || userRoleId == Constant.DUTY_ZK_MANAGER || userRoleId == Constant.DUTY_ZK_ENGINEER)
                    continue;

                return false;
            }

            return true;
        } else if (departmentId.equals(Constant.DEPARTMENT_ZHONGBU)) {

        } else {

        }

        return true;
    }


    @Override
    //@Transactional(readOnly = false)
    @Write
    public BaseModel saveCollection(List<SysUserRole> entities) {

        if (entities != null && entities.size() > 0) {
            dao.insertAll(entities);

            return BaseModel.success("更新用户id = " + entities.get(0).getSysUserId() + "的角色信息成功");
        }

        return BaseModel.success("更新角色信息成功");


    }
}
