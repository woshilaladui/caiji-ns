package com.whut.ns.caiji.web.test;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2021/4/7 20:58
 * @desription
 */
public class Main {

    public static void main(String[] args) {
        // 获取 Spring 容器
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring-context.xml");



        // 从 Spring 容器中获取对象
        UserServiceImpl userService = (UserServiceImpl) applicationContext.getBean("userService");
        userService.sayHi();

        System.out.println(userService.getA());
        System.out.println(userService.getB());
    }
}
