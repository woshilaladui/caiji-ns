package com.whut.my.caiji.ns.domain.temperature;

import com.whut.my.caiji.ns.commons.persistence.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/11/5 15:39
 * @desription
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class TemperatureCallback extends BaseEntity {

    private String nickName;
    private Date date;
    int flag;

    public TemperatureCallback() {
    }

    public TemperatureCallback(String nickName, Date date, int flag) {
        this.nickName = nickName;
        this.date = date;
        this.flag = flag;
    }

    public TemperatureCallback(Long id, Date createdAt, Date updatedAt, String nickName, Date date, int flag) {
        super(id, createdAt, updatedAt);
        this.nickName = nickName;
        this.date = date;
        this.flag = flag;
    }

    public TemperatureCallback(Date createdAt, Date updatedAt, String nickName, Date date, int flag) {
        super(createdAt, updatedAt);
        this.nickName = nickName;
        this.date = date;
        this.flag = flag;
    }


    @Override
    public String toString() {
        return "TemperatureCallback{" +
                "nickName='" + nickName + '\'' +
                ", date=" + date +
                ", flag=" + flag +
                '}';
    }
}
