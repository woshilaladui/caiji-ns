package com.whut.ns.caiji.hadoop.utils;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;

import java.io.IOException;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/8/16 16:48
 * @desription
 */
public class HdfsUtils {


    /**
     * 获取FileSystem
     *
     * @return fs
     */
    public static FileSystem getFileSystem() {
        Configuration configuration = new Configuration();

        //第一种方式:修改hadoop_username 为root用户
        System.setProperty("HADOOP_USER_NAME", "root");

        //若没有指定上传路径则默认上传到本地项目所属盘符的根目录
        configuration.set("fs.defaultFS", "hdfs://101.200.149.190:8020");

        FileSystem fs = null;

        try {
            fs = FileSystem.get(configuration);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("获取hdfs对象失败 = " + e);
        }

        return fs;
    }

    /**
     * 关闭FileSystem
     * @param fileSystem
     */
    public static void closeFileSystem(FileSystem fileSystem) {

        if (fileSystem != null) {
            try {
                fileSystem.close();
            } catch (IOException e) {
                e.printStackTrace();

                System.out.println("关闭失败 = " + e);
            }
        }

    }

}
