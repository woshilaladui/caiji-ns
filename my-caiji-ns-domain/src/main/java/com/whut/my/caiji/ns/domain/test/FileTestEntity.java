//package com.whut.my.caiji.ns.domain.test;
//
//@Entity  //告诉JPA这是一个实体类
//@Table(name = "zhongkongshi")
//public class FileTestEntity {
//
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Column(name = "id")
//    private Integer id;
//
//    @Column(name = "content")
//    private String content;
//
//    public FileTestEntity() {
//    }
//
//    public Integer getId() {
//        return id;
//    }
//
//    public void setId(Integer id) {
//        this.id = id;
//    }
//
//    @Override
//    public String toString() {
//        return "FileTestEntity{" +
//                "id=" + id +
//                ", content='" + content + '\'' +
//                '}';
//    }
//
//    public String getContent() {
//        return content;
//    }
//
//    public void setContent(String content) {
//        this.content = content;
//    }
//}
