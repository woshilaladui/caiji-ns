package com.whut.ns.caiji.dao.netty.im;



import com.whut.my.caiji.ns.domain.netty.im.ChatMsg;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface ChatMsgMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ChatMsg record);

    int insertSelective(ChatMsg record);

    ChatMsg selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ChatMsg record);

    int updateByPrimaryKey(ChatMsg record);

    List<ChatMsg> getUnReadMsgListByAcceptUid(Long acceptUserId);

    //批量签收
    void batchUpdateMsgSigned(List<Long> msgIdList);
}