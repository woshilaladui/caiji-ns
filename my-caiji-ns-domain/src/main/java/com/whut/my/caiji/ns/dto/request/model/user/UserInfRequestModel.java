package com.whut.my.caiji.ns.dto.request.model.user;

public class UserInfRequestModel {


    public String username;
    public String phone;
    public String password;
    public int state;
    public int department;
    public int duty;
    public int authority;

    public UserInfRequestModel() {
    }

    public UserInfRequestModel(
            String username,
            String phone,
            String password,
            int state,
            int department,
            int duty,
            int authority
    ) {
        this.username = username;
        this.phone = phone;
        this.password = password;
        this.state = state;
        this.department = department;
        this.duty = duty;
        this.authority = authority;
    }

    public int getAuthority() {
        return authority;
    }

    public void setAuthority(int authority) {
        this.authority = authority;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getDepartment() {
        return department;
    }

    public void setDepartment(int department) {
        this.department = department;
    }

    public int getDuty() {
        return duty;
    }

    public void setDuty(int duty) {
        this.duty = duty;
    }

    @Override
    public String toString() {
        return "UserInfRequestModel{" +
                "username='" + username + '\'' +
                ", phone='" + phone + '\'' +
                ", password='" + password + '\'' +
                ", state=" + state +
                ", department=" + department +
                ", duty=" + duty +
                ", permission=" + authority +
                '}';
    }
}
