package com.whut.my.caiji.ns.commons.exception.hanlder;

import com.whut.my.caiji.ns.commons.constants.Constant;
import com.whut.my.caiji.ns.commons.exception.CustomException;
import com.whut.my.caiji.ns.commons.persistence.BaseModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.session.SessionAuthenticationException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

//import javax.servlet.http.HttpServletRequest;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/7/13 10:24
 * @desription
 */
@RestControllerAdvice
//@Slf4j
public class CustomExceptionHanlder {

    @ExceptionHandler(SessionAuthenticationException.class)
    public BaseModel handlerSessionAuthenticationException(
            SessionAuthenticationException exception
    ){
        System.out.println("exception = "+ exception.getMessage());
        return new BaseModel(
                Constant.ERROR,
                "handlerAuthenticationException" + exception.getMessage(),
                null
        );
    }


}
