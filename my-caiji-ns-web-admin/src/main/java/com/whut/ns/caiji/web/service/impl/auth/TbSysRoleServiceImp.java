package com.whut.ns.caiji.web.service.impl.auth;

import com.whut.my.caiji.ns.commons.persistence.BaseModel;
import com.whut.my.caiji.ns.domain.user.SysRole;
import com.whut.ns.caiji.abstracts.AbstractBaseServiceImpl;
import com.whut.ns.caiji.annotation.Read;
import com.whut.ns.caiji.annotation.Write;
import com.whut.ns.caiji.dao.auth.TbSysRoleDao;
import com.whut.ns.caiji.web.service.user.auth.TbSysRoleService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/7/8 2:28
 * @desription
 */
@Service
public class TbSysRoleServiceImp extends AbstractBaseServiceImpl<SysRole, TbSysRoleDao> implements TbSysRoleService {
    @Write
    @Override
    public BaseModel save(SysRole entity) {

        if(entity != null){

            dao.insert(entity);

            return BaseModel.success("保存成功");

        }else {
            return BaseModel.success("保存识别");
        }
    }


    @Override
    @Write
    public BaseModel saveCollection(List<SysRole> entities) {
        return null;
    }

    @Override
    @Read
    public List<SysRole> getRoleByUsername(String username) {
        return dao.selectRoleByUserName(username);
    }
}
