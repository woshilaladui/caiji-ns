package com.whut.ns.caiji.web.service.impl.standard;

import com.whut.my.caiji.ns.commons.persistence.BaseModel;
import com.whut.my.caiji.ns.commons.utils.DateUtils;
import com.whut.my.caiji.ns.domain.standard.Standard;
import com.whut.ns.caiji.abstracts.AbstractBaseServiceImpl;
import com.whut.ns.caiji.annotation.Read;
import com.whut.ns.caiji.annotation.Write;
import com.whut.ns.caiji.dao.standard.TbStandardDao;
import com.whut.ns.caiji.web.service.standard.TbStandardService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/7/14 17:42
 * @desription
 */
@Service
//@Transactional(readOnly = true)
public class TbStandardServiceImp extends AbstractBaseServiceImpl<Standard, TbStandardDao> implements TbStandardService {

    /**
     * 获取改表全部历史记录
     * @param tableName
     * @return
     */
    @Override
    //@Transactional(readOnly = true)
    @Read
    public List<Standard> getStandardsDataByTableName(String tableName) {
        return dao.getStandardsDataByTableName(tableName);
    }

    @Override
    //@Transactional(readOnly = true)
    @Read
    public Standard getStandardDataByTableName(String tableName) {
        return dao.getStandardDataByTableName(tableName);
    }

    @Override
    //@Transactional(readOnly = false)
    @Write
    public BaseModel preSave(Standard standard) {

        //System.out.println("setCreatedAt = " + standard.toString());




            standard.setCreatedAt(DateUtils.getCurrentDate());


        if(standard.getReason() == null)
            standard.setReason("");

        return save(standard);
    }

    @Override
    //@Transactional(readOnly = false)
    @Write
    public BaseModel save(Standard entity) {

        dao.insert(entity);

        return BaseModel.success("保存 "+entity.getTableName()+" 表的标准信息成功");
    }

    @Override
    @Write
    public BaseModel saveCollection(List<Standard> entities) {
        return null;
    }
}
