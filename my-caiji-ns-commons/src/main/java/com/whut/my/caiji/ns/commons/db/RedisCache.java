package com.whut.my.caiji.ns.commons.db;


import com.whut.my.caiji.ns.commons.context.ApplicationContextHolder;
import org.apache.ibatis.cache.Cache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Redis 缓存工具类
 * <p>Title: RedisCache</p>
 * <p>Description: </p>
 *
 * @author Lusifer
 * @version 1.0.0
 * @date 2018/8/13 6:03
 */
public class RedisCache implements Cache {
    private static final Logger logger = LoggerFactory.getLogger(RedisCache.class);



    //private static final Logger log = LoggerFactory.getLogger(MybatisRedisCache.class);
    private String id;
    private final ReadWriteLock readWriteLock = new ReentrantReadWriteLock();
    private static final long EXPIRE_TIME_IN_MINUTES = 30; // redis过期时间


    public RedisCache(String id) {
        this.id = id;
    }

    public RedisCache(){}

    private RedisTemplate<Object, Object> getRedisTemplate() {
        return ApplicationContextHolder.getBean("redisTemplate");
    }



    @Override
    public String getId() {
        return id;
    }

    public void  putObject(Object key,Object value,Integer minutes){
        try {
            RedisTemplate<Object, Object> redisTemplate = getRedisTemplate();
            ValueOperations<Object, Object> opsForValue = redisTemplate.opsForValue();
            opsForValue.set(key, value, minutes, TimeUnit.MINUTES);
            //System.out.println("Put query result to redis");
        } catch (Throwable t) {
            //System.out.println("Redis put failed = " + t);
        }
    }

    @Override
    public void putObject(Object key, Object value) {

//        System.out.println("key = " + key.toString());
//        System.out.println("value = " + value.toString());
        try {
            RedisTemplate redisTemplate = getRedisTemplate();
            ValueOperations opsForValue = redisTemplate.opsForValue();
            opsForValue.set(key, value, 30, TimeUnit.MINUTES);
            //System.out.println("Put query result to redis");
        } catch (Throwable t) {
            //System.out.println("Redis put failed = " + t);
        }

    }

    @Override
    public Object getObject(Object key) {

        try {
            RedisTemplate redisTemplate = getRedisTemplate();
            ValueOperations opsForValue = redisTemplate.opsForValue();
            //System.out.println("Get cached query result from redis");
//            System.out.println("****" + opsForValue.get(key).toString());
            return opsForValue.get(key);
        } catch (Throwable t) {
            //System.out.println("Redis get failed, fail over to db  = " + t);
            return null;
        }
    }

    @Override
    public Object removeObject(Object key) {
        RedisTemplate redisTemplate = getRedisTemplate();
        Object value = redisTemplate.boundHashOps(getId()).delete(key);
        //System.out.println("[从缓存删除了: " + key + "=" + value + " ]");

        return value;
    }

    @Override
    public void clear() {
//        RedisTemplate redisTemplate = getRedisTemplate();
//        redisTemplate.delete(getId());
        RedisTemplate redisTemplate = getRedisTemplate();
        redisTemplate.execute((RedisCallback) connection -> {
            connection.flushDb();
            return null;
        });
        //System.out.println("清空缓存!!!");

    }

    @Override
    public int getSize() {
        RedisTemplate redisTemplate = getRedisTemplate();
        Long size = redisTemplate.boundHashOps(getId()).size();
        return size == null ? 0 : size.intValue();
    }

    @Override
    public ReadWriteLock getReadWriteLock() {
        return readWriteLock;
    }
}

