package com.whut.ns.caiji.security.auth.jwt;


import com.whut.my.caiji.ns.commons.constants.Constant;
import com.whut.my.caiji.ns.commons.exception.CustomException;
import com.whut.my.caiji.ns.commons.persistence.BaseModel;
import com.whut.my.caiji.ns.commons.utils.DateUtils;
import com.whut.my.caiji.ns.domain.log.TbLogRecord;
import com.whut.my.caiji.ns.domain.user.SysRole;
import com.whut.my.caiji.ns.domain.user.SysUser;

import com.whut.my.caiji.ns.dto.response.modle.user.UserBody;
import com.whut.my.caiji.ns.dto.response.modle.user.UserLoginLogBody;
import com.whut.my.caiji.ns.dto.response.modle.user.UserLoginLogModel;
import com.whut.my.caiji.ns.dto.response.modle.user.UserModel;
import com.whut.ns.caiji.annotation.OperationLog;
import com.whut.ns.caiji.web.service.log.OperationLogService;
import com.whut.ns.caiji.web.service.user.auth.TbSysRoleService;
import com.whut.ns.caiji.web.service.user.auth.TbSysUserService;
import org.apache.commons.lang.ArrayUtils;
import org.apache.hadoop.yarn.webapp.hamlet.Hamlet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.*;
import java.util.concurrent.locks.Condition;

@RestController
@CrossOrigin("*")
public class JwtAuthController {


    @Autowired
    private JwtAuthService jwtAuthService;

    @Resource
    private TbSysUserService userService;

    @Resource
    private OperationLogService operationLogService;

    @Autowired
    private TbSysRoleService tbSysRoleService;


    //token持续时间
    @Value("${jwt.expiration}")
    private Long expiration;

    @RequestMapping(value = "/authentication")
    @ResponseBody
    @OperationLog(operation = "登陆系统",tag = "登陆系统")
    public UserModel login(
            String username,
            String password
    ){
        if(StringUtils.isEmpty(username)
                || StringUtils.isEmpty(password)){
            return new UserModel(
                    Constant.ERROR,
                    "用户名或者密码不能为空",
                    null
            );

        }
        try {

            String token = jwtAuthService.login(username, password);

            SysUser sysUser = userService.getUserByUsername(username);


            TbLogRecord tbLogRecord = operationLogService.getLastLoginLog(username,"注销用户");

            String lastLogin = "";
            if(tbLogRecord == null)
                lastLogin = "暂无上次登陆时间";
            else
                lastLogin = DateUtils.formatDate(operationLogService.getLastLoginLog(username,"注销用户").getTime(),"yyyy-MM-dd HH:mm:ss");



            UserLoginLogBody userLoginLogBody = new UserLoginLogBody(
                    lastLogin,
                    "暂无待办事项"
            );



            Date date = new Date(System.currentTimeMillis() + expiration);

            //System.out.println("登陆时间 = "+DateUtils.formatDate(date,DateUtils.YYYY_MM_DD_HH_mm_ss));


            List<SysRole> sysRoles = tbSysRoleService.getRoleByUsername(username);
            Long[] roles = new Long[sysRoles.size()];

            for(int i=0;i< sysRoles.size();i++){
                roles[i] = sysRoles.get(i).getId();
            }

            Arrays.sort(roles);

            return new UserModel(
                    Constant.SUCESS,
                    "登陆系统成功",
                    new UserBody(
                            sysUser,
                            token,
                            date,
                            userLoginLogBody,
                            roles
                    )
            );

        }catch (CustomException e){
            return new UserModel(
                    Constant.ERROR,
                    "登陆失败 用户名或者密码错误",
                    null
            );
        }
    }

    @RequestMapping(value = "/refreshtoken")
    public  BaseModel refresh(@RequestHeader("${jwt.header}") String token){

        try {
            String refreshToken = jwtAuthService.refreshToken(token);
            return new BaseModel(
                    Constant.SUCESS,
                    "刷新令牌成功",
                    refreshToken
            );
        }catch (CustomException e){
            return new BaseModel(
                    Constant.SUCESS,
                    "刷新令牌失败 "+ e.getMessage(),
                    null
            );
        }

    }

    @RequestMapping(value = "/invalidateToken")
    @OperationLog(operation = "注销用户")
    public  BaseModel invalidateToken(@RequestHeader("${jwt.header}") String token){

        return new BaseModel(
                Constant.SUCESS,
                "注销成功",
                jwtAuthService.invalidateToken(token)
        );

    }

    public UserLoginLogModel getUserLoginLog(){
        return null;
    }



}
