package com.whut.my.caiji.ns.dto.response.modle.logRecord;


import com.whut.my.caiji.ns.dto.response.modle.BaseModel;

public class LogRecordModel extends BaseModel {
    public LogRecordBody data;

    public LogRecordModel() {
    }

    public LogRecordModel(LogRecordBody data) {
        this.data = data;
    }

    public LogRecordModel(int code, String msg, LogRecordBody data) {
        super(code, msg,data);
    }




}
