package com.whut.ns.caiji.web.service.log;

import com.whut.my.caiji.ns.commons.persistence.BaseDao;
import com.whut.my.caiji.ns.commons.persistence.BaseService;
import com.whut.my.caiji.ns.domain.log.TbLogRecord;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/7/30 17:39
 * @desription
 */
@Service
public interface OperationLogService extends BaseService<TbLogRecord> {

    List<TbLogRecord> getAllByUsernameOrTimeBetween(
            String username,
            String startDate,
            String endDate
    );

    boolean deleteOperationLogs(List<Long> operationIds);


    /**
     *
     * @param username 用户名
     * @param tag 执行日志的简化名称如：注销登陆 登陆系统等
     * @return TbLogRecord
     */
    TbLogRecord getLastLoginLog(String username,String tag);

    /**
     * 删除N个月之前的日志
     * @param N N个月之前
     * @return
     */
    Integer deleteLogNMonthsAgo(int N);


}
