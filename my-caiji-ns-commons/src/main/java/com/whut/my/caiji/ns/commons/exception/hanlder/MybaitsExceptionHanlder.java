package com.whut.my.caiji.ns.commons.exception.hanlder;

import com.whut.my.caiji.ns.commons.constants.Constant;
import com.whut.my.caiji.ns.commons.persistence.BaseModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataAccessException;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

//import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/7/10 10:38
 * @desription
 */
@RestControllerAdvice
@Slf4j
public class MybaitsExceptionHanlder {


    /**
     * 捕获Mybatis存储过程的异常
     * @param request
     * @param exception
     * @return
     */
//    @ExceptionHandler(DataAccessException.class)
//    public BaseModel handleDataAccessException(
//            HttpServletRequest request,
//            DataAccessException exception
//    ) {
//
//        log.error(exception.getMessage());
//
//        return new BaseModel(
//                Constant.ERROR,
//                "数据异常，数据库中不存在该数据 \n"+exception.getMessage(),
//                null
//        );
//    }
}
