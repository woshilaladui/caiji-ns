package com.whut.ns.caiji.security.auth.jwt;

import com.whut.my.caiji.ns.commons.exception.CustomException;
import com.whut.my.caiji.ns.commons.exception.CustomExceptionType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class JwtAuthService {

    @Resource
    AuthenticationManager authenticationManager;

    @Resource
    UserDetailsService userDetailsService;

    @Autowired
    JwtTokenUtil jwtTokenUtil;

    /**
     * 登录认证换取JWT令牌
     * @return JWT
     */
    public String login(String username,String password) throws CustomException {


        try {
            UsernamePasswordAuthenticationToken upToken =
                    new UsernamePasswordAuthenticationToken(
                            username, password
                    );


            //认证
            Authentication authentication = authenticationManager.authenticate(upToken);
            SecurityContextHolder.getContext().setAuthentication(authentication);
        }catch (AuthenticationException e){
            //认证失败抛出自定义异常

            System.out.println("异常 = "+e.getMessage());

            throw new CustomException(CustomExceptionType.USER_INPUT_ERROR
                            ,e.getMessage());
        }

        UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        return jwtTokenUtil.generateToken(userDetails);
    }

    public String invalidateToken(String token){
        return jwtTokenUtil.invalidateToken(token);
    }

    public String refreshToken(String oldToken) throws CustomException{

            if(!jwtTokenUtil.isTokenExpired(oldToken)){
                return jwtTokenUtil.refreshToken(oldToken);
            }

        return null;
    }



}
