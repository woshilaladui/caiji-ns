package com.whut.my.caiji.ns.dto.response.modle.netty.im;



import java.io.Serializable;

public class DataContent implements Serializable {
    private Integer action;//动作类型
    private ChatMsgBody chatMsgBody;//用户的聊天内容
    private String extand;//扩展字段

    public Integer getAction() {
        return action;
    }

    public void setAction(Integer action) {
        this.action = action;
    }


    public ChatMsgBody getChatMsgBody() {
        return chatMsgBody;
    }

    public void setChatMsgBody(ChatMsgBody chatMsgBody) {
        this.chatMsgBody = chatMsgBody;
    }

    public String getExtand() {
        return extand;
    }

    public void setExtand(String extand) {
        this.extand = extand;
    }
}
