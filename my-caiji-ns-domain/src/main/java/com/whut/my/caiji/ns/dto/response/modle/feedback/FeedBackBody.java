package com.whut.my.caiji.ns.dto.response.modle.feedback;

import com.whut.my.caiji.ns.domain.feedback.FeedBack;

import java.util.List;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/11/10 9:54
 * @desription
 */
public class FeedBackBody {

    List<FeedBack> feedBacks;

    public FeedBackBody() {
    }

    public FeedBackBody(List<FeedBack> feedBacks) {
        this.feedBacks = feedBacks;
    }

    public List<FeedBack> getFeedBacks() {
        return feedBacks;
    }

    public void setFeedBacks(List<FeedBack> feedBacks) {
        this.feedBacks = feedBacks;
    }

    @Override
    public String toString() {
        return "FeedBackBody{" +
                "feedBacks=" + feedBacks +
                '}';
    }
}
