package com.whut.my.caiji.ns.dto.response.modle.standard;


import java.io.Serializable;

public class StandardBody implements Serializable {

    Object standards;

    public StandardBody() {
    }

    public StandardBody(Object standards) {
        this.standards = standards;
    }

    public Object getStandards() {
        return standards;
    }

    public void setStandards(Object standards) {
        this.standards = standards;
    }


}
