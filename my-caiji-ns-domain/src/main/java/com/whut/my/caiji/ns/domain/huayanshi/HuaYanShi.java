package com.whut.my.caiji.ns.domain.huayanshi;

import com.whut.my.caiji.ns.commons.persistence.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Date;


/**
 * 化验室的实体类和表的映射
 *
 *
 * @author nianshao zm
 * @date 2020/5/27
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class HuaYanShi extends BaseEntity {


    private static final long serialVersionUID = -3267303808924356164L;
    private Date date;

    private Integer index;//下标

    private Long departmentId;//该行数据属于哪个部门

    private String tableName;

    private String data;

    private Long userId;

    //实际上是standard表中的id下标
    private Long standardId;

    public HuaYanShi(
            Long id,
            Date date,
            Integer index,
            Long departmentId,
            String tableName,
            String data,
            Long userId,
            Long standardId,
            Date createdAt,
            Date updatedAt
    ) {
        super(id, createdAt, updatedAt);
        this.date = date;
        this.index = index;
        this.departmentId = departmentId;
        this.tableName = tableName;
        this.data = data;
        this.userId = userId;
        this.standardId = standardId;
    }

    //自定义要重写，不能用原来的toString
    @Override
    public String toString() {
        return "HuaYanShi{" +
                "id=" + getId() +
                "date=" + date +
                ", index=" + index +
                ", departmentId=" + departmentId +
                ", tableName='" + tableName + '\'' +
                ", data='" + data + '\'' +
                ", userId=" + userId +
                ", standardId=" + standardId +
                ", createdAt=" + getCreatedAt() +
                ", updatedAt=" + getUpdatedAt() +
                '}';
    }
}
