package com.whut.my.caiji.ns.dto.response.modle.permission;

import java.util.List;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/11/18 15:49
 * @desription
 */
public class PermissionModel {

    private Integer id;
    private String name;
    private String request;
    private String type;
    private List<PermissionModel> children;

    public PermissionModel() {
    }

    public PermissionModel(Integer id, String name, String request, String type, List<PermissionModel> children) {
        this.id = id;
        this.name = name;
        this.request = request;
        this.type = type;
        this.children = children;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<PermissionModel> getChildren() {
        return children;
    }

    @Override
    public String toString() {
        return "PermissionModel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", request='" + request + '\'' +
                ", type='" + type + '\'' +
                ", children=" + children +
                '}';
    }

    public void setChildren(List<PermissionModel> children) {
        this.children = children;
    }
}
