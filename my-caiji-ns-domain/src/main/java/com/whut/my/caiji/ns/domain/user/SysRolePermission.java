package com.whut.my.caiji.ns.domain.user;

import com.whut.my.caiji.ns.commons.persistence.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class SysRolePermission extends BaseEntity {


    private static final long serialVersionUID = 6805391529670263761L;
    private Long sysRoleId;

    private Long sysPermissionId;

    public SysRolePermission( Long sysRoleId, Long sysPermissionId,Date createdAt, Date updatedAt) {
        super(createdAt, updatedAt);
        this.sysRoleId = sysRoleId;
        this.sysPermissionId = sysPermissionId;
    }

    @Override
    public String toString() {
        return "SysRolePermission{" +
                "id=" + getId() +
                "sysRoleId=" + sysRoleId +
                ", sysPermissionId=" + sysPermissionId +
                ", createdAt=" + getCreatedAt() +
                ", updatedAt=" + getUpdatedAt() +
                '}';
    }
}
