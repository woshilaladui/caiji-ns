package com.whut.my.caiji.ns.domain.log;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.whut.my.caiji.ns.commons.persistence.BaseEntity;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * @author nianshao zm
 * @date 2020//5/27
 * 日志
 */
@Data
@Builder
@EqualsAndHashCode(callSuper = true)
public class TbLogRecord extends BaseEntity {


    private static final long serialVersionUID = -4710802655852623246L;
    private String operation;

    private String ip;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date time;

    private String username;

    private String tag;

    public TbLogRecord() {
    }

    public TbLogRecord(String operation, String ip, Date time, String username) {
        this.operation = operation;
        this.ip = ip;
        this.time = time;
        this.username = username;
    }

    public TbLogRecord(
            Long id,
            String operation,
            String ip,
            Date time,
            String username,
            Date createdAt,
            Date updatedAt
    ) {
        super(id, createdAt, updatedAt);
        this.operation = operation;
        this.ip = ip;
        this.time = time;
        this.username = username;
    }

    public TbLogRecord(
            String operation,
            String ip,
            Date time,
            String username,
            Date createdAt,
            Date updatedAt
    ) {
        super(createdAt, updatedAt);
        this.operation = operation;
        this.ip = ip;
        this.time = time;
        this.username = username;
    }

    public TbLogRecord(String operation, String ip, Date time, String username, String tag) {
        this.operation = operation;
        this.ip = ip;
        this.time = time;
        this.username = username;
        this.tag = tag;
    }

    public TbLogRecord(Long id, Date createdAt, Date updatedAt, String operation, String ip, Date time, String username, String tag) {
        super(id, createdAt, updatedAt);
        this.operation = operation;
        this.ip = ip;
        this.time = time;
        this.username = username;
        this.tag = tag;
    }

    public TbLogRecord(Date createdAt, Date updatedAt, String operation, String ip, Date time, String username, String tag) {
        super(createdAt, updatedAt);
        this.operation = operation;
        this.ip = ip;
        this.time = time;
        this.username = username;
        this.tag = tag;
    }

    @Override
    public String toString() {
        return "TbLogRecord{" +
                "id=" + getId() +
                "operation='" + operation + '\'' +
                ", ip='" + ip + '\'' +
                ", time=" + time +
                ", username='" + username + '\'' +
                ", createdAt=" + getCreatedAt() +
                ", updatedAt=" + getUpdatedAt() +
                '}';
    }
}
