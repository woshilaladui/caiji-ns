package com.whut.my.caiji.ns.domain.db;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/7/28 17:04
 * @desription
 */
public enum DBTypeEnum {
    /**
     * 表示主数据库
     */
    MASTER,

    /**
     * 表示从数据库
     */
    SLAVE;

}
