package com.whut.my.caiji.ns.dto.response.modle.logRecord;



import com.whut.my.caiji.ns.domain.log.TbLogRecord;


import java.util.List;

public class LogRecordBody {

    public List<TbLogRecord> logRecord;

    public LogRecordBody() {
    }

    public LogRecordBody(List<TbLogRecord> logRecord) {
        this.logRecord = logRecord;
    }

    public List<TbLogRecord> getLogRecord() {
        return logRecord;
    }

    public void setLogRecord(List<TbLogRecord> logRecord) {
        this.logRecord = logRecord;
    }

    @Override
    public String toString() {
        return "LogRecordBody{" +
                "logRecord=" + logRecord +
                '}';
    }
}
