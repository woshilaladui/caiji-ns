package com.whut.ns.caiji.dao;




import com.whut.my.caiji.ns.commons.persistence.BaseDao;
import com.whut.my.caiji.ns.domain.user.TbUser;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

@Repository
@Component
public interface TbUserDao extends BaseDao<TbUser> {

    //TbUser findUserByUsername(String username);

    TbUser selectUserByPhone(String phone);

}
