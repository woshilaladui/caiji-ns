package com.whut.my.caiji.ns.dto.response.modle.huayanshi;

import com.whut.my.caiji.ns.domain.huayanshi.HuaYanShi;


/**
 * 待用 可以用也可以不用
 * 嵌套多的情况可以开启
 */
public class HuaYanShiBaseModel {
    private HuaYanShi huaYanShi;

    public HuaYanShiBaseModel() {
    }

    public HuaYanShiBaseModel(HuaYanShi huaYanShi) {
        this.huaYanShi = huaYanShi;
    }

    public HuaYanShi getHuaYanShi() {
        return huaYanShi;
    }

    public void setHuaYanShi(HuaYanShi huaYanShi) {
        this.huaYanShi = huaYanShi;
    }
}
