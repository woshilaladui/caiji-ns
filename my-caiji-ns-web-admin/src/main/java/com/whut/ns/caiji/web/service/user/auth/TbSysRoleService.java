package com.whut.ns.caiji.web.service.user.auth;

import com.whut.my.caiji.ns.commons.persistence.BaseService;
import com.whut.my.caiji.ns.domain.user.SysPermission;
import com.whut.my.caiji.ns.domain.user.SysRole;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/7/6 15:31
 * @desription
 */
@Service
public interface TbSysRoleService extends BaseService<SysRole> {

    List<SysRole> getRoleByUsername(String username);

}
