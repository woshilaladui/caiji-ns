package com.whut.ns.caiji.web.service.user.auth;

import com.whut.my.caiji.ns.commons.persistence.BaseModel;
import com.whut.my.caiji.ns.commons.persistence.BaseService;
import com.whut.my.caiji.ns.domain.user.SysUser;
import org.springframework.stereotype.Service;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/7/6 15:31
 * @desription
 */
@Service
public interface TbSysUserService extends BaseService<SysUser> {

    BaseModel preSave(SysUser sysUser);

    BaseModel lockUser(String username, Integer enabled);

    BaseModel deleteUser(Long userId);

//    BaseModel updatePassword(String password);
//
//    BaseModel updatePhone(String phone);

    SysUser getUserByUsername(String username);

    /**
     * @param phone
     * @return
     */
    Boolean isPhoneExist(String phone);

    Boolean isUsernameExist(String username);

    BaseModel updateUserPassword(
            Long id,
            String oldPassword,
            String newPassword
    );


}
