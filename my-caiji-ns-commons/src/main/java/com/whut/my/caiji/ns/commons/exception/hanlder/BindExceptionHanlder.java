package com.whut.my.caiji.ns.commons.exception.hanlder;

import com.whut.my.caiji.ns.commons.persistence.BaseModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

//import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/7/9 17:33
 * @desription
 */

@RestControllerAdvice
@Slf4j
public class BindExceptionHanlder {

    /**
     * 捕获 validated 校验失败的异常
     * @param request
     * @param exception
     * @return
     */
    @ExceptionHandler(BindException.class)
    public BaseModel handleBindException(HttpServletRequest request, BindException exception) {
        List<FieldError> allErrors = exception.getFieldErrors();
        StringBuilder sb = new StringBuilder();
        for (FieldError errorMessage : allErrors) {
            sb.append(errorMessage.getField()).append(": ").append(errorMessage.getDefaultMessage()).append(", ");
        }
//        System.out.println(sb.toString());

        log.error(exception.getMessage());
        return BaseModel.error(sb.toString());
    }

}
