package com.whut.ns.caiji.web.service.user.auth;

import com.whut.my.caiji.ns.commons.persistence.BaseService;
import com.whut.my.caiji.ns.domain.user.SysPermission;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/7/6 15:34
 * @desription
 */
@Service
public interface TbSysPermissionService extends BaseService<SysPermission> {

     List<SysPermission> getPermissionByUsername(String username);

}
