package com.whut.ns.caiji.hadoop.join;

import org.apache.commons.beanutils.BeanUtils;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/11/2 10:36
 * @desription
 */
public class Test {

    public static void main(String[] args) throws InvocationTargetException, IllegalAccessException {

        Iterable<User> users;

        List<User> userList = new ArrayList<>();
        User user1 = new User(1,"a");
        User user2 = new User(2,"b");
        User user3 = new User(3,"c");
        userList.add(user1);
        userList.add(user2);
        userList.add(user3);
        users = userList;

        List<User> temp = userList;

        List<User> temp2  = new ArrayList<>();

        for (User user : temp) {

            User tempuser = new User();
            BeanUtils.copyProperties(tempuser, user);


            temp2.add(tempuser);
        }



//        for (User user : users) {
//
//            temp.add(user);
//
//        }

        System.out.println(temp.size());
        for (User user : temp) {
            System.out.println(user.toString());
        }
        System.out.println("---------------------");

        user1.setUsername("aa");


        for (User user : temp) {
            System.out.println(user.toString());
        }
        System.out.println("---------------------");
        System.out.println("size = " + temp2.size());

        for (User user : temp2) {
            System.out.println(user.toString());
        }
    }
}
