package com.whut.ns.caiji.web.service.impl.huayanshi;

import com.whut.my.caiji.ns.commons.constants.Constant;
import com.whut.my.caiji.ns.commons.constants.TableConstant;
import com.whut.my.caiji.ns.commons.persistence.BaseModel;
import com.whut.my.caiji.ns.commons.utils.DateUtils;
import com.whut.my.caiji.ns.domain.huayanshi.HuaYanShi;
import com.whut.my.caiji.ns.domain.standard.Standard;
import com.whut.my.caiji.ns.domain.zhongkongshi.ZhongKongShi;
import com.whut.my.caiji.ns.dto.response.modle.huayanshi.HuaYanShiBody;
import com.whut.my.caiji.ns.dto.response.modle.huayanshi.HuaYanShiModel;
import com.whut.ns.caiji.abstracts.AbstractBaseServiceImpl;
import com.whut.ns.caiji.annotation.Read;
import com.whut.ns.caiji.annotation.Write;
import com.whut.ns.caiji.dao.huayanshi.TbHuaYanShiDao;
import com.whut.ns.caiji.dao.standard.TbStandardDao;
import com.whut.ns.caiji.web.service.huayanshi.TbHuaYanShiService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/7/15 10:53
 * @desription
 */
@Service
//@Transactional(readOnly = true)
public class TbHuaYanShiServiceImp extends AbstractBaseServiceImpl<HuaYanShi, TbHuaYanShiDao> implements TbHuaYanShiService {

    public int getA(){
        return 1;
    }

    @Resource
    private TbStandardDao tbStandardDao;

    @Override
    //@Transactional(readOnly = true)
    @Read
    public List<HuaYanShi> getHuaYanShiDataByTableNameAndDate(String tableName, Date date) {
        return dao.getHuaYanShiDataByTableNameAndDate(tableName, DateUtils.formatDate(date, "yyyy-MM-dd"));
    }

    @Override
    @Write
    public BaseModel preSaveCollection(List<HuaYanShi> sourceHuaYanShis) {

        HuaYanShi one = sourceHuaYanShis.get(0);

        String date = DateUtils.parseDate(one.getDate(), "YYYY-MM-dd");
        String tableName = one.getTableName();

        List<Integer> indexs = dao.getHuaYanShiCurrentDateIndex(tableName, date);

        /**
         * 对比 sourceHuaYanShis B和 dataBaseHuaYanShis A的 id编号
         *
         * 错误：
         * B 中传递过来的数据中有id == null 字段的则为需要插入的数据
         * B 中 id != null的则为需要更新的数据
         *
         * 还需要判断 是否该表存在标准
         *
         * 正确：
         * 还要判断index字段是否重复
         *
         * 场景:用户第一次点击提交没有刷新界面此时页面该行数据还是没有id 再点提交该行数据应该做
         * 的是更新而不是插入
         *
         */

        Standard standard = tbStandardDao.getStandardDataByTableName(tableName);


        boolean standardFlag = isHaveStandard(tableName);

        //如果该表有标准，但是数据库没查到标准则返回错误，设置标准后再提交
        //有标准
        if (standardFlag) {

            if (standard == null)
                return BaseModel.error("缺少标准，请先设置标准");
        }

        //有标准则开始分离出哪些需要更新，哪些需要插入
        List<HuaYanShi> addHuaYanShis = new ArrayList<>();
        List<HuaYanShi> updateHuaYanShis = new ArrayList<>();

        /**
         * 分离出哪些需要插入哪些需要更新
         */
        sourceHuaYanShis.forEach(
                huaYanShi -> {
                    if (!indexs.contains(huaYanShi.getIndex())) {

                        if (standard != null)
                            huaYanShi.setStandardId(standard.getId());
                        else
                            huaYanShi.setStandardId((long) Integer.MAX_VALUE);

                        huaYanShi.setCreatedAt(DateUtils.getCurrentDate());
                        huaYanShi.setUpdatedAt(DateUtils.getCurrentDate());
                        addHuaYanShis.add(huaYanShi);
                    } else {
                        huaYanShi.setUpdatedAt(DateUtils.getCurrentDate());
                        updateHuaYanShis.add(huaYanShi);
                    }

                }
        );

        //更新
        if (updateHuaYanShis.size() > 0) {
            dao.updateHuaYanShis(updateHuaYanShis);
        }


        return saveCollection(addHuaYanShis);
    }

    @Override
    public HuaYanShiModel getHuaYanShiDataDifferenceValueBytableNameAndDate(
            String tableName,
            Date today,
            Date yesterday
    ) {
        //
        List<HuaYanShi> huaYanShis = doGetHuaYanShiDataDifferenceValueBytableNameAndDate(tableName, today, yesterday);

        //两天数据都全
        if (huaYanShis.size() == 2) {

            String[] todayData = huaYanShis.get(0).getData().split(",");
            String[] yesterdayData = huaYanShis.get(1).getData().split(",");

            int length = todayData.length;
            String[] result;
            //如果是每月电量表 需要额外字段
            /**
             * 头=35KW表的607-窑头进线柜
             * 尾=35KW表的603-窑尾进线柜
             * 原料=35KW表的606-原料变压器
             */
            if (TableConstant.Eletri_ME.equals(tableName)) {
                result = new String[length + 3];
            } else {
                result = new String[length];
            }

            //计算两天数据的差值
            calculateDifference(length, result, todayData, yesterdayData);


            /**
             * 判断是每月电量表还是35KW表
             * 每月电量需要特殊处理
             *
             *头=35KW表的607-窑头进线柜
             *尾=35KW表的603-窑尾进线柜
             *原料=35KW表的606-原料变压器
             *
             */
            if (TableConstant.Eletri_ME.equals(tableName)) {

                calculateExternalMEValue(length, today, yesterday, result);

            }else {
                calculateExternalTFKTValue(length,result,todayData);
            }


            //拼接数据
            HuaYanShi huaYanShi = new HuaYanShi();
            huaYanShi.setData(String.join(",", result));
            huaYanShi.setIndex(0);


            List<HuaYanShi> differenceValueResultList = new ArrayList<>();

            differenceValueResultList.add(huaYanShi);

            return new HuaYanShiModel(
                    Constant.SUCESS,
                    "获取两天差值成功",
                    new HuaYanShiBody(differenceValueResultList)
            );


        } else {
            return new HuaYanShiModel(
                    Constant.SUCESS,
                    "获取失败，需要today数据和yesterday数据",
                    null
            );
        }

    }


    private List<HuaYanShi> doGetHuaYanShiDataDifferenceValueBytableNameAndDate(
            String tableName,
            Date today,
            Date yesterday
    ) {

        return dao.getHuaYanShiDataDifferenceValueBytableNameAndDate(
                tableName,
                DateUtils.formatDate(today, "YYYY-MM-dd"),
                DateUtils.formatDate(yesterday, "YYYY-MM-dd")
        );
    }

    private void calculateExternalTFKTValue(
            int length,
            String[] result,
            String[] todayData
    ){
        result[length-1] = todayData[length-1];
    }

    private void calculateExternalMEValue(
            int length,
            Date today,
            Date yesterday,
            String[] result
    ) {
        //获取35KW表两天的差值
        List<HuaYanShi> EletriTFKTList = doGetHuaYanShiDataDifferenceValueBytableNameAndDate(
                TableConstant.Eletri_TFKT,
                today,
                yesterday
        );

        double front, back;
        String[] todayDataTFKT = EletriTFKTList.get(0).getData().split(",");

        String[] yesterdayDataTFKT = EletriTFKTList.get(1).getData().split(",");

        String[] EletriResult = new String[length];

        //是Eletri_35KV这个表的length
        calculateDifference(yesterdayDataTFKT.length, EletriResult, todayDataTFKT, yesterdayDataTFKT);

        front = Double.parseDouble(EletriResult[10]);
        back = Double.parseDouble(result[0]);

        result[18] = String.valueOf((front - back));

        front = Double.parseDouble(EletriResult[7]);
        back = Double.parseDouble(result[7]);
        result[19] = String.valueOf((front - back));

        front = Double.parseDouble(EletriResult[9]);
        back = Double.parseDouble(result[15]);
        result[20] = String.valueOf((front - back));
    }

    private void calculateDifference(
            int length,
            String[] result,
            String[] todayData,
            String[] yesterdayData
    ) {

        for (int i = 0; i < length; i++) {
            if (!todayData[i].equals("") && !yesterdayData[i].equals("")) {
                result[i] = (Integer.parseInt(todayData[i]) - Integer.parseInt(yesterdayData[i])) + "";
            } else if (todayData[i].equals("") && yesterdayData[i].equals("")) {
                result[i] = "";
            } else {
                result[i] = todayData[i].equals("") ? "-" + yesterdayData[i] : todayData[i];
            }
        }//end for
    }


    @Override
    @Write
    public BaseModel save(HuaYanShi entity) {
        return null;
    }

    @Override
    @Write
    public BaseModel saveCollection(List<HuaYanShi> entities) {

        if (entities.size() > 0) {
            dao.insertAll(entities);
            return BaseModel.success("保存" + entities.get(0).getTableName() + "化验室数据成功");
        }

        return BaseModel.success("保存化验室数据成功");
    }


    private boolean isHaveStandard(String tableName) {

        switch (tableName) {
            //分析表格
            case TableConstant.Analysis_SHS:
            case TableConstant.Analysis_SY:
            case TableConstant.Analysis_TF:
            case TableConstant.Analysis_FMHg:
            case TableConstant.Analysis_FMHs:
            case TableConstant.Analysis_BS:
            case TableConstant.Analysis_RY:
            case TableConstant.Analysis_CYA:
            case TableConstant.Analysis_CCA:
            case TableConstant.Analysis_CYT:
            case TableConstant.Analysis_CCT:
            case TableConstant.Analysis_JCM:
            case TableConstant.Analysis_SMA:
            case TableConstant.Analysis_MFA:
            case TableConstant.Limestone_CaCO3://荧光分析表格
            case TableConstant.Limestone_CRO:
            case TableConstant.Eletri_ME://电量表格
            case TableConstant.Eletri_TFKT:
                return false;

            //荧光分析表格
            case TableConstant.Limestone_SHS:
            case TableConstant.Limestone_SY:
            case TableConstant.Limestone_TF:
            case TableConstant.Limestone_FMHg:
            case TableConstant.Limestone_FMHs:
            case TableConstant.Limestone_CRM:
            case TableConstant.Limestone_RMC:
            case TableConstant.Limestone_KAS:
            case TableConstant.Limestone_FAS:
                return true;
            default:
                return false;
        }

    }
}
