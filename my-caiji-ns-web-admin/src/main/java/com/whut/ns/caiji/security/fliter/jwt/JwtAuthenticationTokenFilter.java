package com.whut.ns.caiji.security.fliter.jwt;

import com.whut.my.caiji.ns.commons.exception.CustomException;
import com.whut.my.caiji.ns.commons.exception.CustomExceptionType;
import com.whut.ns.caiji.security.auth.jwt.JwtTokenUtil;
import com.whut.ns.caiji.web.service.impl.auth.MyUserDetailsServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.access.ExceptionTranslationFilter;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.annotation.Resource;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class JwtAuthenticationTokenFilter extends OncePerRequestFilter {

    @Autowired
    JwtTokenUtil jwtTokenUtil;

    @Autowired
    MyUserDetailsServiceImp myUserDetailsServiceImp;

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain filterChain)
            throws ServletException, IOException ,CustomException{



        String jwtToken = request.getHeader(jwtTokenUtil.getHeader());


        if(!StringUtils.isEmpty(jwtToken)){
            String username = jwtTokenUtil.getUsernameFromToken(jwtToken);

            //如果被security认证过就直接跳过一系列的校验
            if(username != null &&
                    SecurityContextHolder.getContext().getAuthentication() == null){

                //MyUserDetailsServiceImp myUserDetailsServiceImp = new MyUserDetailsServiceImp();

                UserDetails userDetails = myUserDetailsServiceImp.loadUserByUsername(username);

                if(jwtTokenUtil.validateToken(jwtToken,userDetails)){
                    //给使用该JWT令牌的用户进行授权
                    UsernamePasswordAuthenticationToken authenticationToken
                            = new UsernamePasswordAuthenticationToken(userDetails,null,userDetails.getAuthorities());


                    SecurityContextHolder.getContext().setAuthentication(authenticationToken);

                    //SecurityContextPersistenceFilter
                }else {

                    //token 无效
                    throw new CustomException(CustomExceptionType.TOKEN_INVALID_ERROR,"token过期");


                }
            }
        }

        filterChain.doFilter(request,response);
    }
}
