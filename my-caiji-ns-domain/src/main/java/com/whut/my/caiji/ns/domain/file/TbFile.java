package com.whut.my.caiji.ns.domain.file;


/**
 * 文件
 *
 *
 * @author nianshao zm
 * @date 2020/5/27
 *
 */
public class TbFile {

    //@Column(name = "id")
    private Integer id;


    //@Column(name = "t_file_name")
    private String fileName;

    //@Column(name = "t_download_path")
    private String downloadPath;

    //@Column(name = "t_desription")
    private String description;

    //@Email 校验邮箱
    //@Max(value = 11)限制长度
    //@Length 限定长度
    //@NotEmpty 判断字符串是否为空不去掉首尾空格
//    @NotBlank//不能为空 去掉首尾的空格
//    @Column(name = "t_date")
    private String Date;

//    @NotBlank//不能为空
//    @Column(name = "t_user_id")
    private int userId;

    public TbFile() {
    }

    public TbFile(
            String fileName,
            String downloadPath,
            String description,
            String date,
            int userId
    ) {
        this.fileName = fileName;
        this.downloadPath = downloadPath;
        this.description = description;
        Date = date;
        this.userId = userId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getDownloadPath() {
        return downloadPath;
    }

    public void setDownloadPath(String downloadPath) {
        this.downloadPath = downloadPath;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "TbFile{" +
                "id=" + id +
                ", fileName='" + fileName + '\'' +
                ", downloadPath='" + downloadPath + '\'' +
                ", description='" + description + '\'' +
                ", Date='" + Date + '\'' +
                ", userId=" + userId +
                '}';
    }
}
