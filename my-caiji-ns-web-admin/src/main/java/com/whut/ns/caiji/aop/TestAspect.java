package com.whut.ns.caiji.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.aop.ClassFilter;
import org.springframework.aop.support.NameMatchMethodPointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2021/5/14 19:46
 * @desription
 */
@Aspect
@Component
public class TestAspect {
    @Pointcut(value = "execution(* com.whut.ns.caiji.aop.Add.*(..)) && args(com.whut.ns.caiji.aop.User)")
    public void execMethod1() {
    }
//
//    @Pointcut("args(com.whut.ns.caiji.aop.User)")
//    public void execMethod2(){
//
//    }
//    @Before(value = "execMethod2()")
//    public void exec2BeforeAdvice(){
//        System.out.println("aaaaaaaaa");
//    }


    @Around(value = "execMethod1()")
    public void execAroundAdvice(ProceedingJoinPoint joinPoint) throws Throwable {

        System.out.println("1");


        Object proceed = joinPoint.proceed();

        System.out.println(4);
    }

    @After(value = "execMethod1()")
    public void execAfterAdvice(JoinPoint joinPoint) {
        System.out.println(5);
        int a = 1 / 0;
    }

    @AfterThrowing(
            pointcut = "execution(boolean *.execute(String,..))",
            throwing = "e"
    )
    public void afterThrowing(RuntimeException e) {

    }


    @AfterReturning(value = "execMethod1()")
    public void execAfterReturningAdvice() {
        System.out.println(6 + " 正常 ");
    }

    @AfterThrowing(value = "execMethod1()")
    public void execAfterThrowingAdvice() {
        System.out.println(6 + " 异常 ");
    }


    @Before(value = "execMethod1()")
    public void execBeforeAdvice(JoinPoint joinPoint) {
        System.out.println(2);
    }
}
