package com.whut.ns.caiji.dao.auth;

import com.whut.my.caiji.ns.commons.persistence.BaseDao;
import com.whut.my.caiji.ns.domain.user.SysRole;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/7/6 11:38
 * @desription
 */
@Repository
public interface TbSysRoleDao extends BaseDao<SysRole> {

    //查询角色role_code
    List<String> selectRoleCodeByUserName(String username);

    List<Long> selectRoleIdByUserName(String username);

    //selectRoleByUserName

    List<SysRole> selectRoleByUserName(String username);


}
