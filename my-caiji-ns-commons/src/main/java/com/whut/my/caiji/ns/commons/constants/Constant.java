package com.whut.my.caiji.ns.commons.constants;

/**
 * @author zm
 * @className Constant
 * @Date 2019/8/22 9:31
 * @Version 1.0
 * @Description
 **/

public class Constant {

    /**
     *
     */
    public static final int MESSAGE_BOARD = 1;// 定义留言板的to_user

    public static final int SUCESS = 0;

    //返回结果成功，但是今天没有数据
    public static final int SUCCESS_NO_DATA = 400;

    public static final int ERROR = 1;

    public static final int FILE_FLAG_EXCEL = 0;//excel

    public static final int FILE_FLAG_PDF = 1;

    public static final String CAPTCHA_SESSION_KEY = "captcha_key";
    public static final String SMS_SESSION_KEY = "sms_key";

    /**********************************************************************/
    //token 校验先前码
    public static final String AUTH_HEDAER_AUTHORIZATION = "nianshao ";//length = 9
    public static final int PREVIOUS_CODE = 9;
    public static final String CLAIMS = "claims";//全局存储token
    public static final int EXPIRE = 1000*60*60*6;//6个小时过期


    /**********************************************************************/
    public static final String UPLOAD_URL = "/root/home/admin/files/shuini/pdfs";


    /**********************************************************************/
    public static final int STATE_ON = 1;//在职
    public static final int STATE_OFF = 0;//离职


    /*****************************权限分配**********************************/



    //type 权限
    public static final long AUTHORITY_NONE = 0L;//无  游客
    public static final long AUTHORITY_MANAGER = 1L;//总经理
    public static final long AUTHORITY_SECTION_MANAGER = 2L;//部门经理
    public static final long AUTHORITY_EMPLOYEE = 3L;//员工
    //public static final int AUTHORITY_OPERATOR = 1;//操作员
//    public static final int AUTHORTIY_ENGINEER = 2;//工程师
//    public static final int AUTHORITY_DIRECTOR = 3;//主任
//    public static final int AUTHORITY_MANAGER = 4;//经理



    /**********************************************************************/

    //具体职务
    public static final long DUTY_NONE = 0L;
    //可以填写并查看中控室的所有表格，拥有中控室人员管理权限，中控室指标设置权限。
    public static final long DUTY_ZK_MANAGER = 1L;//中控室主任
    //可以查看中控室的所有表格，拥有中控室部门的所有查看权限。
    public static final long DUTY_ZK_ENGINEER = 2L;//总工程师

    //可以填写并查看化验室的所有表格，拥有化验室人员管理权限，化验室指标设置权限。
    public static final long DUTY_HYS_MANAGER = 3L;//化验室主任

    //超级管理员拥有各种权限
    public static final long DUTY_ADMIN = 4L;

    //各操作员可以在自己值班期间填写和修改对应班次的中控室操作记录，同时拥有查询所有中控室表格的权限。
    public static final long DUTY_EMPLOYEE_ZK_OPERATOR = 50L;//中控室操作员
    //对于特殊的数据，在手机web上进行填写和修改。
    public static final long DUTY_EMPLOYEE_SD_OPERATOR = 51L;//实地操作员(仅针对手机web版本)

    public static final long ZK_START = 50L;

    public static final long ZK_END = 60L;

    //各操作员可以在自己值班期间填写和修改对应班次的化验室荧光-分析部门-的所有表格，并能查询所有化验室表格。
    public static final long DUTY_EMPLOYEE_YG_ANALYST = 61L;//化验室荧光分析员
    //各操作员可以在自己值班期间填写和修改对应班次的化验室荧光-控制部门-的所有表格，并能查询所有化验室表格。
    public static final long DUTY_EMPLOYEE_YG_OPERATOR = 62L;//化验室荧光控制员

    public static final long YG_START = 61L;

    public static final long YG_END = 70L;

    //各操作员可以在自己值班期间填写和修改对应班次的化验室分析部门的所有表格，并能查询所有化验室表格。
    public static final long DUTY_EMPLOYEE_HYS_ANALYST = 71L;//化验室分析员
    //各操作员可以在自己值班期间填写和修改对应班次的化验室物检部门的所有表格，并能查询所有化验室表格。
    public static final long DUTY_EMPLOYEE_HYS_OPERATOR = 72L;//化验室物检员

    public static final long HYS_START = 71L;

    public static final long HYS_END = 80L;


    /**********************************************************************/
    //部门
    public static final long DEPARTMENT_NONE = 0L;//无
    public static final long DEPARTMENT_HUAYS = 1L;//化验室
    public static final long DEPARTMENT_ZHKONGKS = 2L;//中控室
    public static final long DEPARTMENT_XINGZS = 3L;//行政部门
    public static final long DEPARTMENT_ZHONGBU = 4L;//总部

//    //科室
//    public static final int SECTION_NONE = 0;//无
//    public static final int SECTION_HEAD = 1;//总部科室
//    public static final int SECTION_FLOURSECENCE = 2;//荧光科室
//    public static final int SECTION_ANALYSIS = 3;//分析科室
//    public static final int PHCHECK = 4;//物检科室

//    //type 权限
//    public static final int AUTHORITY_NONE = 0;//无
//    //public static final int AUTHORITY_OPERATOR = 1;//操作员
//    public static final int AUTHORTIY_ENGINEER = 2;//工程师
//    public static final int AUTHORITY_DIRECTOR = 3;//主任
//    public static final int AUTHORITY_MANAGER = 4;//经理


}
