package com.whut.ns.caiji.web.service.impl.auth;

import com.whut.my.caiji.ns.commons.constants.Constant;
import com.whut.my.caiji.ns.commons.persistence.BaseModel;
import com.whut.my.caiji.ns.domain.user.SysPermission;
import com.whut.my.caiji.ns.domain.user.SysUser;
import com.whut.ns.caiji.abstracts.AbstractBaseServiceImpl;
import com.whut.ns.caiji.annotation.Read;
import com.whut.ns.caiji.annotation.Write;
import com.whut.ns.caiji.dao.auth.TbSysPermissionDao;
import com.whut.ns.caiji.dao.auth.TbSysUserDao;
import com.whut.ns.caiji.web.service.user.auth.TbSysPermissionService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/7/6 15:56
 * @desription
 */
@Service
public class TbSysPermissionServiceImp extends AbstractBaseServiceImpl<SysPermission, TbSysPermissionDao> implements TbSysPermissionService {


    @Override
    @Read
    public List<SysPermission> getPermissionByUsername(String username) {
        return dao.selectPermissionUrlsByUserName(username);
    }

    @Override
    @Write
    public BaseModel save(SysPermission entity) {

        if(entity != null){
            dao.insert(entity);
        }

        return new BaseModel(
                Constant.SUCESS,
                "保存成功",
                null
        );
    }

    @Override
    @Write
    public BaseModel saveCollection(List<SysPermission> entities) {

        if(entities != null && entities.size() > 0){
            dao.insertAll(entities);
        }

        return null;
    }
}
