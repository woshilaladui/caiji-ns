package com.whut.ns.caiji.aop;

import com.whut.ns.caiji.config.db.DynamicSwitchDBTypeUtil;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/7/28 21:53
 * @desription
 */
@Aspect
@Component
public class DataSourceAOP {


    /**
     * 只要加了@Read注解的方法就是一个切入点
     */
    @Pointcut("@annotation(com.whut.ns.caiji.annotation.Read)")
    public void readPointcut() {
    }



    /**
     * 只要加了@Write注解的方法就是一个切入点
     */
    @Pointcut("@annotation(com.whut.ns.caiji.annotation.Write)")
    public void writePointcut() {}


    /**
     * 配置前置通知,如果是readPoint就切换数据源为从数据库
     */
    @Before("readPointcut()")
    public void readAdvise() {
        //System.out.println("aaa");
        DynamicSwitchDBTypeUtil.slave();
    }

    /**
     * 配置前置通知，如果是writePoint就切换数据源为主数据库
     */
    //execution（<访问修饰符> <返回类型> <方法名>(<参数>)<异常>）
    @Before("writePointcut()")
    public void writeAdvise() {

        DynamicSwitchDBTypeUtil.master();
    }

}
