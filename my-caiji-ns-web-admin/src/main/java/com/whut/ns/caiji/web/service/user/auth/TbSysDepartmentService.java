package com.whut.ns.caiji.web.service.user.auth;

import com.whut.my.caiji.ns.commons.persistence.BaseService;
import com.whut.my.caiji.ns.domain.user.SysDepartment;
import org.springframework.stereotype.Service;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/7/6 15:37
 * @desription
 */
@Service
public interface TbSysDepartmentService extends BaseService<SysDepartment> {
}
