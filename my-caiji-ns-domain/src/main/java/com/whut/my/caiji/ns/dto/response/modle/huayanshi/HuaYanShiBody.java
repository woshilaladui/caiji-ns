package com.whut.my.caiji.ns.dto.response.modle.huayanshi;

import com.whut.my.caiji.ns.domain.huayanshi.HuaYanShi;
import com.whut.my.caiji.ns.domain.standard.Standard;


import java.util.List;

public class HuaYanShiBody {
    List<HuaYanShi> huaYanShis;
    Standard standard;

    public HuaYanShiBody() {
    }

    public HuaYanShiBody(List<HuaYanShi> huaYanShis, Standard standard) {
        this.huaYanShis = huaYanShis;
        this.standard = standard;
    }

    public Standard getStandard() {
        return standard;
    }

    public void setStandard(Standard standard) {
        this.standard = standard;
    }

    public HuaYanShiBody(List<HuaYanShi> huaYanShis) {
        this.huaYanShis = huaYanShis;
    }

    public List<HuaYanShi> getHuaYanShis() {
        return huaYanShis;
    }

    public void setHuaYanShis(List<HuaYanShi> huaYanShis) {
        this.huaYanShis = huaYanShis;
    }
}
