package com.whut.my.caiji.ns.dto.request.model.user;



public class UserUpdateRequestModel {


    public int id;
    public String username;
    public String phone;
    public int state;
    public int department;
    public int duty;
    public int authority;

    public UserUpdateRequestModel() {
    }

    public UserUpdateRequestModel(
            int id,
            String username,
            String phone,
            int state,
            int department,
            int duty,
            int authority
    ) {
        this.id = id;
        this.username = username;
        this.phone = phone;
        this.state = state;
        this.department = department;
        this.duty = duty;
        this.authority = authority;
    }

    public int getAuthority() {
        return authority;
    }

    public void setAuthority(int authority) {
        this.authority = authority;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getDepartment() {
        return department;
    }

    public void setDepartment(int department) {
        this.department = department;
    }

    public int getDuty() {
        return duty;
    }

    public void setDuty(int duty) {
        this.duty = duty;
    }

    @Override
    public String toString() {
        return "UserUpdateRequestModel{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", phone='" + phone + '\'' +
                ", state=" + state +
                ", department=" + department +
                ", duty=" + duty +
                ", permission=" + authority +
                '}';
    }
}
