package com.whut.my.caiji.ns.dto.response.modle.zhongkongshi;



import com.whut.my.caiji.ns.domain.zhongkongshi.ZhongKongShi;

import java.util.List;

public class ZhongKongShiBody {

    List<ZhongKongShi> zhongKongShis;

    public ZhongKongShiBody() {
    }

    public ZhongKongShiBody(List<ZhongKongShi> zhongKongShis) {
        this.zhongKongShis = zhongKongShis;
    }

    public List<ZhongKongShi> getZhongKongShis() {
        return zhongKongShis;
    }

    public void setZhongKongShis(List<ZhongKongShi> zhongKongShis) {
        this.zhongKongShis = zhongKongShis;
    }
}
