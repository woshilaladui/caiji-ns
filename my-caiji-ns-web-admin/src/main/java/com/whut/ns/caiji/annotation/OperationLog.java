package com.whut.ns.caiji.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/7/30 9:42
 * @desription
 */
@Target(value={ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface OperationLog {

    //执行的操作
    String operation() default "";

    //执行操作的简称标记
    String tag() default "";


}
