package com.whut.my.caiji.ns.commons.constants;

public class ServiceConstant {

    //服务器文件存放的根地址

    private static final String UPLOAD_ROOT_URL = "/root/home/admin/caiji/files";

    public static final String UPLOAD_BACKGROUND_PHOTO_URL = UPLOAD_ROOT_URL + "/background_pic/";

    public static final String UPLOAD_FILE_URL = UPLOAD_ROOT_URL + "/file/";

    public static final String UPLOAD_PHOTO_URL = UPLOAD_ROOT_URL + "/photo/";

    //public static final String UPLOAD_URL = ROOT_URL+"/tempFile";

    public static final String UPLOAD_URL = "E:\\fileupload";

}
