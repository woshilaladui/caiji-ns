package com.whut.ns.caiji.aop;

import com.whut.my.caiji.ns.commons.constants.Constant;
import com.whut.my.caiji.ns.commons.persistence.BaseModel;
import com.whut.my.caiji.ns.commons.utils.DateUtils;
import com.whut.my.caiji.ns.commons.utils.IpUtils;
import com.whut.my.caiji.ns.domain.huayanshi.HuaYanShi;
import com.whut.my.caiji.ns.domain.log.TbLogRecord;
import com.whut.my.caiji.ns.domain.user.SysUser;
import com.whut.my.caiji.ns.dto.request.model.huayanshi.HuaYanShiRequestModel;
import com.whut.my.caiji.ns.dto.response.modle.feedback.FeedBackModel;
import com.whut.my.caiji.ns.dto.response.modle.huayanshi.HuaYanShiBody;
import com.whut.my.caiji.ns.dto.response.modle.huayanshi.HuaYanShiModel;
import com.whut.my.caiji.ns.dto.response.modle.standard.StandardModel;
import com.whut.my.caiji.ns.dto.response.modle.user.UserBody;
import com.whut.my.caiji.ns.dto.response.modle.user.UserModel;
import com.whut.my.caiji.ns.dto.response.modle.zhongkongshi.ZhongKongShiModel;
import com.whut.ns.caiji.annotation.OperationLog;
import com.whut.ns.caiji.config.db.DynamicSwitchDBTypeUtil;
import com.whut.ns.caiji.security.auth.jwt.JwtTokenUtil;
import com.whut.ns.caiji.web.service.log.OperationLogService;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.support.BeanDefinitionReader;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/7/30 15:22
 * @desription
 */
@Aspect
@Component
public class OperationLogAOP implements Ordered {

    @Autowired
    private HttpServletRequest httpServletRequest;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private OperationLogService operationLogService;

    /**
     * 只要加了@OperationLog注解的方法就是一个切入点
     */
    @Pointcut("@annotation(com.whut.ns.caiji.annotation.OperationLog)")
    public void operationLogPointcut() {}

    /**
     * 切面的思想在执行完方法的时候来记录操作日志
     * @param operationLog 注解上的值
     * @param object 方法的返回值
     */
    @AfterReturning(
            value = "operationLogPointcut() && @annotation(operationLog) ",
            returning = "object"
    )
    public void operationLogAfter(OperationLog operationLog,Object object){
        Date now = new Date();

        String strNow  = DateUtils.getFormatTime("yyyy-MM-dd HH:mm:ss");

        String username = jwtTokenUtil.getUsername();


        String ip = "127.0.0.1";

        String test_ip = IpUtils.getIPAddress(httpServletRequest);



        String specificOperation = specificOperation(object);

        if(specificOperation.equals("-1")){


            //登陆操作特殊处理
            UserModel userModel = (UserModel) object;


            if(userModel.code == Constant.ERROR)
                return;

            SysUser sysUser = (SysUser) ((UserBody) userModel.getData()).user;



            username = sysUser.getUsername();


        }
            StringBuilder operation  = new StringBuilder();

//            System.out.println("now = "+now);
//            System.out.println("strNow = "+strNow);

            operation.append(username);
            operation.append(" 在 ");
            operation.append(strNow);
            operation.append(" ");
            operation.append("ip = ");
            operation.append(ip);
            operation.append(" ");
            //!(specificOperation.equals("")||specificOperation.equals("-1"))
            if(specificOperation.equals("")||specificOperation.equals("-1")){
                //特殊处理
                operation.append(operationLog.operation());
                //operation.append(specificOperation);
            }else {
                operation.append(specificOperation);
            }
            String strOperation = operation.toString();

            TbLogRecord tbLogRecord = new TbLogRecord(
                    strOperation,
                    ip,
                    now,
                    username,
                    operationLog.operation()
            );

        //StringBuilder 来拼接string提高访问效率


//        System.out.println("operation = " + strOperation);

        operationLogService.save(tbLogRecord);
    }

    /**
     * 具体的操作日志  比如某人查询了化验室表格这是模糊一点的信息
     *                 准确为某人查询了中控日报表格这是具体信息
     * @param obj
     * @return
     */
    private String specificOperation(Object obj){
        if(obj instanceof HuaYanShiModel){

            return ((HuaYanShiModel) obj).getMsg();

        }else if(obj instanceof ZhongKongShiModel){

            return ((ZhongKongShiModel) obj).getMsg();

        }else if(obj instanceof StandardModel){

            return ((StandardModel) obj).getMsg();

            //UserModel
        }else if(obj instanceof BaseModel){

            return ((BaseModel) obj).getMsg();

        }else if(obj instanceof FeedBackModel){
            return ((FeedBackModel) obj).getMsg();
        }
        else if(obj instanceof UserModel){



            return "-1";
        }
        else {
            return "";
        }
    }

    /**
     * 用切面aop思想来记录日志
     * @param operationLog
     */
    @Before("operationLogPointcut() && @annotation(operationLog)")
    public void operationLogAdvise(OperationLog operationLog) {

//        Date now = DateUtils.getCurrentDate();
//
//        String strNow  = DateUtils.getFormatTime("yyyy-MM-dd HH:mm:ss");
//
//        String username = jwtTokenUtil.getUsername();
//        String ip = IpUtils.getIPAddress(httpServletRequest);
//
//        //StringBuilder 来拼接string提高访问效率
//        StringBuilder operation  = new StringBuilder();
//
//        operation.append(username);
//        operation.append(" 在 ");
//        operation.append(strNow);
//        operation.append(" ");
//        operation.append("ip = ");
//        operation.append(ip);
//        operation.append(" ");
//        operation.append(operationLog.operation());
//        String strOperation = operation.toString();
//
//
//        TbLogRecord tbLogRecord = new TbLogRecord(
//                strOperation,
//                ip,
//                now,
//                username
//        );

        //operationLogService.save(tbLogRecord);

    }


    /**
     * 优先级  反比
     * @return
     */
    @Override
    public int getOrder() {
        return 100;
    }
}
