package com.whut.ns.caiji.security.auth.jwt;

import com.whut.my.caiji.ns.commons.exception.CustomException;
import com.whut.my.caiji.ns.commons.exception.CustomExceptionType;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Data
@Component
public class JwtTokenUtil {

    @Value("${jwt.secret}")
    private String secret;

    @Value("${jwt.expiration}")
    private Long expiration;

    @Value("${jwt.kaptchaExpiration}")
    private Long kaptchaExpiration;

    @Value("${jwt.header}")
    private String header;

    @Autowired
    private HttpServletRequest httpServletRequest;


    /**
     * 生成token令牌
     *
     * @param userDetails 用户
     * @return 令token牌
     */
    public String generateToken(UserDetails userDetails) {
        Map<String, Object> claims = new HashMap<>(2);
        claims.put("sub", userDetails.getUsername());
        claims.put("created", new Date());

        return generateToken(claims);
    }

    /**
     * 生成验证码的token
     * @param key
     * @return
     */
    public String generateKaptchaToken(String key){
        Map<String, Object> claims = new HashMap<>(2);
        claims.put("sub", key);
        claims.put("created", new Date());
        return generateKaptchaToken(claims);
    }

    /**
     * 从claims生成令牌,如果看不懂就看谁调用它
     *
     * @param claims 数据声明
     * @return 令牌
     */
    private String generateToken(Map<String, Object> claims) {
        Date expirationDate = new Date(System.currentTimeMillis() + expiration);
        return Jwts.builder().setClaims(claims)
                .setExpiration(expirationDate)
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }

    private String generateKaptchaToken(Map<String, Object> claims){
        Date expirationDate = new Date(System.currentTimeMillis() + kaptchaExpiration);
        return Jwts.builder().setClaims(claims)
                .setExpiration(expirationDate)
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }

    /**
     * 解密验证码
     * @param token
     * @return
     */
    public String getCaptchaCodeFromToken(String token){
        String key;
        try {
            Claims claims = getClaimsFromToken(token);
            key = claims.getSubject();
        } catch (Exception e) {
            key = null;
        }
        return key;
    }

    /**
     * 从令牌中获取用户名
     *
     * @param token 令牌
     * @return 用户名
     */
    public String getUsernameFromToken(String token) {
        String username;
        try {
            Claims claims = getClaimsFromToken(token);
            username = claims.getSubject();
        } catch (Exception e) {
            username = null;
        }
        return username;
    }

    public String getUsername(){
        return getUsernameFromToken(getToken());
    }

    public String getToken(){
        return httpServletRequest.getHeader(getHeader());
    }

    /**
     * 判断令牌是否过期
     *
     * @param token 令牌
     * @return 是否过期
     */
    public Boolean isTokenExpired(String token) {
        try {
            Claims claims = getClaimsFromToken(token);
            Date expiration = claims.getExpiration();
            return expiration.before(new Date());
        } catch (Exception e) {
            return false;
        }
    }



    /**
     * 刷新令牌
     *
     * @param token 原令牌
     * @return 新令牌
     */
    public String refreshToken(String token) throws CustomException {
        String refreshedToken;

            Claims claims = getClaimsFromToken(token);

            if (claims != null) {
                claims.put("created", new Date());

                refreshedToken = generateToken(claims);
            }else {
                refreshedToken = null;
            }


        return refreshedToken;
    }

    public String invalidateToken(String token){

            Claims claims = getClaimsFromToken(token);

            if(claims != null){
                return  Jwts.builder().setClaims(claims)
                        .setExpiration(new Date())
                        .signWith(SignatureAlgorithm.HS512, secret)
                        .compact();
            }else {
                return null;
            }

    }

    /**
     * 验证令牌
     *
     * @param token       令牌
     * @param userDetails 用户
     * @return 是否有效
     */
    public Boolean validateToken(String token, UserDetails userDetails) {

        String username = getUsernameFromToken(token);
        return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
    }


    /**
     * 从令牌中获取数据声明
     *
     * @param token 令牌
     * @return 数据声明
     */
    private Claims getClaimsFromToken(String token) throws CustomException{
        Claims claims;
        try {
            claims = Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
        } catch (Exception e) {
            claims = null;

            throw new CustomException(
                    CustomExceptionType.TOKEN_INVALID_ERROR,
                    "token无效"
            );

        }
        return claims;
    }

}

