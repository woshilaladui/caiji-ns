package com.whut.ns.caiji.web.service.impl.auth;

import com.whut.my.caiji.ns.commons.persistence.BaseModel;
import com.whut.my.caiji.ns.domain.user.SysDepartment;
import com.whut.ns.caiji.abstracts.AbstractBaseServiceImpl;
import com.whut.ns.caiji.annotation.Write;
import com.whut.ns.caiji.dao.auth.TbSysDepartmentDao;
import com.whut.ns.caiji.web.service.user.auth.TbSysDepartmentService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/7/10 10:03
 * @desription
 */
@Service
public class TbSysDepartmentServiceImp extends AbstractBaseServiceImpl<SysDepartment, TbSysDepartmentDao> implements TbSysDepartmentService {

    @Override
    @Write
    public BaseModel save(SysDepartment entity) {
        return null;
    }

    @Override
    @Write
    public BaseModel saveCollection(List<SysDepartment> entities) {
        return null;
    }
}
