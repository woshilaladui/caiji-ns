package com.whut.my.caiji.ns.domain.netty.im;

import com.whut.my.caiji.ns.commons.persistence.BaseEntity;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2021/2/15 11:25
 * @desription 好友列表
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Builder
public class MyFriends extends BaseEntity {

    private Long myUserId;

    private Long myFriendUserId;

    public MyFriends() {
    }

    public MyFriends(Long myUserId, Long myFriendUserId) {
        this.myUserId = myUserId;
        this.myFriendUserId = myFriendUserId;
    }

    public MyFriends(Long id, Date createdAt, Date updatedAt, Long myUserId, Long myFriendUserId) {
        super(id, createdAt, updatedAt);
        this.myUserId = myUserId;
        this.myFriendUserId = myFriendUserId;
    }

    public MyFriends(Date createdAt, Date updatedAt, Long myUserId, Long myFriendUserId) {
        super(createdAt, updatedAt);
        this.myUserId = myUserId;
        this.myFriendUserId = myFriendUserId;
    }
}
