package com.whut.my.caiji.ns.domain.standard;

import com.whut.my.caiji.ns.commons.persistence.BaseEntity;
import com.whut.my.caiji.ns.domain.user.TbUser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * 水泥采集系统某些表需要标准值来校验
 *
 *
 * @author nianshao zm
 * @date 2020/5/27
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class Standard extends BaseEntity {


    private static final long serialVersionUID = -2834870361641636747L;
    private String tableName;

    private String startValue;

    private String endValue;

    //填写标准的人
    private String username;

    private String reason;//修改原因

    @Override
    public String toString() {
        return "Standard{" +
                "id=" + getId() +
                "tableName='" + tableName + '\'' +
                ", startValue='" + startValue + '\'' +
                ", endValue='" + endValue + '\'' +
                ", username='" + username + '\'' +
                ", reason='" + reason + '\'' +
                ", createdAt=" + getCreatedAt() +
                ", updatedAt=" + getUpdatedAt() +
                '}';
    }
}
