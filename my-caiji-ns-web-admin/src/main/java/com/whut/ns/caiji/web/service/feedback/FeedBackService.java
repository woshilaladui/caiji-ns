package com.whut.ns.caiji.web.service.feedback;

import com.whut.my.caiji.ns.commons.persistence.BaseService;
import com.whut.my.caiji.ns.domain.feedback.FeedBack;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;


/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/11/10 9:37
 * @desription
 */
@Component
public interface FeedBackService extends BaseService<FeedBack> {

    List<FeedBack> getAllFeedBackByUsernameOrTimeBetween(String username, Date startDate, Date endDate);



}
