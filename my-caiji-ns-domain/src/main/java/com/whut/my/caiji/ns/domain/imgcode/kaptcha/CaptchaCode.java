package com.whut.my.caiji.ns.domain.imgcode.kaptcha;

import java.io.Serializable;
import java.time.LocalDateTime;

public class CaptchaCode implements Serializable {


    private static final long serialVersionUID = 433048112560925844L;
    private String code;

    //过期时间
    private LocalDateTime expireTime;


    public CaptchaCode(String code, int expireAfterSeconds){
        this.code = code;
        this.expireTime = LocalDateTime.now().plusSeconds(expireAfterSeconds);
    }

    public boolean isExpired(){
        return  LocalDateTime.now().isAfter(expireTime);
    }

    public String getCode() {
        return code;
    }

    @Override
    public String toString() {
        return "CaptchaCode{" +
                "code='" + code + '\'' +
                ", expireTime=" + expireTime +
                '}';
    }
}
