package com.whut.ns.caiji.scheduler;

import com.whut.ns.caiji.web.service.log.OperationLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author zm
 * @version 1.0.0
 * @date 2020/12/6 10:35
 * @desription
 */
@Component
public class DeleteOperationLog {

    @Autowired
    private OperationLogService operationLogService;

    private static final int N = 2;

    //每天上午10点开始删除
    @Scheduled(cron = "0 0 10 * * ?")
    private void deleteOperationLogBeforeNMonth() {
        int flag = operationLogService.deleteLogNMonthsAgo(N);

        if (flag > 0)
            System.out.println("删除N = " + N + "个月前日志成功 总共删除条数为 = "+flag);
        else
            System.out.println("删除N = " + N + "个月前日志失败");

    }

}
