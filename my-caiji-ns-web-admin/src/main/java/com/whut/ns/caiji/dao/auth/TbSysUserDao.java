package com.whut.ns.caiji.dao.auth;

import com.whut.my.caiji.ns.commons.persistence.BaseDao;
import com.whut.my.caiji.ns.commons.persistence.BaseModel;
import com.whut.my.caiji.ns.domain.user.SysPermission;
import com.whut.my.caiji.ns.domain.user.SysUser;
import com.whut.ns.caiji.security.auth.MyUserDetails;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TbSysUserDao extends BaseDao<SysUser> {


    //查询用户信息
    MyUserDetails selectByUserName(String username);

    /**
     * 更改用户状态
     *
     * @param username 用户名
     * @param enabled  原本状态
     * @return
     */
    Integer lockUser(String username, Integer enabled);

    /**
     * 通过手机号来查询用户
     *
     * @param phone 手机号
     * @return
     */
    SysUser selectUserByPhone(String phone);


    /**
     * 通过用户名查询用户
     *
     * @param username
     * @return
     */
    SysUser selectUserByUsername(String username);

    SysUser selectUserByUserId(Long id);

    /**
     * 通过用户名查询部门Id
     *
     * @param username
     * @return
     */
    Long selectDepartmentIdByUsername(String username);


    /**
     * 通过用户Id来查询部门id
     *
     * @param id
     * @return
     */
    Long selectDepartmentIdByUserId(Long id);


}
