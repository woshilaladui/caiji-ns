package com.whut.ns.caiji.dao.log;

import com.whut.my.caiji.ns.commons.persistence.BaseDao;
import com.whut.my.caiji.ns.domain.log.TbLogRecord;

import java.util.List;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/7/30 16:53
 * @desription
 */
public interface OperationLogDao extends BaseDao<TbLogRecord> {


    List<TbLogRecord> getAllByUsernameOrTimeBetween(
            String username,
            String startDate,
            String endDate
    );

    Integer deleteOperationLogs(List<Long> operationId);

    /**
     *
     * @param username 用户名
     * @param tag 执行日志的简化名称如：注销登陆 登陆系统等
     * @return TbLogRecord
     */
    TbLogRecord getLastLoginLog(String username,String tag);

    /**
     * 删除N个月之前的日志
     * @param N N个月之前
     * @return
     */
    Integer deleteLogNMonthsAgo(int N);

}
