package com.whut.my.caiji.ns.dto.response.modle.standard;


import com.whut.my.caiji.ns.dto.response.modle.BaseModel;

public class StandardModel extends BaseModel {

    public StandardBody data;

    public StandardModel() {
    }

    public StandardModel(StandardBody data) {
        this.data = data;
    }

    public StandardModel(int code, String msg, StandardBody data) {
        super(code, msg);
        this.data = data;
    }

    public StandardModel(int code, String msg, String controllerName, StandardBody data) {
        super(code, msg, controllerName);
        this.data = data;
    }

    public StandardBody getData() {
        return data;
    }

    public void setData(StandardBody data) {
        this.data = data;
    }
}
