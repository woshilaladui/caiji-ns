package com.whut.my.caiji.ns.commons.constants;

public  class TableConstant {

    /***********************************化验室*****************************************/

    /********************荧光分析表格*****************************/
    //进厂原材料分析化学报告单（石灰石）
    public static final String Limestone_SHS = "RMA_SHS";

    //进厂砂岩原材料分析化学报告单
    public static final String Limestone_SY = "RMA_SY";

    //进厂铁粉原材料分析化学报告单
    public static final String Limestone_TF = "RMA_TF";

    //进厂粉煤灰（干）原材料分析化学报告单
    public static final String Limestone_FMHg = "RMA_FMHg";

    //进厂粉煤灰（湿）原材料分析化学报告单
    public static final String Limestone_FMHs = "RMA_FMHs";

    //仓下石灰石原材料化学分析报告单
    public static final String Limestone_CaCO3 = "CX_CaCO3";

    //出磨生料化学分析报告单
    public static final String Limestone_CRM = "CRM";

    //入窑生料化学分析报告单
    public static final String Limestone_RMC = "RMC";

    //出窑熟料全分析汇总表
    public static final String Limestone_KAS = "KAS";

    //出厂熟料全分析汇总表
    public static final String Limestone_FAS = "FAS";

    //临城中联福石控制室原始记录
    public static final String Limestone_CRO = "CRO";

    /********************分析表格*****************************/
    //原材料分析原始记录 石灰石
    public static final String Analysis_SHS = "RAO_SHS";

    //原材料分析原始记录 砂岩
    public static final String Analysis_SY = "RAO_SY";

    //原材料分析原始记录 铁粉
    public static final String Analysis_TF = "RAO_TF";

    //原材料分析原始记录 粉煤灰(干)
    public static final String Analysis_FMHg = "RAO_FMHg";

    //原材料分析原始记录 粉煤灰(湿)
    public static final String Analysis_FMHs = "RAO_FMHs";

    //出磨生料分析原始记录
    public static final String Analysis_BS = "Raw_BS";

    //入窑生料分析原始记录
    public static final String Analysis_RY = "Raw_RY";

    //出窑熟料化学分析单
    public static final String Analysis_CYA = "NS_CYA";

    //出厂熟料化学分析单
    public static final String Analysis_CCA = "NS_CCA";

    //出窑熟料物理性能检测
    public static final String Analysis_CYT = "NS_CYT";

    //出厂熟料物理性能检测
    public static final String Analysis_CCT = "NS_CCT";

    //进厂原燃材料水分
    public static final String Analysis_JCM = "NS_JCM";

    //神木工业分析原始记录
    public static final String Analysis_SMA = "NS_SMA";

    //煤粉工业分析原始记录
    public static final String Analysis_MFA = "NS_MFA";


    /********************电量表格*****************************/

    //每月电量表
    public static final String Eletri_ME = "ET_ME";

    //35KW表
    public static final String Eletri_TFKT = "ET_TFKT";


}
