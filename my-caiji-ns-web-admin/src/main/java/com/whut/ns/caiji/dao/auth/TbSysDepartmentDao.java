package com.whut.ns.caiji.dao.auth;

import com.whut.my.caiji.ns.commons.persistence.BaseDao;
import com.whut.my.caiji.ns.domain.user.SysDepartment;
import org.springframework.stereotype.Repository;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/7/6 11:45
 * @desription
 */
@Repository
public interface TbSysDepartmentDao extends BaseDao<SysDepartment> {
}
