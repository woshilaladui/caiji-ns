package com.whut.my.caiji.ns.domain.netty.im;

import com.whut.my.caiji.ns.commons.persistence.BaseEntity;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2021/2/15 11:25
 * @desription 好友申请列表
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Builder
public class FriendsRequest extends BaseEntity {

    private Long sendUserId;

    private Long acceptUserId;

    public FriendsRequest() {
    }

    public FriendsRequest(Long sendUserId, Long acceptUserId) {
        this.sendUserId = sendUserId;
        this.acceptUserId = acceptUserId;
    }

    public FriendsRequest(Long id, Date createdAt, Date updatedAt, Long sendUserId, Long acceptUserId) {
        super(id, createdAt, updatedAt);
        this.sendUserId = sendUserId;
        this.acceptUserId = acceptUserId;
    }

    public FriendsRequest(Date createdAt, Date updatedAt, Long sendUserId, Long acceptUserId) {
        super(createdAt, updatedAt);
        this.sendUserId = sendUserId;
        this.acceptUserId = acceptUserId;
    }
}