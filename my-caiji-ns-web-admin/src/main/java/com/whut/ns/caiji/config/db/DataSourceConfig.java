package com.whut.ns.caiji.config.db;


import com.whut.my.caiji.ns.domain.db.DBTypeEnum;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/7/28 17:19
 * @desription
 */
@Configuration
public class DataSourceConfig {

    @Value("${spring.datasource.type}")
    private Class<? extends DataSource> dataSourceType;

    /**
     * 将创建的master数据源存入Spring容器中，并且注入内容
     * key值为方法名
     * @return master数据源
     */
    @Bean(name = "masterDataSource")
    @Primary
    @ConfigurationProperties(prefix = "spring.datasource.druid.master")
    public DataSource masterDataSource() {

        return DataSourceBuilder
                .create()
                .type(dataSourceType)
                .build();
    }

    /**
     * 将创建的slave数据源存入Spring容器中，并且注入内容
     * key值为方法名
     * @return slave数据源
     */
    @Bean(name = "slaveDataSource")
    @ConfigurationProperties(prefix = "spring.datasource.druid.slave")
    public DataSource slaveDataSource() {
        return DataSourceBuilder
                .create()
                .type(dataSourceType)
                .build();
    }

    /**
     * 决定最终要使用的数据源
     * @return
     */
    @Bean
    public DataSource targetDataSource(
            @Qualifier("masterDataSource") DataSource masterDataSource,
            @Qualifier("slaveDataSource") DataSource slaveDataSource
    ) {

        // 用来存放主数据源和从数据源
        Map<Object, Object> targetDataSource = new HashMap<>();

        // 往map中添加主数据源
        targetDataSource.put(DBTypeEnum.MASTER,masterDataSource);

        // 往map中添加从数据源
        targetDataSource.put(DBTypeEnum.SLAVE,slaveDataSource);

        // 创建 routtingDataSource 用来实现动态切换
        RouttingDataSource routtingDataSource = new RouttingDataSource();


        // 设置默认的数据源
        routtingDataSource.setDefaultTargetDataSource(masterDataSource);

        // 绑定所有的数据源
        routtingDataSource.setTargetDataSources(targetDataSource);


        return routtingDataSource;
    }

}
