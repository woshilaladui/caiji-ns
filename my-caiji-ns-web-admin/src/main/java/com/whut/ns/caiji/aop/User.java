package com.whut.ns.caiji.aop;

import org.springframework.stereotype.Component;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2021/5/17 15:28
 * @desription
 */

public class User {
    private Integer id;
    private String name;

    public User() {
    }

    public User(Integer id) {
        this.id = id;
    }

    public User(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
