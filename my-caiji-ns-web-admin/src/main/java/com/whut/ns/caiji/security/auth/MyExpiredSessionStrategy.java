package com.whut.ns.caiji.security.auth;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.session.SessionInformationExpiredEvent;
import org.springframework.security.web.session.SessionInformationExpiredStrategy;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Deprecated
public class MyExpiredSessionStrategy implements SessionInformationExpiredStrategy {

    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public void onExpiredSessionDetected(
            SessionInformationExpiredEvent sessionInformationExpiredEvent
    ) throws IOException, ServletException {

        Map<String,Object> map = new HashMap<>();

        map.put("code",0);
        map.put("msg","您已经在另外一台电脑或机器登陆，被迫下线");

        sessionInformationExpiredEvent
                .getResponse()
                .getWriter()
                .write(objectMapper.writeValueAsString(map));

    }
}
