package com.whut.ns.caiji.netty.im;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import org.springframework.stereotype.Component;

import java.io.BufferedInputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * @author zm
 * @version 1.0.0
 * @date 2021/2/15 11:23
 * @desription WebSocketServer 聊天系统服务端
 */
@Component
public class WebSocketServer {



    private static class SingletionWSServer {
        static final WebSocketServer instance = new WebSocketServer();
    }

    public static WebSocketServer getInstance() {
        return SingletionWSServer.instance;
    }

    private EventLoopGroup bossGroup;
    private EventLoopGroup workerGroup;
    private ServerBootstrap server;
    private ChannelFuture future;

    public WebSocketServer() {
        bossGroup = new NioEventLoopGroup(1);
        workerGroup = new NioEventLoopGroup(8);
        server = new ServerBootstrap();
        server.group(bossGroup, workerGroup)
                .channel(NioServerSocketChannel.class)
                .childHandler(new WSServerInitialzer());
    }

    public void start() {

        String ip = "127.0.0.1";
        try {
            InetAddress address = InetAddress.getLocalHost();
            ip = address.getHostAddress();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        //System.out.println("ip = " + ip);

        try {
            this.future = server.bind(ip,8888).sync();
        } catch (Exception e) {

            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();

        }
        if (future.isSuccess()) {
            System.out.println("启动 Netty 成功");
        }else {
            System.out.println("启动失败 ");
        }
    }
}
