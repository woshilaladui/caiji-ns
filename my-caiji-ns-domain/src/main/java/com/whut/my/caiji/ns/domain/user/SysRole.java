package com.whut.my.caiji.ns.domain.user;

import com.whut.my.caiji.ns.commons.persistence.BaseEntity;
import com.whut.my.caiji.ns.commons.utils.DateUtils;
import lombok.*;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysRole extends BaseEntity {


    private static final long serialVersionUID = -3356146752110240113L;
    private String roleName;
    private String roleCode;
    private String roleDescription;
    private Integer roleStatus;


    @Override
    public String toString() {
        return "SysRole{" +
                "id=" + getId() +
                "roleName='" + roleName + '\'' +
                ", roleCode='" + roleCode + '\'' +
                ", roleDescription='" + roleDescription + '\'' +
                ", roleStatus=" + roleStatus +
                ", createdAt=" + getCreatedAt() +
                ", updatedAt=" + getUpdatedAt() +
                '}';
    }
}
