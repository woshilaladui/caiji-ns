package com.whut.my.caiji.ns.dto.response.modle;

import lombok.*;

/**
 * @author zm
 * @className BaseModel
 * @Date 2019/8/22 9:18
 * @Version 1.0
 * @Description
 **/
@Data
@Builder
@ToString(callSuper = true)

public class BaseModel {

    //状态代码 方便前端匹配错误码表
    public int code;

    //可读性错误信息
    public String msg;

    public Object data;


    public BaseModel() {
    }

    public BaseModel(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public BaseModel(int code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }
}
