package com.whut.my.caiji.ns.dto.response.modle.user;



import com.whut.my.caiji.ns.domain.user.SysRole;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class UserBody  implements Serializable {


    public Object user;

    public String token;

    //token过期时间

    public Date expiration;

    public UserLoginLogBody userLoginLog;

    public Long[] roles;


    public UserBody(Object user, String token, Date expiration, UserLoginLogBody userLoginLog) {
        this.user = user;
        this.token = token;
        this.expiration = expiration;
        this.userLoginLog = userLoginLog;
    }


    public UserBody(Object user, String token, Date expiration, UserLoginLogBody userLoginLog, Long[] roles) {
        this.user = user;
        this.token = token;
        this.expiration = expiration;
        this.userLoginLog = userLoginLog;
        this.roles = roles;
    }

    public Long[] getRoles() {
        return roles;
    }

    public void setRoles(Long[] roles) {
        this.roles = roles;
    }

    public UserLoginLogBody getUserLoginLog() {
        return userLoginLog;
    }

    public void setUserLoginLog(UserLoginLogBody userLoginLog) {
        this.userLoginLog = userLoginLog;
    }

    public UserBody() {
    }

    public Object getUser() {
        return user;
    }

    public UserBody(Object user) {
        this.user = user;
    }



    public void setUser(Object user) {
        this.user = user;
    }

    public UserBody(Object user, String token) {
        this.user = user;
        this.token = token;
    }

    public UserBody(Object user, String token,Date expiration) {
        this.user = user;
        this.token = token;
        this.expiration = expiration;
    }



    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Date getExpiration() {
        return expiration;
    }

    public void setExpiration(Date expiration) {
        this.expiration = expiration;
    }



}
