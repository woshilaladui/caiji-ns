package com.whut.my.caiji.ns.domain.user;

import com.whut.my.caiji.ns.commons.persistence.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;


@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class SysUserRole extends BaseEntity {


    private static final long serialVersionUID = -8776947616325529053L;
    private Long sysUserId;

    private Long sysRoleId;

    public SysUserRole( Long sysUserId, Long sysRoleId,Date createdAt, Date updatedAt) {
        super(createdAt, updatedAt);
        this.sysUserId = sysUserId;
        this.sysRoleId = sysRoleId;
    }

    @Override
    public String toString() {
        return "SysUserRole{" +
                "id=" + getId() +
                "sysUserId=" + sysUserId +
                ", sysRoleId=" + sysRoleId +
                ", createdAt=" + getCreatedAt() +
                ", updatedAt=" + getUpdatedAt() +
                '}';
    }
}
