package com.whut.ns.caiji.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author zm
 * @version 1.0.0
 * @date 2020/7/28 21:52
 * @desription 操作数据库只写注解
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Write {
}
