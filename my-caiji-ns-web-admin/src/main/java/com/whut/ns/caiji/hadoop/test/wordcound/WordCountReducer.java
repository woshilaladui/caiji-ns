package com.whut.ns.caiji.hadoop.test.wordcound;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/9/29 15:06
 * @desription
 */
public class WordCountReducer extends Reducer<Text, IntWritable, Text, IntWritable> {


    int sum;

    IntWritable v = new IntWritable();

    @Override
    protected void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {


        // 1 累加求和
        System.out.println("key  = " + key);
        sum = 0;
        for (IntWritable count : values) {

            System.out.println("count  = " + count);
            sum += count.get();
        }

        // 2 输出
        v.set(sum);
        context.write(key, v);


    }
}
