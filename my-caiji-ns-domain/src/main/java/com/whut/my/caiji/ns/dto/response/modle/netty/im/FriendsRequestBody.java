package com.whut.my.caiji.ns.dto.response.modle.netty.im;

import java.io.Serializable;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2021/2/15 13:35
 * @desription
 */
public class FriendsRequestBody implements Serializable {
    private Long sendUserId;
    private String sendUsername;
    //private String sendFaceImage;
    //private String sendNickname;

    public Long getSendUserId() {
        return sendUserId;
    }

    public void setSendUserId(Long sendUserId) {
        this.sendUserId = sendUserId;
    }

    public String getSendUsername() {
        return sendUsername;
    }

    public void setSendUsername(String sendUsername) {
        this.sendUsername = sendUsername;
    }
}
