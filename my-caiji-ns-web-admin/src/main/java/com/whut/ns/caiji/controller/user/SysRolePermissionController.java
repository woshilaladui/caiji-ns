package com.whut.ns.caiji.controller.user;

import com.whut.my.caiji.ns.commons.constants.Constant;
import com.whut.my.caiji.ns.commons.persistence.BaseModel;
import com.whut.my.caiji.ns.commons.utils.ListUtils;
import com.whut.my.caiji.ns.domain.user.SysPermission;
import com.whut.my.caiji.ns.dto.response.modle.permission.PermissionModel;
import com.whut.ns.caiji.annotation.OperationLog;
import com.whut.ns.caiji.web.service.user.auth.TbSysPermissionService;
import com.whut.ns.caiji.web.service.user.auth.TbSysRolePermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/7/5 15:52
 * @desription
 */
@Controller
@RequestMapping("/sys/permission")
@CrossOrigin("*")
public class SysRolePermissionController {


    @Autowired
    private TbSysRolePermissionService tbSysRolePermissionService;

    @Autowired
    private TbSysPermissionService tbSysPermissionService;

    @RequestMapping("/getRolePermissionByRoleId")
    @ResponseBody
    @OperationLog(operation = "通过角色id来查询该角色拥有的权限信息")
    public BaseModel getRolePermissionByRoleId(Long roleId){

        return new BaseModel(
                Constant.SUCESS,
                "获取 角色id = "+ roleId + "信息成功 ",
                tbSysRolePermissionService.selectRolePermissionIdByRoleId(roleId)
        );
    }



    @RequestMapping("/getPermissionByUsername")
    @ResponseBody
    @OperationLog(operation = "通过用户名获取该用户全部权限信息")
    public BaseModel getPermissionByUsername(
            @RequestParam String username
    ){

        List<SysPermission> sourceList = tbSysPermissionService.getPermissionByUsername(username);
        List<SysPermission> targetList = new ArrayList<>();

        //预处理
        preSortList(targetList);

        //将权限列表格式化
        sortList(sourceList,targetList,0L);

        return new BaseModel(
                Constant.SUCESS,
                "获取"+username+"的权限信息成功",
                resolvePermissionList(targetList)
        );
    }


    @RequestMapping(value = "/setPermissionByPermissionIds",method = RequestMethod.POST)
    @ResponseBody
    @OperationLog(operation = "通过该用户id来设置该用户的权限信息")
    public BaseModel setPermissionByPermissionIds(
            @RequestBody Map<String,Object> map
    ){

        Long roleId = Long.valueOf((Integer) map.get("roleId"));
        List<Integer> permissionIds = (ArrayList<Integer>) map.get("permissionIds");

        return tbSysRolePermissionService.preSaveCollection(roleId, ListUtils.changeIntegerListToLongList(permissionIds));
    }

    /**
     * 格式化权限列表以适配前端
     */
    private PermissionModel resolvePermissionList(
            List<SysPermission> targetList
    ){

        //System.out.println("targetList = " + targetList);

        List<PermissionModel> listParent = new ArrayList<>();

        int i=1;

        while (i< targetList.size()){

            if(targetList.get(i).isParent()){

                SysPermission sysPermission = targetList.get(i);

                List<PermissionModel> listChildren = new ArrayList<>();


                i++;

                //为子节点
                while (!targetList.get(i).isParent()){
                    //构造子节点
                    SysPermission sysPermissionChildren = targetList.get(i);
                    //System.out.println("sysPermissionChildren = " + sysPermissionChildren.toString());
                    PermissionModel permissionChildren = new PermissionModel(
                            Math.toIntExact(sysPermissionChildren.getId()),
                            sysPermissionChildren.getPermissionName(),
                            sysPermissionChildren.getPermissionUrl(),
                            "权限",
                            null
                    );
                    listChildren.add(permissionChildren);
                    i++;
                    if(i == targetList.size())
                        break;
                }


                listParent.add(new PermissionModel(
                        Math.toIntExact(sysPermission.getId()),
                        sysPermission.getPermissionName(),
                        sysPermission.getPermissionUrl(),
                        "菜单",
                        listChildren
                ));

                if(i == targetList.size())
                    break;

            }
        }

        return new PermissionModel(
                1,
                "权限",
                "/",
                "菜单",
                listParent
        );
    }

    /**
     * 预处理
     * @param targetList
     */
    private void preSortList(List<SysPermission> targetList){
        targetList.add(new SysPermission(
                0L,
                "权限",
                "/",
                0,
                true
        ));
    }

    /**
     * 排序
     *
     * @param sourceList 数据源集合
     * @param targetList 排序后的集合
     * @param parentId   父节点的 ID
     */
    private void sortList(List<SysPermission> sourceList, List<SysPermission> targetList, Long parentId) {
        for (SysPermission sourceEntity : sourceList) {
            if (sourceEntity.getPermissionParentId().equals(parentId)) {

                if(sourceEntity.getId() != 1L){
                    targetList.add(sourceEntity);
                }

                // 判断有没有子节点，如果有则继续追加
                if (sourceEntity.isParent()) {
                    for (SysPermission currentEntity : sourceList) {
                        if (currentEntity.getPermissionParentId().equals(sourceEntity.getId())) {
                            sortList(sourceList, targetList, sourceEntity.getId());
                            break;
                        }
                    }
                }
            }
        }
    }

}
