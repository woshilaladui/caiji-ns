package com.whut.my.caiji.ns.commons.email;

import com.whut.my.caiji.ns.commons.utils.DateUtils;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/11/23 18:48
 * @desription
 */
@Component
public class SendEmail {


    private Email email = new SimpleEmail();

    @Value("${email.host.name}")
    private String hostName;

    @Value("${email.smtp.port}")
    private Integer port;

    @Value("${email.username}")
    private String from;

    @Value("${email.username}")
    private String username;

    @Value("${email.password}")
    private String password;

    public void send(String subject, String msg, String... to) throws EmailException {
        email.setHostName(hostName);
        email.setSmtpPort(port);
        email.setAuthenticator(new DefaultAuthenticator(username, password));
        email.setSSLOnConnect(true);
        email.setFrom(username);
        email.setSubject(subject);
        email.setMsg(msg);
        email.addTo(to);
        email.send();
    }



}
