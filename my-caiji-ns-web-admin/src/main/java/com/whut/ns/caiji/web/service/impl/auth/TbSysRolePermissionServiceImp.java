package com.whut.ns.caiji.web.service.impl.auth;

import com.whut.my.caiji.ns.commons.constants.Constant;
import com.whut.my.caiji.ns.commons.persistence.BaseModel;
import com.whut.my.caiji.ns.commons.utils.DateUtils;
import com.whut.my.caiji.ns.domain.user.SysPermission;
import com.whut.my.caiji.ns.domain.user.SysRole;
import com.whut.my.caiji.ns.domain.user.SysRolePermission;
import com.whut.ns.caiji.abstracts.AbstractBaseServiceImpl;
import com.whut.ns.caiji.annotation.Read;
import com.whut.ns.caiji.annotation.Write;
import com.whut.ns.caiji.dao.auth.*;
import com.whut.ns.caiji.security.auth.jwt.JwtTokenUtil;
import com.whut.ns.caiji.web.service.user.auth.TbSysPermissionService;
import com.whut.ns.caiji.web.service.user.auth.TbSysRolePermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.Date;
import java.util.List;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/7/6 16:51
 * @desription
 */
@Service
//@Transactional(readOnly = true)
public class TbSysRolePermissionServiceImp extends AbstractBaseServiceImpl<SysRolePermission, TbSysRolePermissionDao> implements TbSysRolePermissionService {


    @Autowired
    private TbSysRoleDao tbSysRoleDao;


    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private TbSysUserDao tbSysUserDao;

    @Write
    @Override
    public BaseModel save(SysRolePermission entity) {
        return null;
    }

    /**
     *
     *
     * 设置角色权限
     *
     * @param roleId
     * @param rolePermissionIds 用户传过来要修改的PermissionIds
     * @return
     */
    //@Transactional(readOnly = false)
    @Write
    @Override
    public BaseModel preSaveCollection(
            Long roleId,
            List<Long> rolePermissionIds
    ){



        /**
         *
         * 获取该用户的角色信息
         *
         * 超级admin可以修改任何东西
         *
         * 中控室之只能修改中控室的
         *
         */






        /**
         * 可能是移除 也可能是新添加
         * 1.查询该roleId 的所有permissionIds
         * 2.对比这两个集合
         */

        //从数据库查询到的
        List<Long> dateBaseRolePermissionIds = dao.selectRolePermissionIdByRoleId(roleId);

        //需要插入的id
        List<Long> addRolePermissions = getAddIds(
                dateBaseRolePermissionIds,
                rolePermissionIds
        );


        //需要删除的id
        List<Long> deleteRolePermissions = getDeleteIds(
                dateBaseRolePermissionIds,
                rolePermissionIds
        );

        //删除
        if(deleteRolePermissions != null && deleteRolePermissions.size()>0){
            dao.deleteRolePermissionByPermissionIds(deleteRolePermissions,roleId);
        }

        //新增
        List<SysRolePermission> sysRolePermissions = new ArrayList<>();
        for(int i=0;i<addRolePermissions.size();i++){

            sysRolePermissions.add(
                    new SysRolePermission(
                            roleId,
                            Long.valueOf(addRolePermissions.get(i)),
                            DateUtils.getCurrentDate(),
                            DateUtils.getCurrentDate()
                    )
            );
        }
        return saveCollection(sysRolePermissions);
    }

    @Override
    @Read
    public List<Long> selectRolePermissionIdByRoleId(Long roleId) {

        return dao.selectRolePermissionIdByRoleId(roleId);
    }

    //@Transactional(readOnly = false)
    @Write
    @Override
    public BaseModel saveCollection(List<SysRolePermission> entities) {

        if(entities != null && entities.size() > 0){
            dao.insertAll(entities);
        }

        return new BaseModel(
                Constant.SUCESS,
                "更新用户id = "+entities.get(0).getSysRoleId()+"的权限成功",
                null
        );
    }
}
