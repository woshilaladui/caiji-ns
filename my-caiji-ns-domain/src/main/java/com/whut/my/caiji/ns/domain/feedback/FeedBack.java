package com.whut.my.caiji.ns.domain.feedback;

import com.whut.my.caiji.ns.commons.persistence.BaseEntity;
import lombok.*;

import java.util.Date;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/11/10 9:30
 * @desription
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FeedBack extends BaseEntity {


    private static final long serialVersionUID = 1281141984262657139L;
    private String username;
    private String classification;
    private String title;
    private String content;

}
