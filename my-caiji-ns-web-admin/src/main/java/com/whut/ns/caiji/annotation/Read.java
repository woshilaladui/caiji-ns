package com.whut.ns.caiji.annotation;

import org.springframework.aop.Pointcut;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author zm
 * @version 1.0.0
 * @date 2020/7/28 21:51
 * @desription 操作数据库只读注解
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Read {
}
