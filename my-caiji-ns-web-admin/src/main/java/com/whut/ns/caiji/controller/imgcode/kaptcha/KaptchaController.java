package com.whut.ns.caiji.controller.imgcode.kaptcha;

import com.google.code.kaptcha.impl.DefaultKaptcha;
import com.whut.my.caiji.ns.commons.constants.Constant;
import com.whut.my.caiji.ns.commons.context.ApplicationContextHolder;
//import com.whut.my.caiji.ns.commons.db.RedisCache;
import com.whut.my.caiji.ns.commons.persistence.BaseModel;
import com.whut.ns.caiji.security.auth.jwt.JwtTokenUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.ServletWebRequest;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * 验证码控制器
 * <p>Title: KaptchaController</p>
 * <p>Description: </p>
 *
 * @author Lusifer
 * @version 1.0.0
 * @date 2018/7/6 14:23
 */
@Controller
@Slf4j
@CrossOrigin("*")
//@EnableRedisHttpSession
public class KaptchaController {

    @Resource
    DefaultKaptcha captchaProducer;

    @Autowired
    JwtTokenUtil jwtTokenUtil;


    @RequestMapping(value = "verification", method = RequestMethod.GET)
    public void verification(HttpSession session, HttpServletResponse response, ServletWebRequest request) throws IOException, ServletRequestBindingException {

        response.setDateHeader("Expires", 0);
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.addHeader("Access-Control-Allow-Headers", "Content-Type,kaptchaToken");
        response.addHeader("Access-Control-Expose-Headers","Content-Type,kaptchaToken");
        response.setHeader("Pragma", "no-cache");

        response.setContentType("image/jpeg");

        String capText = captchaProducer.createText();

        String kaptchaToken = jwtTokenUtil.generateKaptchaToken(capText);
        response.setHeader("kaptchaToken",kaptchaToken);


//        RedisCache redisCache = new RedisCache();
//        redisCache.putObject(session.getId().toString(),kaptchaToken,1);


        try(ServletOutputStream out = response.getOutputStream()){
            BufferedImage bufferedImage = captchaProducer.createImage(capText);
            ImageIO.write(bufferedImage,"jpg",out);
            out.flush();
        } catch (IOException e) {

            e.printStackTrace();
        }

    }
}
