package com.whut.ns.caiji.hadoop.test;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/8/16 15:03
 * @desription
 */
public class HdfsClient1 {

    //copyFromLocalFile
    public static void main(String[] args) throws IOException, URISyntaxException, InterruptedException {



        /**
         *
         * Configuration参数对象加载机制
         * 1.首先构造通过默认配置文件 xx-default.xml来配置
         * 2.再加载用户配置文件，覆盖默认的配置文件
         * 3.手动加载set设置的配置，覆盖上面配置
         *
         */
        Configuration conf = new Configuration();

        //第一种方式:修改hadoop_username 为root用户
        System.setProperty("HADOOP_USER_NAME","root");

        //若没有指定上传路径则默认上传到本地项目所属盘符的根目录
        conf.set("fs.defaultFS", "hdfs://101.200.149.190:8020");

        //FileSystem fs = FileSystem.get(conf);

       // 第二种方式:通过获取FileSystem直接设置root用户
        FileSystem fs = FileSystem.get(new URI("hdfs://101.200.149.190:8020"),conf,"root");

        //第三种方式:修改hdfs的配置文件
        /**
         *
         * <property>
         *     <name>dfs.permission</name>
         *     <value>false</value>
         * </property>
         *
         *
         *
         */

        //将本地的一个文件D:/file1,上传到HDFS上 /file1
        // 1. 使用Path描述两个文件
        Path localPath = new Path("D:\\data/input/inputword.txt");
        Path hdfsPath = new Path("/test.txt");
        //2.调用上传方法
        fs.copyFromLocalFile(localPath, hdfsPath);
        //3.关闭
        fs.close();
        System.out.println("上传成功");

    }
}
