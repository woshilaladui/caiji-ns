package com.whut.ns.caiji.controller.zhongkongshi;

import com.whut.my.caiji.ns.commons.constants.Constant;
import com.whut.my.caiji.ns.commons.persistence.BaseModel;
import com.whut.my.caiji.ns.domain.zhongkongshi.ZhongKongShi;
import com.whut.my.caiji.ns.dto.response.modle.zhongkongshi.ZhongKongShiBody;
import com.whut.my.caiji.ns.dto.response.modle.zhongkongshi.ZhongKongShiModel;
import com.whut.ns.caiji.annotation.OperationLog;
import com.whut.ns.caiji.web.service.zhongkongshi.TbZhongKongShiService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/7/13 17:46
 * @desription
 */
@Controller
@RequestMapping("/zhongkongshi")
@CrossOrigin("*")
public class ZhongKongShiController {

    @Resource
    private TbZhongKongShiService zhongKongShiService;

    @RequestMapping(value = "/getAllZhongKongShiDatas")
    @ResponseBody
    @OperationLog(operation = "获取全部中控室信息")
    public ZhongKongShiModel getAllZhongKongShiDatas(){
        return new ZhongKongShiModel(
                Constant.SUCESS,
                "获取全部中控室数据成功",
                new ZhongKongShiBody(zhongKongShiService.selectAll())
        );
    }


    @RequestMapping(value = "/getZhongKongShiDataByTableNameAndDate")
    @ResponseBody
    @OperationLog(operation = "通过表名和日期来查询中控室信息")
    public ZhongKongShiModel getZhongKongShiDataByTableNameAndDate(
            @RequestParam(name = "tableName", required = true) String tableName,
            @RequestParam(name = "date", required = true) Date date

    ){

        return new ZhongKongShiModel(
                Constant.SUCESS,
                "获取 "+tableName+" 中控室数据成功",
                new ZhongKongShiBody(
                        zhongKongShiService.getZhongKongShiDataByTableNameAndDate(
                                tableName,
                                date
                        )
                )
        );
    }

    @RequestMapping(value = "/saveZhongKongData", method = RequestMethod.POST)
    @ResponseBody
    @OperationLog(operation = "保存中控室信息")
    public BaseModel saveZhongKongData(
        @RequestBody List<ZhongKongShi> data
    ){

        return zhongKongShiService.preSaveCollection(data);

    }


}
