package com.whut.ns.caiji.controller.huayanshi;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.whut.my.caiji.ns.commons.constants.Constant;
import com.whut.my.caiji.ns.commons.persistence.BaseModel;
import com.whut.my.caiji.ns.domain.huayanshi.HuaYanShi;
import com.whut.my.caiji.ns.dto.response.modle.huayanshi.HuaYanShiBody;
import com.whut.my.caiji.ns.dto.response.modle.huayanshi.HuaYanShiModel;

import com.whut.ns.caiji.annotation.OperationLog;
import com.whut.ns.caiji.web.service.huayanshi.TbHuaYanShiService;
import com.whut.ns.caiji.web.service.standard.TbStandardService;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/7/15 10:22
 * @desription
 */
@Controller
@RequestMapping("/huayanshi")
@CrossOrigin("*")
public class HuaYanShiController {

    @Resource
    private TbHuaYanShiService huaYanShiService;

    @Resource
    private TbStandardService tbStandardService;

    @RequestMapping(value = "/getAllHuaYanShiDatas")
    @ResponseBody
    @OperationLog(operation = "查看化验室日志")
    public HuaYanShiModel getAllHuaYanShiDatas(){

        return new HuaYanShiModel(
                Constant.SUCESS,
                "获取全部化验室数据成功",
                new HuaYanShiBody(huaYanShiService.selectAll())
        );
    }



    @RequestMapping(value = "/getHuaYanShiDataByTableNameAndDate")
    @ResponseBody
    @OperationLog(operation = "通过表名和日期来查看化验室日志")
    public HuaYanShiModel getHuaYanShiDataByTableNameAndDate(
            @RequestParam(name = "tableName", required = true) String tableName,
            @RequestParam(name = "date", required = true) Date date

    ) {

        return new HuaYanShiModel(
                Constant.SUCESS,
                "获取 "+tableName+" 化验室数据成功",
                new HuaYanShiBody(
                        huaYanShiService.getHuaYanShiDataByTableNameAndDate(
                                tableName,
                                date
                        ),
                        tbStandardService.getStandardDataByTableName(tableName)
                )
        );

    }//end getHuaYanShiDataByDate


    /**
     * 包括暂存和存储整张表
     *
     * @param
     * @return
     */
    @RequestMapping(value = "/saveHuaYanShiData", method = RequestMethod.POST)
    @ResponseBody
    @OperationLog(operation = "保存化验室")
    public BaseModel saveHuaYanShiData(
            @RequestBody List<HuaYanShi> data
    ) {
        return huaYanShiService.preSaveCollection(data);

    }//end saveHuaYanShiData


    @RequestMapping(value = "/getHuaYanShiDataDifferenceValueBytableNameAndDate", method = RequestMethod.POST)
    @ResponseBody
    @OperationLog(operation = "通过表名查询两天数据的差别")
    public HuaYanShiModel getHuaYanShiDataDifferenceValueBytableNameAndDate(
            String tableName, Date today, Date yesterday
    ) {

        return huaYanShiService.getHuaYanShiDataDifferenceValueBytableNameAndDate(tableName,today,yesterday);

    }//end saveHuaYanShiData
}
