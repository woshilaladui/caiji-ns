package com.whut.my.caiji.ns.dto.response.modle;

public class Result {
    //状态代码 方便前端匹配错误码表
    public int code;

    //可读性错误信息
    public String msg;

    //属于哪个控制器处理的范畴
    public String controllerName;

    public Object data;

    public String token;

    public Result(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Result(int code, Object data, String token) {
        this.code = code;
        this.data = data;
        this.token = token;
    }

    public Result(int code, String msg, String controllerName, Object data, String token) {
        this.code = code;
        this.msg = msg;
        this.controllerName = controllerName;
        this.data = data;
        this.token = token;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getControllerName() {
        return controllerName;
    }

    public void setControllerName(String controllerName) {
        this.controllerName = controllerName;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "Result{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", controllerName='" + controllerName + '\'' +
                ", data=" + data +
                ", token='" + token + '\'' +
                '}';
    }
}
