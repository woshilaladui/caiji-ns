package com.whut.my.caiji.ns.domain.temperature;

import com.whut.my.caiji.ns.commons.persistence.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/11/5 15:37
 * @desription
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class TemperatureCookies extends BaseEntity {


    private String cookie;

    private String nickName;

    public TemperatureCookies() {
    }


    public TemperatureCookies(String cookie, String nickName) {
        this.cookie = cookie;
        this.nickName = nickName;
    }

    public TemperatureCookies(Long id, Date createdAt, Date updatedAt, String cookie, String nickName) {
        super(id, createdAt, updatedAt);
        this.cookie = cookie;
        this.nickName = nickName;
    }

    public TemperatureCookies(Date createdAt, Date updatedAt, String cookie, String nickName) {
        super(createdAt, updatedAt);
        this.cookie = cookie;
        this.nickName = nickName;
    }

    @Override
    public String toString() {
        return "TemperatureCookies{" +
                "cookie='" + cookie + '\'' +
                ", nickName='" + nickName + '\'' +
                '}';
    }
}
