package com.whut.ns.caiji.security;

import com.whut.ns.caiji.security.auth.*;
import com.whut.ns.caiji.security.fliter.captcha.CaptchaCodeFilter;
import com.whut.ns.caiji.security.fliter.jwt.JwtAuthenticationTokenFilter;
import com.whut.ns.caiji.web.service.impl.auth.MyUserDetailsServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import javax.annotation.Resource;
import java.util.Arrays;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {


    @Resource
    MyUserDetailsServiceImp myUserDetailsServiceImp;

    @Autowired
    private CaptchaCodeFilter captchaCodeFilter;

    @Autowired
    private JwtAuthenticationTokenFilter jwtAuthenticationTokenFilter;


    @Resource
    private MyAuthenticationEntryPoint myAuthenticationEntryPoint;

    @Resource
    private MyAccessDeniedHandler myAccessDeniedHandler;

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        // 开启允许iframe 嵌套
        //http.headers().frameOptions().disable();


        http.cors()
                .and().csrf().disable()
                //自定义异常处理

                .exceptionHandling()
                .authenticationEntryPoint(myAuthenticationEntryPoint)
                .accessDeniedHandler(myAccessDeniedHandler)
                .and()
                //采用jwt进行先前校验
                .addFilterBefore(jwtAuthenticationTokenFilter, UsernamePasswordAuthenticationFilter.class)
                .addFilterBefore(captchaCodeFilter, jwtAuthenticationTokenFilter.getClass())

//                    .csrf()
//                    .csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse())
//                    .ignoringAntMatchers("/authentication")
//                .and()
                .logout()
                .logoutUrl("/signout")
                //.logoutSuccessUrl("/login.html")
                .deleteCookies("JSESSIONID")
                .and()
                .authorizeRequests()
                .antMatchers("/authentication", "/refreshtoken", "/user/login", "/verification", "/test/test", "/temperature/bind", "/temperature/cancelBind", "/temperature/monitorRegister").permitAll()
                //.antMatchers("/index").authenticated()
                //权限表达式
                //.antMatchers("/test").access("hasRole('admin') or hasAnyAuthority('ROLE_admin')")
                .anyRequest().access("@rabcService.hasPermission(request,authentication)")
                .and().sessionManagement()
                //.sessionCreationPolicy(SessionCreationPolicy.STATELESS);
                .sessionCreationPolicy(SessionCreationPolicy.NEVER)
                .sessionFixation().none();
        //super.configure(http);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {

        auth.userDetailsService(myUserDetailsServiceImp)
                .passwordEncoder(passwordEncoder());

        //RememberMeToken

        super.configure(auth);
    }


    @Bean
    CorsConfigurationSource corsConfigurationSource() {

        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowCredentials(true);
        configuration.addAllowedOrigin("*"); // 1 设置访问源地址
        configuration.addAllowedHeader("*"); // 2 设置访问源请求头
        configuration.addAllowedMethod("*"); // 3 设置访问源请求方法
//        configuration.setAllowedOrigins(
//                Arrays.asList("http://localhost:8888","*")
//        );
        //configuration.setAllowedMethods(Arrays.asList("GET","POST"));
        configuration.applyPermitDefaultValues();

        //配置对所有路径都限制
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


    @Bean(name = BeanIds.AUTHENTICATION_MANAGER)
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }


}
