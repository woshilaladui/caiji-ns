package com.whut.ns.caiji.abstracts;

import com.whut.my.caiji.ns.commons.persistence.BaseDao;
import com.whut.my.caiji.ns.commons.persistence.BaseEntity;
import com.whut.my.caiji.ns.commons.persistence.BaseService;
import com.whut.ns.caiji.annotation.Read;
import com.whut.ns.caiji.annotation.Write;
import com.whut.ns.caiji.security.Jwt.JwtUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractBaseServiceImpl <T extends BaseEntity, D extends BaseDao<T>> implements BaseService<T> {


    @Autowired
    protected D dao;

    //JWT 安全验证
    @Autowired
    protected JwtUtils jwtUtil;

    /**
     * 查询全部数据
     *
     * @return
     */
    @Override
    @Read
    public List<T> selectAll() {
        return dao.selectAll();
    }

    /**
     * 删除用户信息
     *
     * @param id
     */
    @Override
    @Write
    public Integer delete(Long id) {
        return dao.delete(id);
    }

    /**
     * 根据 ID 获取信息
     *
     * @param id
     * @return
     */
    @Override
    public T getById(Long id) {
        return dao.getById(id);
    }

    /**
     * 更新信息
     *
     * @param entity
     */
    @Override
    public Integer update(T entity) {
        return dao.update(entity);
    }

    /**
     * 查询总笔数
     *
     * @return
     */
    @Override
    public int count(T entity) {
        return dao.count(entity);
    }




    /**
     *
     * eg: aIds[1,2,3] bIds[2,3,4]
     *
     * @param aIds 数据库中该用户的T 中的id
     * @param bIds 请求过来携带的 T id
     * @return [1]
     */
    protected List<Long> getDeleteIds(
            List<Long> aIds,
            List<Long> bIds
    ){
        List<Long> tempAIds = new ArrayList<>(aIds);

        tempAIds.removeAll(bIds);
        return tempAIds;
    }

    /**
     *
     * eg: aIds[1,2,3] bIds[2,3,4]
     *
     * @param aIds
     * @param bIds
     * @return [4]
     */
    protected List<Long> getAddIds(
            List<Long> aIds,
            List<Long> bIds
    ){
        List<Long> tempBIds = new ArrayList<>(bIds);

        tempBIds.removeAll(aIds);

        return tempBIds;
    }

}
