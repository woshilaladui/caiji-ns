package com.whut.ns.caiji.hadoop.test_grouping;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/10/23 14:59
 * @desription
 */
public class OrderMapper extends Mapper<LongWritable, Text, OrderBean, NullWritable> {


    OrderBean k = new OrderBean();

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException, IOException {

        // 1 获取一行
        String line = value.toString();

        // 2 截取
        String[] fields = line.split("\t");

        System.out.println("filed 0 = "+ fields[0] );
        System.out.println("filed 2 = "+ fields[2] );

        // 3 封装对象
        k.setOrder_id(Integer.parseInt(fields[0]));
        k.setPrice(Double.parseDouble(fields[2]));

        // 4 写出
        context.write(k, NullWritable.get());
    }

}
