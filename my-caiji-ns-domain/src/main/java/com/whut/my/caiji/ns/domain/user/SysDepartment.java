package com.whut.my.caiji.ns.domain.user;

import com.whut.my.caiji.ns.commons.persistence.BaseEntity;
import lombok.*;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class SysDepartment extends BaseEntity {


    private static final long serialVersionUID = 5123850316287967373L;
    private String departmentName;
    private Boolean isParent;
    private Integer departmentParentId;
    private String  departmentParentIds;
    private Integer departmentStatus;

    @Override
    public String toString() {
        return "SysDepartment{" +
                "id=" + getId() +
                "departmentName='" + departmentName + '\'' +
                ", isParent=" + isParent +
                ", departmentParentId=" + departmentParentId +
                ", departmentParentIds='" + departmentParentIds + '\'' +
                ", departmentStatus=" + departmentStatus +
                ", createdAt=" + getCreatedAt() +
                ", updatedAt=" + getUpdatedAt() +
                '}';
    }
}
