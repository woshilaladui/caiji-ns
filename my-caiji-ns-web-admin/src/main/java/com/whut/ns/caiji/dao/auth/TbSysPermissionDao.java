package com.whut.ns.caiji.dao.auth;

import com.whut.my.caiji.ns.commons.persistence.BaseDao;
import com.whut.my.caiji.ns.domain.user.SysPermission;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/7/6 11:43
 * @desription
 */
@Repository
public interface TbSysPermissionDao extends BaseDao<SysPermission> {

    //查询权限
    List<String> selectAuthorityByRoleCode(@Param("roleCodes") List<String> roleCodes);

    List<SysPermission> selectPermissionUrlsByUserName(@Param("username") String username);

}
