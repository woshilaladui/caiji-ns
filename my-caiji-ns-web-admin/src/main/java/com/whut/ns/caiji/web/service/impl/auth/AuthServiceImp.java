package com.whut.ns.caiji.web.service.impl.auth;

import com.whut.my.caiji.ns.domain.user.SysPermission;
import com.whut.ns.caiji.dao.auth.TbSysPermissionDao;
import com.whut.ns.caiji.dao.auth.TbSysUserDao;
import com.whut.ns.caiji.web.service.user.auth.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/7/5 16:15
 * @desription
 */
@Service
public class AuthServiceImp implements AuthService {

    @Autowired
    private TbSysUserDao tbSysUserDao;

    @Autowired
    private TbSysPermissionDao tbSysPermissionDao;

    public List<SysPermission> getPermissionByUsername(String username){
        return tbSysPermissionDao.selectPermissionUrlsByUserName(username);
    }

}
