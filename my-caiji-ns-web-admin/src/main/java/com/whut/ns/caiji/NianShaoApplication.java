package com.whut.ns.caiji;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
//import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.ImportResource;
import org.springframework.scheduling.annotation.EnableScheduling;

import org.springframework.transaction.annotation.EnableTransactionManagement;


@Slf4j
@SpringBootApplication
@ComponentScan("com.whut")
@EnableTransactionManagement//开启事务
//开启 redis
//@EnableRedisHttpSession
//@ImportResource(locations={"classpath:kaptcha.properties"})
@EnableScheduling
@MapperScan("com.whut.ns.caiji.dao")
public class NianShaoApplication extends SpringBootServletInitializer {

	//打包
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(NianShaoApplication.class);
	}

	public static void main(String[] args) {

		log.info("kangkang_v3.0");

		SpringApplication.run(NianShaoApplication.class, args);
	}
}
