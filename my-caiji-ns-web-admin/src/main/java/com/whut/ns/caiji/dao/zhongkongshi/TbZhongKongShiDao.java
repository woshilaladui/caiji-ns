package com.whut.ns.caiji.dao.zhongkongshi;

import com.whut.my.caiji.ns.commons.persistence.BaseDao;
import com.whut.my.caiji.ns.domain.zhongkongshi.ZhongKongShi;

import java.util.List;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/7/13 15:55
 * @desription
 */
public interface TbZhongKongShiDao extends BaseDao<ZhongKongShi> {


    /**
     * 通过表名和时间来查询数据
     * @param tableName
     * @param date
     * @return
     */
    List<ZhongKongShi> getZhongKongShiDataByTableNameAndDate(String tableName,String date);

    Integer updateZhongKongShis(List<ZhongKongShi> zhongKongShis);

    List<Integer> getZhongKongShiCurrentDateIndex(String tableName,String date );

}
