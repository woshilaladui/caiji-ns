package com.whut.ns.caiji.config.db;

import com.whut.my.caiji.ns.domain.db.DBTypeEnum;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/7/28 17:13
 * @desription
 */

/**
 * 决定返回哪个数据源的key
 */
public class RouttingDataSource extends AbstractRoutingDataSource {

    private DataSource masterDataSource;
    private DataSource slaveDataSource;


    public RouttingDataSource() {
    }

    public RouttingDataSource(DataSource masterDataSource, DataSource slaveDataSource) {
        this.masterDataSource = masterDataSource;
        this.slaveDataSource = slaveDataSource;
    }

    @Override
    protected Object determineCurrentLookupKey() {

        /**
         * 返回当前线程正在使用的代表数据库的枚举对象
         */

        return DynamicSwitchDBTypeUtil.get();
    }

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();

        //todo 做读数据库的负载均衡
    }

    /**
     * 连接失败的时候的处理方法，再次切换数据源
     *
     * @return
     * @throws SQLException
     */
    @Override
    public Connection getConnection() throws SQLException {
        try {
            return super.getConnection();
        } catch (SQLException e) {
            if (DynamicSwitchDBTypeUtil.get() == DBTypeEnum.SLAVE) {
                //从数据库连接失败
                return masterDataSource.getConnection();
            } else if (
                    DynamicSwitchDBTypeUtil.get() == DBTypeEnum.MASTER
            ) {
                return slaveDataSource.getConnection();
            }
            throw e;
        }
    }
}
