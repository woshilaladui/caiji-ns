package com.whut.ns.caiji.web.service.impl.auth;

import com.whut.my.caiji.ns.commons.constants.Constant;
import com.whut.my.caiji.ns.commons.persistence.BaseModel;
import com.whut.my.caiji.ns.commons.utils.DateUtils;
import com.whut.my.caiji.ns.commons.utils.EncrtptionUtils;
import com.whut.my.caiji.ns.domain.user.SysUser;
import com.whut.my.caiji.ns.domain.user.TbUser;
import com.whut.ns.caiji.abstracts.AbstractBaseServiceImpl;
import com.whut.ns.caiji.annotation.Read;
import com.whut.ns.caiji.annotation.Write;
import com.whut.ns.caiji.dao.TbUserDao;
import com.whut.ns.caiji.dao.auth.TbSysRoleDao;
import com.whut.ns.caiji.dao.auth.TbSysUserDao;
import com.whut.ns.caiji.dao.auth.TbSysUserRoleDao;
import com.whut.ns.caiji.web.service.TbUserService;
import com.whut.ns.caiji.web.service.user.auth.TbSysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/7/6 15:40
 * @desription
 */
@Service
//@Transactional(readOnly = true)
public class TbSysUserServiceImp extends AbstractBaseServiceImpl<SysUser, TbSysUserDao> implements TbSysUserService {

    @Resource
    private TbSysUserRoleDao tbSysUserRoleDao;

    @Override
    public Integer update(SysUser entity) {

        entity.setUpdatedAt(DateUtils.getCurrentDate());

        //对密码进行加密
        if(entity.getPassword() != null ){
            entity.setPassword(
                    EncrtptionUtils.encondeByBCrypt(entity.getPassword())
            );

        }

        return super.update(entity);
    }

    //@Transactional(readOnly = false)
    @Write
    public BaseModel deleteUser(Long userId) {

        //先删除用户角色表中的该用户
        Integer flagUserRole = tbSysUserRoleDao.deleteUserRoleUserId(userId);

        //再删除用户表
        Integer flagUser = dao.delete(userId);

        if (flagUser > 0) {
            return BaseModel.success("删除用户id = " + userId + " 成功");
        } else {
            return BaseModel.error("删除用户id = " + userId + " 失败 用户不存在");
        }

    }


    @Override
    @Read
    public SysUser getUserByUsername(String username) {
        return dao.selectUserByUsername(username);
    }

    @Override
    @Read
    public Boolean isPhoneExist(String phone) {
        return dao.selectUserByPhone(phone) != null;
    }

    @Override
    @Read
    public Boolean isUsernameExist(String username) {
        return dao.selectByUserName(username) != null;
    }

    @Override
    public BaseModel updateUserPassword(Long id, String oldPassword, String newPassword) {

        String dbPassword = dao.selectUserByUserId(id).getPassword();



        //如果密码错误的话
        if ( !EncrtptionUtils.isMatach(oldPassword,dbPassword)) {
            return BaseModel.error("原始密码错误重新修改");
        }



        //重新构建用户
        SysUser sysUser = new SysUser();
        sysUser.setId(id);
        sysUser.setPassword(newPassword);
        sysUser.setUpdatedAt(DateUtils.getCurrentDate());

        int flag = dao.update(sysUser);

        if (flag > 0)
            return BaseModel.success("修改密码成功");


        return BaseModel.success("修改密码错误，请联系管理人员");

    }


    //@Transactional(readOnly = false)
    @Write
    @Override
    public BaseModel preSave(SysUser sysUser) {

        if (
                !isPhoneExist(sysUser.getPhone())
                        &&
                        !isUsernameExist(sysUser.getUsername())
        ) {

            sysUser.setEnabled(1);
            sysUser.setCreatedAt(DateUtils.getCurrentDate());
            sysUser.setUpdatedAt(DateUtils.getCurrentDate());
            //对密码进行加密
            sysUser.setPassword(
                    EncrtptionUtils.encondeByBCrypt(sysUser.getPassword())
            );
            return save(sysUser);
        } else {
            return BaseModel.error("添加用户" + sysUser.getUsername() + "失败 手机号或用户名已存在");
        }


    }

    //@Transactional(readOnly = false)
    @Write
    @Override
    public BaseModel lockUser(
            String username,
            Integer enabled
    ) {

        enabled = enabled == 1 ? 0 : 1;


        Integer flag = dao.lockUser(username, enabled);

        String msg = enabled == 1 ? "解锁用户 = " + username + " 成功" : "锁定用户 = " + username + "成功";


        if (flag > 0) {

            return new BaseModel(
                    Constant.SUCESS,
                    msg,
                    null
            );
        } else {
            return BaseModel.error("更新用户 = " + username + "的锁定状态失败");
        }


    }

    //@Transactional(readOnly = false)
    @Write
    @Override
    public BaseModel save(SysUser entity) {

        if (entity != null) {
            dao.insert(entity);
        }

        return new BaseModel(
                Constant.SUCESS,
                "新添加用户 " + entity.getUsername() + " 成功",
                null
        );
    }

    @Override
    public BaseModel saveCollection(List<SysUser> entities) {

        return null;
    }

}
