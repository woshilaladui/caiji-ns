package com.whut.my.caiji.ns.domain.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.whut.my.caiji.ns.commons.persistence.BaseEntity;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Data
public class SysPermission extends BaseEntity {


    private static final long serialVersionUID = 4231008112650841450L;
    private String permissionName;
    private String permissionUrl;

    @JsonIgnore
    private boolean isParent;

    @JsonIgnore
    private Long permissionParentId;

    @JsonIgnore
    private String permissionParentIds;

    private Integer permissionStatus;

    public SysPermission() {
    }

    public SysPermission(Long id,String permissionName,String permissionUrl,Integer permissionStatus,boolean isParent){
        super(id,null,null);
        this.permissionName = permissionName;
        this.permissionUrl = permissionUrl;
        this.isParent = isParent;
        this.permissionStatus = permissionStatus;
    }

    public SysPermission(Long id, Date createdAt, Date updatedAt) {
        super(id, createdAt, updatedAt);
    }

    public SysPermission(Long id, Date createdAt, Date updatedAt, String permissionName, String permissionUrl, boolean isParent, Long permissionParentId, String permissionParentIds, Integer permissionStatus) {
        super(id, createdAt, updatedAt);
        this.permissionName = permissionName;
        this.permissionUrl = permissionUrl;
        this.isParent = isParent;
        this.permissionParentId = permissionParentId;
        this.permissionParentIds = permissionParentIds;
        this.permissionStatus = permissionStatus;
    }




    @Override
    public String toString() {
        return "SysPermission{" +
                "id=" + getId() +
                "permissionName='" + permissionName + '\'' +
                ", permissionUrl='" + permissionUrl + '\'' +
                ", isParent=" + isParent +
                ", permissionParentId=" + permissionParentId +
                ", permissionParentIds='" + permissionParentIds + '\'' +
                ", permissionStatus=" + permissionStatus +", createdAt=" + getCreatedAt() +
                ", updatedAt=" + getUpdatedAt() +

                '}';
    }
}
