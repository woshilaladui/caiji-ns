package com.whut.my.caiji.ns.domain.netty.im;

import com.whut.my.caiji.ns.commons.persistence.BaseEntity;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2021/2/15 11:23
 * @desription IM 聊天消息实体
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Builder
public class ChatMsg extends BaseEntity {

    private Long sendUserId;

    private Long acceptUserId;

    private String msg;

    private Integer signFlag;

    public ChatMsg() {
    }

    public ChatMsg(Long sendUserId, Long acceptUserId, String msg, Integer signFlag) {
        this.sendUserId = sendUserId;
        this.acceptUserId = acceptUserId;
        this.msg = msg;
        this.signFlag = signFlag;
    }

    public ChatMsg(Long id, Date createdAt, Date updatedAt, Long sendUserId, Long acceptUserId, String msg, Integer signFlag) {
        super(id, createdAt, updatedAt);
        this.sendUserId = sendUserId;
        this.acceptUserId = acceptUserId;
        this.msg = msg;
        this.signFlag = signFlag;
    }

    public ChatMsg(Date createdAt, Date updatedAt, Long sendUserId, Long acceptUserId, String msg, Integer signFlag) {
        super(createdAt, updatedAt);
        this.sendUserId = sendUserId;
        this.acceptUserId = acceptUserId;
        this.msg = msg;
        this.signFlag = signFlag;
    }

    public Long getSendUserId() {
        return sendUserId;
    }

    public void setSendUserId(Long sendUserId) {
        this.sendUserId = sendUserId;
    }

    public Long getAcceptUserId() {
        return acceptUserId;
    }

    public void setAcceptUserId(Long acceptUserId) {
        this.acceptUserId = acceptUserId;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getSignFlag() {
        return signFlag;
    }

    public void setSignFlag(Integer signFlag) {
        this.signFlag = signFlag;
    }
}
