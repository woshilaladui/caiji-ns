package com.whut.ns.caiji.web.service.impl.auth;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author zm
 * @version 1.0.0
 * @date 2020/7/5 10:32
 * @desription 用于校验用户是否有该请求论路径的访问权限
 *
 */
@Component("rabcService")
public class MyRBACServiceImp {

    /**
     * 判断某用户是否具有该request资源的访问权限
     */
    public boolean hasPermission(
            HttpServletRequest request,
            Authentication authentication
    ){

        //取出认证主体
        Object principal = authentication.getPrincipal();


        if(principal instanceof UserDetails){

            UserDetails userDetails = ((UserDetails)principal);

            //TODO 开启权限显示
            //System.out.println("userDetails"+ userDetails.getAuthorities());

            //只会有一个
            List<GrantedAuthority> authorityList =
                    AuthorityUtils.commaSeparatedStringToAuthorityList(request.getRequestURI());

            //System.out.println("authorityList"+ authorityList);
            //判断该请求路径是否被包含在该用户的权限集合中
            return userDetails.getAuthorities().contains(authorityList.get(0));

        }
        return false;
    }
}
