package com.whut.ns.caiji.security.auth;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.whut.my.caiji.ns.commons.constants.Constant;
import com.whut.my.caiji.ns.commons.persistence.BaseModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class MyAccessDeniedHandler implements AccessDeniedHandler {

    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public void handle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AccessDeniedException e) throws IOException, ServletException {
        httpServletResponse.setContentType("application/json;charset=utf-8");
        httpServletResponse.getWriter().write(objectMapper.writeValueAsString(
                new BaseModel(
                        Constant.ERROR,
                        "没有权限访问该资源 "+ e.getMessage(),
                        null
                )
        ));
    }
}
