package com.whut.my.caiji.ns.dto.request.model.standard;

import java.util.Date;

public class StandardSetRequestModel {

    public String tableName;
    public String startValue;
    public String endValue;
    public String reason;
    public Date createdAt;

    public StandardSetRequestModel() {
    }

    public StandardSetRequestModel(
            String tableName,
            String startValue,
            String endValue,
            String reason,
            Date createdAt
    ) {
        this.tableName = tableName;
        this.startValue = startValue;
        this.endValue = endValue;
        this.reason = reason;
        this.createdAt = createdAt;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getStartValue() {
        return startValue;
    }

    public void setStartValue(String startValue) {
        this.startValue = startValue;
    }

    public String getEndValue() {
        return endValue;
    }

    public void setEndValue(String endValue) {
        this.endValue = endValue;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public String toString() {
        return "StandardSetRequestModel{" +
                "tableName='" + tableName + '\'' +
                ", startValue='" + startValue + '\'' +
                ", endValue='" + endValue + '\'' +
                ", reason='" + reason + '\'' +
                ", createdAt=" + createdAt +
                '}';
    }
}
