package com.whut.my.caiji.ns.commons.exception;

public class CustomException extends RuntimeException {
    //异常错误编码
    private int code ;
    //异常信息
    private String msg;

    private CustomException(){}

    public CustomException(CustomExceptionType exceptionTypeEnum, String msg) {
        this.code = exceptionTypeEnum.getCode();
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }


    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
