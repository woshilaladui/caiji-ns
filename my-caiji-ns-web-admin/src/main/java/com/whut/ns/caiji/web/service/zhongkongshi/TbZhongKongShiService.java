package com.whut.ns.caiji.web.service.zhongkongshi;

import com.whut.my.caiji.ns.commons.persistence.BaseModel;
import com.whut.my.caiji.ns.commons.persistence.BaseService;
import com.whut.my.caiji.ns.domain.zhongkongshi.ZhongKongShi;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/7/13 15:50
 * @desription
 */
@Service
public interface TbZhongKongShiService extends BaseService<ZhongKongShi> {

    List<ZhongKongShi> getZhongKongShiDataByTableNameAndDate(String tableName, Date date);

    BaseModel preSaveCollection(List<ZhongKongShi> sourceZhongKongShis);

}
