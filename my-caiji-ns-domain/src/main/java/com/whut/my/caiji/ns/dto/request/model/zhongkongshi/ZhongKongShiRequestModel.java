package com.whut.my.caiji.ns.dto.request.model.zhongkongshi;

import java.util.Date;

public class ZhongKongShiRequestModel {

    public Date date;
    public int index;
    public int department;
    public int duty;
    public String tableName;
    public int authority;
    public String data;

    public ZhongKongShiRequestModel() {
    }

    public ZhongKongShiRequestModel(
            Date date,
            int index,
            int department,
            int duty,
            String tableName,
            int authority,
            String data
    ) {
        this.date = date;
        this.index = index;
        this.department = department;
        this.duty = duty;
        this.tableName = tableName;
        this.authority = authority;
        this.data = data;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getDepartment() {
        return department;
    }

    public void setDepartment(int department) {
        this.department = department;
    }

    public int getDuty() {
        return duty;
    }

    public void setDuty(int duty) {
        this.duty = duty;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public int getAuthority() {
        return authority;
    }

    public void setAuthority(int authority) {
        this.authority = authority;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ZhongKongShiRequestModel{" +
                "date=" + date +
                ", index=" + index +
                ", department=" + department +
                ", duty=" + duty +
                ", tableName='" + tableName + '\'' +
                ", permission=" + authority +
                ", data='" + data + '\'' +
                '}';
    }
}
