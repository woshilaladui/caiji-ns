package com.whut.ns.caiji.dao.feedback;

import com.whut.my.caiji.ns.commons.persistence.BaseDao;
import com.whut.my.caiji.ns.domain.feedback.FeedBack;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/11/10 9:34
 * @desription
 */
@Repository
public interface FeedBackDao extends BaseDao<FeedBack> {

    List<FeedBack> getAllFeedBackByUsernameOrTimeBetween(String username,String startDate,String endDate);

}
