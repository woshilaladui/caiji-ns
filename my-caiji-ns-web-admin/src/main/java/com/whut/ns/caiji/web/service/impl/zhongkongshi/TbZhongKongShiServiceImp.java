package com.whut.ns.caiji.web.service.impl.zhongkongshi;


import com.whut.my.caiji.ns.commons.persistence.BaseModel;
import com.whut.my.caiji.ns.commons.utils.DateUtils;
import com.whut.my.caiji.ns.domain.zhongkongshi.ZhongKongShi;
import com.whut.ns.caiji.abstracts.AbstractBaseServiceImpl;
import com.whut.ns.caiji.annotation.Read;
import com.whut.ns.caiji.annotation.Write;
import com.whut.ns.caiji.dao.zhongkongshi.TbZhongKongShiDao;
import com.whut.ns.caiji.web.service.zhongkongshi.TbZhongKongShiService;
import org.springframework.stereotype.Service;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/7/13 15:52
 * @desription
 */
@Service
//@Transactional(readOnly = true)
public class TbZhongKongShiServiceImp extends AbstractBaseServiceImpl<ZhongKongShi,TbZhongKongShiDao> implements TbZhongKongShiService {


    @Override
    @Read
    public List<ZhongKongShi> getZhongKongShiDataByTableNameAndDate(String tableName, Date date) {

        return dao.getZhongKongShiDataByTableNameAndDate(tableName,DateUtils.formatDate(date,"yyyy-MM-dd"));
    }

    @Override
    //@Transactional(readOnly = false)
    @Write
    public BaseModel preSaveCollection(List<ZhongKongShi> sourceZhongKongShis) {

        ZhongKongShi one = sourceZhongKongShis.get(0);

        String date = DateUtils.parseDate(one.getDate(),"YYYY-MM-dd");
        String tableName = one.getTableName();

        List<Integer> indexs= dao.getZhongKongShiCurrentDateIndex(tableName,date);


        /**
         * 对比 sourceZhongKongShis B和 dataBaseZhongKongShis A的 id编号
         *
         * 错误：
         * B 中传递过来的数据中有id == null 字段的则为需要插入的数据
         * B 中 id != null的则为需要更新的数据
         *
         * 正确：
         * 还要判断index字段是否重复
         *
         * 场景:用户第一次点击提交没有刷新界面此时页面该行数据还是没有id 再点提交该行数据应该做
         * 的是更新而不是插入
         *
         */

        //需要新插入数据库的数据
        List<ZhongKongShi> addZhongKongShis = new ArrayList<>();
        List<ZhongKongShi> updateZhongKongShis = new ArrayList<>();

        //TODO 性能问题
        Date nowDate = new Date();

        /**
         * 分离出哪些需要插入哪些需要更新
         */
        sourceZhongKongShis.forEach(
                zhongKongShi -> {

                    if(!indexs.contains(zhongKongShi.getIndex())){
                        zhongKongShi.setCreatedAt(nowDate);
                        zhongKongShi.setUpdatedAt(nowDate);
                        addZhongKongShis.add(zhongKongShi);
                    }
                    else{
                        zhongKongShi.setUpdatedAt(nowDate);
                        updateZhongKongShis.add(zhongKongShi);
                    }

                }
        );

        //更新
        if(updateZhongKongShis.size() > 0){
            dao.updateZhongKongShis(updateZhongKongShis);
        }

        //插入
        return saveCollection(addZhongKongShis);
    }


    @Override
    @Write
    public BaseModel save(ZhongKongShi entity) {
        return null;
    }

    @Override
    //@Transactional(readOnly = false)
    @Write
    public BaseModel saveCollection(List<ZhongKongShi> entities) {

        if(entities != null && entities.size() > 0){
            dao.insertAll(entities);
            return BaseModel.success("保存 "+entities.get(0).getTableName()+" 中控室数据成功");
        }

        return BaseModel.success("保存中控室数据成功");


    }


}
