package com.whut.ns.caiji.scheduler;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author zm
 * @version 1.0.0
 * @date 2020/8/9 9:24
 * @desription 定时备份数据库
 */
@Component
public class DatabaseScheduler {

    //master数据库
    private static final String HOST = "101.200.149.190";

    private static final Integer PORT = 3306;

    //Linux
    private static final String dataBaseBackUpPath = "/home/admin/tomcat/backUp/caiji";

    //Windows
    //private static final String dataBaseBackUpPath = "E:\\backUp\caiji";

    @Value("${spring.datasource.druid.master.username}")
    private String username;

    @Value("${spring.datasource.druid.master.password}")
    private String password;


    //@Scheduled(fixedRate = 60000)
    //每天上午10点开始备份
    //暂时关掉备份
    //@Scheduled(cron = "0 0 10 * * ?")
    public void dump() throws Exception {
        //log.info("备份数据库");
        String backName = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss").format(new Date());
        //dataBaseDump("101.149.200.190", "3306", "root", "wjk19971106", "caiji", backName);
        dataBaseDump(HOST, PORT, username, password, "caiji", backName);
        //backup();
    }

    private static void dataBaseDump(
            String host,
            Integer port,
            String username,
            String password,
            String dataBaseName,
            String backUpName
    ) {

        //生成存放目录
        File file = new File(dataBaseBackUpPath);
        if (!file.exists()) {
            file.mkdir();
        }

        //生成保存文件
        File datafile = new File(file + File.separator + backUpName + ".sql");
        if (datafile.exists()) {
            System.out.println(backUpName + "文件名已存在，请更换");
        }

        //获取系统名称，用来判断是window还是linux
        String os = System.getProperties().getProperty("os.name");

        //用于去调用系统自带的脚本运行程序
        String[] commands = new String[3];

        if (os.startsWith("Win")) {
            //window 调用 cmd
            commands[0] = "cmd.exe";
            commands[1] = "/c";
        } else {
            //Linux调用 /bin/sh  执行脚本文件
            commands[0] = "/bin/sh";
            commands[1] = "-c";
        }

        //拼接 dump语句
        StringBuilder mysqlDump = new StringBuilder();

        //mysqldump --opt --user=root --password=wjk19971106 --host=101.200.149.190 --protocol=tcp --port=3306 --default-character-set=utf8 --single-transaction=TRUE --routines --events caiji > /home/admin/tomcat/2020-08-09-16-48-06.sql
        mysqlDump.append("mysqldump");
        mysqlDump.append(" --opt");
        mysqlDump.append(" --user=").append(username);
        mysqlDump.append(" --password=").append(password);
        mysqlDump.append(" --host=").append(host);
        mysqlDump.append(" --protocol=tcp");
        mysqlDump.append(" --port=").append(port);
        mysqlDump.append(" --default-character-set=utf8");
        mysqlDump.append(" --single-transaction=TRUE");
        mysqlDump.append(" --routines");
        mysqlDump.append(" --events");
        mysqlDump.append(" ").append(dataBaseName);
        mysqlDump.append(" > ");
        mysqlDump.append("").append(datafile).append("");
        String command = mysqlDump.toString();
        System.out.println(command);
        commands[2] = command;

        Runtime runtime = Runtime.getRuntime();

        try {

            Process process = runtime.exec(commands);

            if (process.waitFor() == 0) {
                System.out.println("【" + dataBaseName + "】备份成功");
            } else {
                InputStream is = process.getErrorStream();
                if (is != null) {
                    BufferedReader in = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8));
                    String line;
                    StringBuilder sb = new StringBuilder();
                    while ((line = in.readLine()) != null) {
                        sb.append(line);
                    }
                    System.out.println("数据库备【" + dataBaseName + "】份失败\r\n" + sb.toString());

                    in.close();
                    is.close();
                }
            }


        } catch (Exception e) {

            e.printStackTrace();
            System.err.println("数据库备【" + dataBaseName + "】份失败");
        }
    }
}
