package com.whut.ns.caiji.security.Jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class JwtUtils {

    @Value("${jwt.config.key}")
    private String key ;
    @Value("${jwt.config.ttl}")
    private long ttl ;//一个小时

    public String getKey() {
        return key;
    }
    public void setKey(String key) {
        this.key = key;
    }
    public long getTtl() {
        return ttl;
    }

    public void setTtl(long ttl) {
        this.ttl = ttl;
    }

    /**
     *
     * @param id
     * @param subject 以是JSON数据 尽可能少 subject是存放用户名
     * @param authority
     * @return
     */
    public String createJWT(String id, String subject, int authority) {
        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);
        long nowTime = System.currentTimeMillis();//当前时间
        long exp = nowTime + 1000*60*60*6;//过期时间为6小时
        JwtBuilder builder = Jwts.builder().setId(id)
                .setSubject(subject)
                .setIssuedAt(now)
                .signWith(SignatureAlgorithm.HS256, key).claim("roles", authority)//设置权限
                .signWith(SignatureAlgorithm.HS256, key).claim("userId",id);




//        System.out.println("ttl = "+ ttl);

        if (ttl > 0) {//设置过期时间
            builder.setExpiration(new Date(exp));
        }
        return builder.compact();
    }

    /**
     * 解析JWT
     * @param jwtStr
     * @return
     */
    public Claims parseJWT(String jwtStr){
        return  Jwts.parser()
                .setSigningKey(key)
                .parseClaimsJws(jwtStr)
                .getBody();
    }

}
