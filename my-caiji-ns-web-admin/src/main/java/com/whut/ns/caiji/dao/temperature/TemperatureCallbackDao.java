package com.whut.ns.caiji.dao.temperature;

import com.whut.my.caiji.ns.commons.persistence.BaseDao;
import com.whut.my.caiji.ns.domain.temperature.TemperatureCallback;
import org.springframework.stereotype.Repository;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/11/5 15:43
 * @desription
 */
@Repository
public interface TemperatureCallbackDao extends BaseDao<TemperatureCallback> {

    /**
     * 通过用户名和日期来查询
     * @param nickName
     * @param date
     * @return
     */
    TemperatureCallback getTemperatureCallbackByNickNameAndDate(
            String nickName,
            String date
    );

}
