package com.whut.ns.caiji.dao.temperature;

import com.whut.my.caiji.ns.commons.persistence.BaseDao;
import com.whut.my.caiji.ns.domain.temperature.TemperatureCookies;
import org.springframework.stereotype.Repository;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/11/5 15:49
 * @desription
 */
@Repository
public interface TemperatureCookiesDao extends BaseDao<TemperatureCookies> {
}
