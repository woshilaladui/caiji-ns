package com.whut.my.caiji.ns.commons.persistence;

import org.springframework.dao.DataAccessException;

import java.util.List;
import java.util.Map;

/**
 * 所有数据访问层的基类
 * <p>Title: BaseDao</p>
 * <p>Description: </p>
 *
 * @author Lusifer
 * @version 1.0.0
 * @date 2020/5/30 11:52
 */
public interface BaseDao<T extends BaseEntity> {
    /**
     * 查询全部数据
     *
     * @return
     */
    List<T> selectAll();

    /**
     * 新增
     *
     * @param entity
     */
    Integer insert(T entity) throws DataAccessException;

    /**
     * 新增一个集合
     *
     * @param entities
     */
    Integer insertAll(List<T> entities) throws DataAccessException;

    /**
     * 删除
     *
     * @param id
     */
    Integer delete(Long id) throws DataAccessException;

    /**
     * 根据 ID 查询信息
     *
     * @param id
     * @return
     */
    T getById(Long id);

    /**
     * 更新
     *
     * @param entity
     */
    Integer update(T entity) throws DataAccessException;



    /**
     * 查询总笔数
     *
     * @return
     */
    int count(T entity);
}
