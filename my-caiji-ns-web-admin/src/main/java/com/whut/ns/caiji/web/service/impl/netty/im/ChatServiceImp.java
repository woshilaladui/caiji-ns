package com.whut.ns.caiji.web.service.impl.netty.im;

import com.whut.my.caiji.ns.domain.netty.im.ChatMsg;
import com.whut.my.caiji.ns.domain.netty.im.FriendsRequest;
import com.whut.my.caiji.ns.dto.response.modle.netty.im.ChatMsgBody;
import com.whut.my.caiji.ns.dto.response.modle.netty.im.FriendsRequestBody;
import com.whut.my.caiji.ns.dto.response.modle.netty.im.MyFriendsBody;
import com.whut.my.caiji.ns.enums.im.MsgSignFlagEnum;
import com.whut.ns.caiji.dao.netty.im.ChatMsgMapper;
import com.whut.ns.caiji.web.service.netty.im.ChatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2021/2/15 13:40
 * @desription
 */
@Service
public class ChatServiceImp implements ChatService {

    @Autowired
    private ChatMsgMapper chatMsgMapper;

    @Override
    public void sendFriendRequest(Long myUserId, Long friendUserId) {


    }

    @Override
    public List<FriendsRequestBody> queryFriendRequestList(Long acceptUserId) {
        return null;
    }

    @Override
    public void deleteFriendRequest(FriendsRequest friendsRequest) {

    }

    @Override
    public void passFriendRequest(Long sendUserId, Long acceptUserId) {

    }

    @Override
    public List<MyFriendsBody> queryMyFriends(Long userId) {
        return null;
    }

    @Override
    public Long saveMsg(ChatMsgBody chatMsgBody) {

        ChatMsg msgDb = new ChatMsg();

        msgDb.setSendUserId(chatMsgBody.getSenderId());
        msgDb.setAcceptUserId(chatMsgBody.getReceiverId());
        msgDb.setSignFlag(MsgSignFlagEnum.UNSIGNED.type);

        chatMsgMapper.insert(msgDb);


        return msgDb.getId();
    }

    @Override
    public void updateMsgSigned(List<Long> msgIdList) {
        chatMsgMapper.batchUpdateMsgSigned(msgIdList);
    }

    @Override
    public List<ChatMsg> getUnReadMsgList(Long acceptUserId) {
        return null;
    }

}
