package com.whut.ns.caiji.web.service.netty.im;

import com.whut.my.caiji.ns.domain.netty.im.ChatMsg;
import com.whut.my.caiji.ns.domain.netty.im.FriendsRequest;
import com.whut.my.caiji.ns.dto.response.modle.netty.im.ChatMsgBody;
import com.whut.my.caiji.ns.dto.response.modle.netty.im.FriendsRequestBody;
import com.whut.my.caiji.ns.dto.response.modle.netty.im.MyFriendsBody;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2021/2/15 13:33
 * @desription
 */
@Service
public interface ChatService {

    //发送好友请求
    void sendFriendRequest(Long myUserId,Long friendUserId);

    //好友请求列表查询
    List<FriendsRequestBody> queryFriendRequestList(Long acceptUserId);

    //处理好友请求——忽略好友请求
    void deleteFriendRequest(FriendsRequest friendsRequest);

    //处理好友请求——通过好友请求
    void passFriendRequest(Long sendUserId,Long acceptUserId);

    //好友列表查询
    List<MyFriendsBody> queryMyFriends(Long userId);

    //保存用户聊天消息
    Long saveMsg(ChatMsgBody chatMsgBody);

    void updateMsgSigned(List<Long> msgIdList);

    //获取未签收的消息列表
    List<ChatMsg> getUnReadMsgList(Long acceptUserId);


}
