package com.whut.ns.caiji.web.service.user.auth;

import com.whut.my.caiji.ns.domain.user.SysPermission;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/7/5 16:42
 * @desription
 */
@Service
public interface AuthService {

     List<SysPermission> getPermissionByUsername(String username);

}
