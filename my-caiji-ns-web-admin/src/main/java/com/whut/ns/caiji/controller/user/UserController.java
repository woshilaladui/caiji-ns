package com.whut.ns.caiji.controller.user;


import com.whut.my.caiji.ns.domain.user.TbUser;
import com.whut.my.caiji.ns.dto.response.modle.user.UserBody;
import com.whut.my.caiji.ns.dto.response.modle.user.UserModel;
import com.whut.ns.caiji.dao.auth.TbSysUserDao;
import com.whut.ns.caiji.dao.TbUserDao;
import com.whut.ns.caiji.security.auth.MyUserDetails;
import com.whut.ns.caiji.web.service.TbUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@CrossOrigin(origins = "*")
@RequestMapping("/user")
public class UserController {

    @Autowired
    private TbUserService tbUserService;

    @Autowired
    private TbUserDao tbUserDao;

    @Autowired
    private TbSysUserDao tbSysUserDao;


    /**
     * 登录逻辑
     *
     * @param phone
     * @param password
     * @return
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ResponseBody
    public UserModel login(@RequestParam(required = true) String phone, @RequestParam(required = true) String password) {

        TbUser tbUser = tbUserDao.selectUserByPhone(phone);


        MyUserDetails myUserDetails = tbSysUserDao.selectByUserName("admin");


        //System.out.println("登陆登陆登陆在服务器2");

        UserBody userBody = tbUserService.login(phone, password);

           return new UserModel(
                   userBody
           );



    }

    @RequestMapping("/register")
    @ResponseBody
    public String register(

    ) {

        return "hello";

    }


}
