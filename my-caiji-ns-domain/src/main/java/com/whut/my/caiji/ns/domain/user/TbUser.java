package com.whut.my.caiji.ns.domain.user;


import com.whut.my.caiji.ns.commons.persistence.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;


public class TbUser extends BaseEntity {


    private static final long serialVersionUID = 6403243638459790428L;
    private String username;

    private String phone;

    private String password;

    private int state;

    private int department;

    private int duty;

    private int authority;

    private String detail;

    public TbUser() {
    }

    public TbUser(String username, String phone, String password, int state, int department, int duty, int authority, String detail) {
        this.username = username;
        this.phone = phone;
        this.password = password;
        this.state = state;
        this.department = department;
        this.duty = duty;
        this.authority = authority;
        this.detail = detail;
    }

    public TbUser(Long id, Date createdAt, Date updatedAt, String username, String phone, String password, int state, int department, int duty, int authority, String detail) {
        super(id, createdAt, updatedAt);
        this.username = username;
        this.phone = phone;
        this.password = password;
        this.state = state;
        this.department = department;
        this.duty = duty;
        this.authority = authority;
        this.detail = detail;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getDepartment() {
        return department;
    }

    public void setDepartment(int department) {
        this.department = department;
    }

    public int getDuty() {
        return duty;
    }

    public void setDuty(int duty) {
        this.duty = duty;
    }

    public int getAuthority() {
        return authority;
    }

    public void setAuthority(int authority) {
        this.authority = authority;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }


    @Override
    public String toString() {
        return "TbUser{" +
                "username='" + username + '\'' +
                ", phone='" + phone + '\'' +
                ", password='" + password + '\'' +
                ", state=" + state +
                ", department=" + department +
                ", duty=" + duty +
                ", authority=" + authority +
                ", detail='" + detail + '\'' +
                '}';
    }
}
