package com.whut.ns.caiji.security.fliter.captcha;


//import com.whut.my.caiji.ns.commons.db.RedisCache;

import com.whut.ns.caiji.security.auth.MyAuthenticationEntryPoint;

import com.whut.ns.caiji.security.auth.jwt.JwtTokenUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.session.SessionAuthenticationException;

import org.springframework.stereotype.Component;

import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;



//@EnableRedisHttpSession
@Component("captchaCodeFilter")
@Slf4j
public class CaptchaCodeFilter extends OncePerRequestFilter {


    @Autowired
    private MyAuthenticationEntryPoint myAuthenticationEntryPoint;


    @Autowired
    private JwtTokenUtil jwtTokenUtil;


    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain filterChain)
            throws ServletException, IOException,AuthenticationException {


        if(
                        //防止单刷验证码接口
                        StringUtils.contains(request.getRequestURI(),"/authentication")
                &&
                        StringUtils.equalsIgnoreCase(request.getMethod(),"post")
        ){
            try{
//                //验证谜底与用户输入是否匹配

                validate(new ServletWebRequest(request));
            } catch(AuthenticationException e){
                System.out.println("e = "+e.getMessage());
                myAuthenticationEntryPoint.commence(
                        request,response,e
                );
                return;
            }
        }

        filterChain.doFilter(request,response);

    }

    private void validate(ServletWebRequest request) throws ServletRequestBindingException {

        // redisCache = new RedisCache();
        //String kaptchaToken = (String) redisCache.getObject(session.getId().toString());

        String kaptchaToken =  request.getRequest().getHeader("kaptchaToken");


        //输入的验证码
        String codeInRequest = ServletRequestUtils.getStringParameter(
                    request.getRequest(),"captchaCode");

        //验证码不能为空
        if(StringUtils.isEmpty(codeInRequest)){
            throw new SessionAuthenticationException("验证码不能为空");
        }

        //弃用session
        if(kaptchaToken == null || kaptchaToken.length() == 0){
            throw new SessionAuthenticationException("验证码不存在");
        }


        //解密
        String decodeCodeInRequest= jwtTokenUtil.getCaptchaCodeFromToken(kaptchaToken);

        if(decodeCodeInRequest == null || decodeCodeInRequest.length() == 0){
            throw new SessionAuthenticationException("验证码失效请刷新验证码");
        }



        /**
         * 不匹配需要移除原来的验证码
         */
        if(!codeInRequest.equals(decodeCodeInRequest)){
            //不匹配需要删除原来存在的验证码防止通过该验证码来暴力破解密码
            //redisCache.removeObject(session.getId().toString());
            throw new SessionAuthenticationException("验证码不匹配");
        }
    }



}
