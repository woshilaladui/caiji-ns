package com.whut.ns.caiji.web.test;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2021/4/7 20:58
 * @desription
 */
public class UserServiceImpl implements UserService {

    private int a;

    private int b;

    public UserServiceImpl() {
    }

    public UserServiceImpl(int a) {
        this.a = a;
    }

    public UserServiceImpl(int a, int b) {
        this.a = a;
        this.b = b;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    @Override
    public void sayHi() {
        System.out.println("hi");
    }
}
