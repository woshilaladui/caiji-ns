package com.whut.ns.caiji.controller.log;

import com.whut.my.caiji.ns.commons.constants.Constant;
import com.whut.my.caiji.ns.commons.utils.DateUtils;
import com.whut.my.caiji.ns.commons.utils.ListUtils;
import com.whut.my.caiji.ns.domain.log.TbLogRecord;
import com.whut.my.caiji.ns.dto.response.modle.logRecord.LogRecordBody;
import com.whut.my.caiji.ns.dto.response.modle.logRecord.LogRecordModel;
import com.whut.ns.caiji.web.service.log.OperationLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author Administrator
 * @version 1.0.0
 * @date 2020/7/31 11:20
 * @desription
 */
@Controller
@RequestMapping("/logRecordController")
@CrossOrigin("*")
public class OperationLogController {

    @Autowired
    private OperationLogService operationLogService;

    @RequestMapping("/getAllLogRecords")
    @ResponseBody
    public LogRecordModel getAllLogRecords() {

        List<TbLogRecord> tbLogRecords = operationLogService.selectAll();

        return new LogRecordModel(
                Constant.SUCESS,
                "获取全部日志信息成功",
                new LogRecordBody(tbLogRecords)
        );

    }

    @RequestMapping("/getAllLogsByUsernameOrDateBetween")
    @ResponseBody
    public LogRecordModel getAllLogsByUsernameOrDateBetween(
            @RequestParam(name = "username", required = false) String username,
            @RequestParam(name = "startDate", required = false) Date startDate,
            @RequestParam(name = "endDate", required = false) Date endDate
    ) {

        return new LogRecordModel(
                Constant.SUCESS,
                "获取全部日志信息成功",
                new LogRecordBody(
                        operationLogService.getAllByUsernameOrTimeBetween(
                                username,
                                DateUtils.formatDate(startDate, "YYYY-MM-dd"),
                                DateUtils.formatDate(endDate, "YYYY-MM-dd")
                        )
                )
        );

    }

    @RequestMapping(value = "/deleteLogRecordById",method = RequestMethod.POST)
    @ResponseBody
    public LogRecordModel deleteLogRecordById(
            @RequestBody Map<String,Object> map
    ){
        List<Integer> operationIds = (ArrayList<Integer>) map.get("operationIds");

        boolean flag = operationLogService.deleteOperationLogs(ListUtils.changeIntegerListToLongList(operationIds));

        if(flag){
            return new LogRecordModel(
                    Constant.SUCESS,
                    "删除成功",
                    null
            );
        }else {
            return new LogRecordModel(
                    Constant.SUCESS,
                    "删除失败",
                    null
            );
        }


    }

}
